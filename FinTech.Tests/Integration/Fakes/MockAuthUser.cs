﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace FinTech.Tests.Integration.Fakes
{
    public class MockAuthUser
    {
        public List<Claim> Claims { get; private set; } = new();

        public MockAuthUser(params Claim[] claims)
            => Claims = claims.ToList();
    }
}