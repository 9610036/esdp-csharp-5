using System.Linq;
using System.Security.Claims;
using FinTech.Models.Data;
using FinTech.Services.CompanyService;
using FinTech.Tests.Integration.Fakes;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FinTech.Tests.Integration.Factory
{
    public class CustomWebAppFactory<TStartup>
        : WebApplicationFactory<TStartup> where TStartup : class
    {
        private readonly MockAuthUser _user = new MockAuthUser(
            new Claim("sub", "47d90476-8de1-4a71-b0f0-9beaf4d89c98"),
            new Claim("email", "user@test.com"),
            new Claim(ClaimTypes.Role, "admin"),
            new Claim(ClaimTypes.NameIdentifier, "47d90476-8de1-4a71-b0f0-9beaf4d89c98"));
        
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var contextService = services.SingleOrDefault(d =>
                    d.ServiceType == typeof(DbContextOptions<EsdpContext>));
                services.Remove(contextService);

                services.AddDbContext<EsdpContext>(options => { options.UseInMemoryDatabase("IntegrationTestsDatabase").UseLazyLoadingProxies(); });

                var sp = services.BuildServiceProvider();

                using var scope = sp.CreateScope();
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<EsdpContext>();
                var companyService = scope.ServiceProvider.GetService<ICompanyService>();

                DataSeeding.SeedBank(db);
                DataSeeding.SeedCompany(db);
                DataSeeding.SeedSettings(db, companyService);
                DataSeeding.SeedUser(db, companyService);
                DataSeeding.SeedAssetLiabilities(db, companyService);
                DataSeeding.SeedPaymentAccounts(db, companyService);
                DataSeeding.SeedBudgetItems(db, companyService);
                DataSeeding.SeedOperations(db, companyService);
                
                services.AddTestAuthentication();
            
                // Register a default user, so all requests have it by default
                services.AddScoped(_ => _user); 
                services.AddAntiforgery(t =>
                {
                    t.Cookie.Name = AntiForgeryTokenExtractor.AntiForgeryCookieName;
                    t.FormFieldName = AntiForgeryTokenExtractor.AntiForgeryFieldName;
                });
                db.Database.EnsureCreated();
            });
        }
    }
}
