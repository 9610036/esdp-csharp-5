﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using FinTech.Tests.Integration.Factory;
using FinTech.Tests.Integration.Fakes;
using Microsoft.Net.Http.Headers;
using Xunit;

namespace FinTech.Tests.Integration
{
    public class CounterpartyTest : IClassFixture<CustomWebAppFactory<Startup>>
    {
        private readonly CustomWebAppFactory<Startup> _factory;
        private readonly HttpClient _client;

        public CounterpartyTest(CustomWebAppFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services => { });
            }).CreateClient();
        }

        [Fact]
        public async Task CounterpartyController_Index_Returns_CounterpartyViewModels()
        {
            var response = await _client.GetAsync("Counterparty/Index");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            Assert.Contains("PS Company", responseString);
            Assert.Contains("PS", responseString);
            Assert.Contains("112233445577", responseString);
            Assert.Contains("Hostinger", responseString);
            Assert.Contains("Hostinger Corp", responseString);
            Assert.Contains("112233445566", responseString);
            Assert.Contains("New Counterparty", responseString);
            Assert.Contains("NC", responseString);
            Assert.Contains("100010001000", responseString);
            Assert.Contains("testing", responseString);
        }

        [Fact]
        public async Task CounterpartyController_Edit_Returns_Updated_CounterpartyViewModel()
        {
            var initResponse = await _client.GetAsync("Counterparty/Edit?id=4");
            var antiForgeryValues = await AntiForgeryTokenExtractor.ExtractAntiForgeryValues(initResponse);
            var postRequest = new HttpRequestMessage(HttpMethod.Post, "Counterparty/Edit?id=4");
            postRequest.Headers.Add("Cookie", new CookieHeaderValue(AntiForgeryTokenExtractor.AntiForgeryCookieName, antiForgeryValues.cookieValue).ToString());
            var formModel = new Dictionary<string, string>
            {
                { AntiForgeryTokenExtractor.AntiForgeryFieldName, antiForgeryValues.fieldValue },
                { "Name", "PS4 Company Edited" },
                { "ShortName", "PS4 Edited" },
                {"BIN", "112233445588"},
                {"Comment", "testing edit action"}
            };
            postRequest.Content = new FormUrlEncodedContent(formModel);
            var response = await _client.SendAsync(postRequest);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            Assert.Contains("PS4 Company Edited", responseString);
            Assert.Contains("PS4 Edited", responseString);
            Assert.Contains("112233445588", responseString);
            Assert.Contains("testing edit action", responseString);
        }
        
        [Fact]
        public async Task CounterpartyController_AddNewCounterparty_Returns_New_CounterpartyViewModel()
        {
            var initResponse = await _client.GetAsync("Counterparty/AddNewCounterparty");
            var antiForgeryValues = await AntiForgeryTokenExtractor.ExtractAntiForgeryValues(initResponse);
            var postRequest = new HttpRequestMessage(HttpMethod.Post, "Counterparty/AddNewCounterparty");
            postRequest.Headers.Add("Cookie", new CookieHeaderValue(AntiForgeryTokenExtractor.AntiForgeryCookieName, antiForgeryValues.cookieValue).ToString());
            var formModel = new Dictionary<string, string>
            {
                { AntiForgeryTokenExtractor.AntiForgeryFieldName, antiForgeryValues.fieldValue },
                { "Name", "New Counterparty" },
                { "ShortName", "NC" },
                {"BIN", "100010001000"},
                {"Comment", "testing"}
            };
            postRequest.Content = new FormUrlEncodedContent(formModel);
            var response = await _client.SendAsync(postRequest);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            Assert.Contains("New Counterparty", responseString);
            Assert.Contains("NC", responseString);
            Assert.Contains("100010001000", responseString);
            Assert.Contains("testing", responseString);
        }
        
        [Fact]
        public async Task CounterpartyController_Delete_Returns_CounterpartyViewModels_Without_Deleted()
        {
            var response = await _client.GetAsync("Counterparty/Delete?id=3");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            Assert.DoesNotContain("Super Corp", responseString);
            Assert.DoesNotContain("SuperC", responseString);
            Assert.DoesNotContain("112233445515", responseString);
            Assert.DoesNotContain("Company to be deleted", responseString);
        }
        
        [Fact]
        public async Task CounterpartyController_GetDetails_Returns_CounterpartyViewModels()
        {
            var response = await _client.GetAsync("Counterparty/GetDetails?id=1");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            Assert.Contains("Hostinger", responseString);
            Assert.Contains("Hostinger Corp", responseString);
            Assert.Contains("112233445566", responseString);
        }
    }
}