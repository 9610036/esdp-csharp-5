﻿using System.Linq;
using AutoMapper;
using FinTech.Enums;
using FinTech.MappingConfigurations.BudgetItem.BudgetItemViewModelProfile;
using FinTech.MappingConfigurations.Resolvers;
using FinTech.Models;
using FinTech.Models.Data;
using FinTech.Repositories.BudgetItemRepositories;
using FinTech.Repositories.CompanyRepositories;
using FinTech.Services.BudgetItemService;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace FinTech.Tests.Unit
{
    public class BudgetItemTest
    {
        private readonly EsdpContext _context;
        private readonly IBudgetItemService _budgetItemService;
        private readonly GetCompanyResolver _companyResolver;


        public BudgetItemTest()
        {
            Mock<ICompanyRepository> repositoryMock = new Mock<ICompanyRepository>();
            var options = new DbContextOptionsBuilder<EsdpContext>()
                .UseInMemoryDatabase("TestDatabase").Options;
            _context = new EsdpContext(options);

            _context.Database.EnsureDeleted();
            AddBudgetItem();

            MapperConfiguration configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new CreateBudgetItemViewModelProfile());
                cfg.AddProfile(new UpdateBudgetItemViewModelProfile());
                cfg.AddProfile(new BudgetItemViewModelProfile());
                cfg.ConstructServicesUsing(s => new GetCompanyResolver(repositoryMock.Object));
            });


            IMapper mapper = new Mapper(configuration);
            IBudgetItemRepository budgetItemRepository = new BudgetItemRepository(_context);
            _budgetItemService = new BudgetItemService(budgetItemRepository, mapper);
            
           
        }

        [Fact]
        public void GetBudgetItemById_Returns_BudgetItem()
        {
            var result = _budgetItemService.GetBudgetItemById("1");
            Assert.Equal("1", result.Id);
            Assert.Equal("Аренда офиса", result.Name);
            Assert.Equal(BudgetItemTypes.Permanent, result.Type);
            Assert.NotNull(result.Company);
            Assert.Equal("TestCompany", result.Company.Name);
            Assert.Contains("@", result.Company.Email);
            Assert.Equal("1234567890", result.Company.Bin);
            Assert.Equal("87777777777", result.Company.Phone);
        }

        [Fact]
        public void GetBudgetItemsListByCompanyId_Returns_List_BudgetItems()
        {
            var result = _budgetItemService.GetBudgetItemsListByCompanyId("1", BudgetItemTypes.Expense).ToList();
            Assert.True(result.Count >= 2);
            foreach (var elm in result)
            {
                Assert.Equal(BudgetItemTypes.Permanent, elm.Type);
                Assert.Equal("1", elm.Company.Id);
            }
        }

        [Fact]
        public void DeleteBudgetItem()
        {
            var budgetItem = _budgetItemService.GetBudgetItemById("1");
            _budgetItemService.DeleteBudgetItemById(budgetItem.Id);
            _context.BudgetItems.Remove(budgetItem);
            Assert.False(_budgetItemService.GetBudgetItemById("1").IsActive);
        }

        [Fact]
        public void GetBudgetItemByName_Return_BudgetItem()
        {
            var budgetItemByName = _budgetItemService.GetBudgetItemByName("Аренда офиса");
            Assert.Equal("Аренда офиса", budgetItemByName.Name);
        }

        private void AddBudgetItem()
        {
            if (!_context.BudgetItems.Any())
            {
                Company company = new Company()
                {
                    Id = "1",
                    Name = "TestCompany",
                    Bin = "1234567890",
                    Phone = "87777777777",
                    Email = "test@test.com"
                };
                _context.Companies.Add(company);

                _context.BudgetItems.AddRange(
                    new BudgetItem()
                    {
                        Id = "1",
                        Name = "Аренда офиса",
                        Type = BudgetItemTypes.Permanent,
                        Company = company,
                        IsActive = true
                    },
                    new BudgetItem()
                    {
                        Id = "2",
                        Name = "Оплата за элетроинергию",
                        Type = BudgetItemTypes.Permanent,
                        Company = company,
                        IsActive = true
                    });
                _context.SaveChanges();
            }
        }
      
    }
}