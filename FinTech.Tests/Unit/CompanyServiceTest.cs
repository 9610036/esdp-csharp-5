﻿using System.Linq;
using AutoMapper;
using FinTech.MappingConfigurations.Company.UpdateCompanyViewModelProfile;
using FinTech.Models;
using FinTech.Models.Data;
using FinTech.Repositories.CompanyRepositories;
using FinTech.Repositories.UserRepositories;
using FinTech.Services.CompanyService;
using FinTech.Services.UserService;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace FinTech.Tests.Unit
{
    public class CompanyServiceTest
    {
        private EsdpContext _context;
        private ICompanyService _companyService;
        private readonly IUserService _userService;

        public CompanyServiceTest()
        {
            var options = new DbContextOptionsBuilder<EsdpContext>()
                .UseInMemoryDatabase("CompanyTestDatabase")
                .Options;
            _context = new EsdpContext(options);
            _context.Database.EnsureDeleted();
            AddCompanies();
            MapperConfiguration configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UpdateCompanyViewModelProfile());
            });
            Mapper mapper = new Mapper(configuration);
            ICompanyRepository companyRepository = new CompanyRepository(_context);
            IUserRepository userRepository = new UserRepository(_context);
            _userService = new UserService(userRepository, mapper);
            _companyService = new CompanyService(companyRepository, mapper, _userService);
        }

        [Fact]
        public void GetCompanyByBin_Returns_Company()
        {
            var result = _companyService.GetCompanyByBin("22");
            Assert.IsType<Company>(result);
            Assert.Equal("BB", result.Id);
            Assert.Equal("22", result.Bin);
            Assert.Equal("Company2", result.Name);
            Assert.Equal("445566", result.Phone);
            Assert.Equal("bb@445566.bb", result.Email);
        }


        [Fact]
        public void DeleteCompany_Returns_RemoveCompanyFromContext()
        {
            var company = _context.Companies.FirstOrDefault(c => c.Id == "AA");
            _companyService.DeleteCompany(company);
            Assert.False(company.IsActive);
            Assert.False(_context.Companies.FirstOrDefault(c => c.Id == "AA").IsActive);
            Assert.True(_context.Companies.FirstOrDefault(c => c.Id == "BB").IsActive);
            Assert.True(_context.Companies.FirstOrDefault(c => c.Id == "CC").IsActive);
        }

        [Fact]
        public void GetCompanyById_Returns_Company()
        {
            var result = _companyService.GetCompanyById("BB");
            Assert.IsType<Company>(result);
            Assert.Equal("BB", result.Id);
            Assert.Equal("22", result.Bin);
            Assert.Equal("Company2", result.Name);
            Assert.Equal("445566", result.Phone);
            Assert.Equal("bb@445566.bb", result.Email);
        }

        private void AddCompanies()
        {
            _context.Companies.AddRange(
                new Company {Id = "AA", Bin = "11", Name = "Company1", Phone = "112233", Email = "aa@112233.aa", IsActive = true},
                new Company {Id = "BB", Bin = "22", Name = "Company2", Phone = "445566", Email = "bb@445566.bb", IsActive = true},
                new Company {Id = "CC", Bin = "33", Name = "Company3", Phone = "778899", Email = "cc@778899.cc", IsActive = true});
            _context.SaveChanges();
        }
    }
}