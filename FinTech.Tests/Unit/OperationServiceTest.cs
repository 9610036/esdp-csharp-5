﻿using System;
using System.Linq;
using AutoMapper;
using FinTech.Enums;
using FinTech.MappingConfigurations.Operation.CreateOperationViewModelProfile;
using FinTech.MappingConfigurations.Operation.OperationViewModelProfile;
using FinTech.MappingConfigurations.Operation.UpdateOperationViewModelProfile;
using FinTech.Models;
using FinTech.Models.Data;
using FinTech.Services.CompanyService;
using FinTech.Services.Filtration.Operation;
using FinTech.Services.OperationService;
using FinTech.Services.Pagination.Operation;
using FinTech.Tests.Unit.Fakes;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace FinTech.Tests.Unit
{
    public class OperationServiceTest
    {
        private readonly EsdpContext _context;
        private readonly IOperationService _operationService;

        public OperationServiceTest()
        {
            Mock<ICompanyService> companyMock = new Mock<ICompanyService>();
            Mock<OperationPaginationService> operationPagination = new Mock<OperationPaginationService>();
            Mock<IOperationFiltrationService> operationFiltrationService = new Mock<IOperationFiltrationService>();
            var options = new DbContextOptionsBuilder<EsdpContext>()
                .UseInMemoryDatabase("TestDatabaseOperation").Options;
            _context = new EsdpContext(options);

            _context.Database.EnsureDeleted();
            AddOperations();
            MapperConfiguration configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new CreateOperationViewModelProfile());
                cfg.AddProfile(new UpdateOperationViewModelProfile());
                cfg.AddProfile(new OperationViewModelProfile());
            });

            IMapper mapper = new Mapper(configuration);
            FakeOperationRepository fakeOperationRepository = new FakeOperationRepository(_context);

            _operationService = new OperationService(fakeOperationRepository, operationPagination.Object,
                operationFiltrationService.Object, mapper);
        }


        [Fact]
        public void GetOperationsListByCompanyId_Return_List_Operations()
        {
            var listOperations = _operationService.GetOperationsListByCompanyId("1");
            Assert.True(listOperations.ToList().Count >= 1);
        }


        [Fact]
        public void DeleteOperation()
        {
            var operation = _operationService.GetOperationById("1");
            Assert.NotEmpty(operation.Id);
            _operationService.DeleteOperation("1");
            var changeOperation = _operationService.GetOperationById("1");
            Assert.False(changeOperation.IsActive);
        }

        [Fact]
        public void GetOperationById_Return_Operation()
        {
            var operation = _operationService.GetOperationById("1");
            Assert.Equal("1", operation.Id);
        }

        private void AddOperations()
        {
            Company company = new Company()
            {
                Id = "1",
                Name = "TestCompany",
                Bin = "1234567890",
                Phone = "87777777777",
                Email = "test@test.com"
            };
            _context.Companies.Add(company);

            Bank bank = new Bank {Id = "B", Bik = "22", Name = "Bank2", IsActive = true};
            _context.Banks.Add(bank);

            Counterparty counterparty = new Counterparty()
            {
                Id = "1",
                Bin = "31213121312",
                ShortName = "RPG",
                Name = "Too RPG",
                Comment = "Коментарий 2",
                Company = company
            };
            _context.Counterparties.Add(counterparty);

            PaymentAccount paymentAccount = new PaymentAccount()
            {
                Id = "1",
                Iban = "3212131212",
                Amount = 123000,
                Bank = bank,
                IsActive = true
            };
            _context.PaymentAccounts.Add(paymentAccount);

            BudgetItem budgetItem = new BudgetItem()
            {
                Id = "1",
                Name = "Аренда офиса",
                Type = BudgetItemTypes.Permanent,
                Company = company
            };
            _context.BudgetItems.Add(budgetItem);

            if (!_context.Operations.Any())
            {
                _context.Operations.Add(
                    new Operation()
                    {
                        Id = "1", CreationDate = DateTime.Now, Sum = 2000,
                        Comment = "Коментарий1", IsActive = true, IsConfirmed = true,
                        Type = OperationTypes.Financial,
                        BudgetItem = budgetItem,
                        Counterparty = counterparty,
                        PaymentAccount = paymentAccount
                    });
                _context.SaveChanges();
            }
        }
    }
}