using System;
using System.Linq;
using AutoMapper;
using FinTech.Enums;
using FinTech.MappingConfigurations.AssetLiability;
using FinTech.Models;
using FinTech.Models.Data;
using FinTech.Repositories.AssetLiabilityRepositories;
using FinTech.Repositories.CompanyRepositories;
using FinTech.Services.AssetLiabilityService;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace FinTech.Tests.Unit
{
    public class AssetLiabilityTest
    {
        private readonly EsdpContext _context;
        private readonly IAssetLiabilityService _assetLiabilityService;
        

        public AssetLiabilityTest()
        {
            var options = new DbContextOptionsBuilder<EsdpContext>()
                .UseInMemoryDatabase("AssetLiabilityTestDatabase")
                .Options;
            _context = new EsdpContext(options);
            _context.Database.EnsureDeleted();
            AddAssetLiability();
            MapperConfiguration configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AssetLiabilityViewModelProfile());
            });
            IMapper mapper = new Mapper(configuration);
            IAssetLiabilityRepository assetLiabilityRepository = new AssetLiabilityRepository(_context);
            ICompanyRepository companyRepository = new CompanyRepository(_context);
            _assetLiabilityService = new AssetLiabilityService(assetLiabilityRepository, companyRepository, mapper);
        }
        
        [Fact]
        public void GetAssetLiabilityById_Returns_AssetLiability()
        {
            var result = _assetLiabilityService.GetAssetLiabilityById("123");
            Assert.Equal("123", result.Id);
            Assert.Equal("Машина", result.Name);
            Assert.Equal(AssetLiabilityTypes.Asset, result.Type);
            Assert.NotNull(result.Company);
            Assert.IsType<AssetLiability>(result);
        }
        
        [Fact]
        public void GetAssetLiabilityListByCompanyId_Returns_List_Of_AssetLiabilities()
        {
            var result = _assetLiabilityService.GetAssetLiabilitiesByCompanyId("1").ToList();
            Assert.Equal("123", result[0].Id);
            Assert.Equal("Машина", result[0].Name);
            Assert.Equal(12345.00m, result[0].Sum);
            Assert.Equal(AssetLiabilityTypes.Asset, result[0].Type);
            Assert.Equal("456", result[1].Id);
            Assert.Equal("Кредит", result[1].Name);
            Assert.Equal(56789.00m, result[1].Sum);
            Assert.Equal(AssetLiabilityTypes.Liability, result[1].Type);
        }
        
        [Fact]
        public void GetAssetLiabilitiesByCompanyId_Year_Type_Returns_List_Of_AssetLiabilities()
        {
            var result = _assetLiabilityService.GetAssetLiabilitiesByCompanyId_Year_Type("1",DateTime.Now, AssetLiabilityTypes.Asset);
            Assert.NotNull(result);
            Assert.Equal("123", result[0].Id);
            Assert.Equal("Машина", result[0].Name);
            Assert.Equal(12345.00m, result[0].Sum);
            Assert.Equal(AssetLiabilityTypes.Asset, result[0].Type);
            Assert.Equal(2021,result[0].CreationDate.Year);
        }
        
        private void AddAssetLiability()
        {
            if (!_context.BudgetItems.Any())
            {
                Company company = new Company()
                {
                    Id = "1",
                    Name = "TestCompany",
                    Bin = "1234567890",
                    Phone = "87777777777",
                    Email = "test@test.com"
                };
                _context.Companies.Add(company);
            
                _context.AssetLiabilities.AddRange(
                    new AssetLiability
                    {
                        Id = "123",
                        Name = "Машина",
                        Sum = 12345.00m,
                        Type = AssetLiabilityTypes.Asset,
                        CreationDate = DateTime.Now,
                        Company = company
                    },
                    new AssetLiability
                    {
                        Id = "456",
                        Name = "Кредит",
                        Sum = 56789.00m,
                        Type = AssetLiabilityTypes.Liability,
                        CreationDate = DateTime.Now,
                        Company = company
                    });
                _context.SaveChanges();
            }
        }
    }
}