﻿using System.Collections.Generic;
using AutoMapper;
using FinTech.MappingConfigurations.Bank.BankViewModelProfile;
using FinTech.MappingConfigurations.Bank.UpdateBankViewModelProfile;
using FinTech.Models;
using FinTech.Models.Data;
using FinTech.Repositories.BankRepositories;
using FinTech.Services.BankService;
using FinTech.ViewModels.Banks;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace FinTech.Tests.Unit
{
    public class BankServiceTest
    {
        private readonly EsdpContext _context;
        private readonly IBankService _bankService;

        public BankServiceTest()
        {
            var options = new DbContextOptionsBuilder<EsdpContext>()
                .UseInMemoryDatabase("BankTestDatabase")
                .Options;
            _context = new EsdpContext(options);
            _context.Database.EnsureDeleted();
            AddBanks();
            MapperConfiguration configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new BankViewModelProfile());
                cfg.AddProfile(new UpdateBankViewModelProfile());
            });
            IMapper mapper = new Mapper(configuration);
            IBankRepository bankRepository = new BankRepository(_context);
            _bankService = new BankService(bankRepository, mapper);
        }

        [Fact]
        public void GetFirstOrDefaultBankById_Returns_Bank()
        {
            var result = _bankService.GetFirstOrDefaultBankById("B");
            Assert.IsType<Bank>(result);
            Assert.Equal("B", result.Id);
            Assert.Equal("22", result.Bik);
            Assert.Equal("Bank2", result.Name);
            Assert.True(result.IsActive);
        }

        [Fact]
        public void GetBankDetailsById_Returns_BankViewModel()
        {
            var result = _bankService.GetBankDetails("B");
            Assert.IsType<BankViewModel>(result);
            Assert.Equal("B", result.Id);
            Assert.Equal("22", result.Bik);
            Assert.Equal("Bank2", result.Name);
            Assert.True(result.IsActive);
        }

        [Fact]
        public void UpdateBankViewModel_Returns_UpdateBankViewModel()
        {
            var result = _bankService.GetUpdateBankViewModelById("B");
            Assert.IsType<UpdateBankViewModel>(result);
            Assert.Equal("B", result.Id);
            Assert.Equal("22", result.Bik);
            Assert.Equal("Bank2", result.Name);
        }

        [Fact]
        public void GetBankViewModels_Returns_BankViewModels()
        {
            var result = _bankService.GetBankViewModels();
            Assert.IsType<List<BankViewModel>>(result);
            Assert.Equal(2, result.Count);
            Assert.Equal("A", result[0].Id);
            Assert.Equal("B", result[1].Id);
            Assert.Equal("11", result[0].Bik);
            Assert.Equal("22", result[1].Bik);
            Assert.Equal("Bank1", result[0].Name);
            Assert.Equal("Bank2", result[1].Name);
            Assert.True(result[0].IsActive);
            Assert.True(result[1].IsActive);
        }

        private void AddBanks()
        {
            _context.Banks.AddRange(
                new Bank {Id = "A", Bik = "11", Name = "Bank1", IsActive = true},
                new Bank {Id = "B", Bik = "22", Name = "Bank2", IsActive = true},
                new Bank {Id = "C", Bik = "33", Name = "Bank3", IsActive = false});
            _context.SaveChanges();
        }
    }
}