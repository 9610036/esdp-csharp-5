using System.Linq;
using AutoMapper;
using FinTech.MappingConfigurations.Counterparty.CounterpartyViewModelProfile;
using FinTech.MappingConfigurations.Counterparty.UpdateCounterpartyViewModelProfile;
using FinTech.Services.CompanyService;
using FinTech.Services.CounterpartyService;
using FinTech.Tests.Unit.Fakes;
using Moq;
using Xunit;

namespace FinTech.Tests.Unit
{
    public class CounterpartyServiceTest
    {
        private readonly ICounterpartyService _counterpartyService;

        public CounterpartyServiceTest()
        {
            Mock<ICompanyService> companyServiceMock = new Mock<ICompanyService>();
            FakeCounterpartyRepository repository = new FakeCounterpartyRepository();
            MapperConfiguration configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new CounterpartyViewModelProfile());
                cfg.AddProfile(new UpdateCounterpartyViewModelProfile());
            });
            IMapper mapper = new Mapper(configuration);

            _counterpartyService = new CounterpartyService(
                repository,
                companyServiceMock.Object,
                mapper
            );
        }

        [Fact]
        public void GetCounterpartyById_Returns_Counterparty()
        {
            var result = _counterpartyService.GetCounterpartyById("1");
            Assert.Equal("12345", result.Id);
            Assert.Equal("FakeCounterparty LTD", result.Name);
            Assert.Equal("FC", result.ShortName);
            Assert.Equal("333444555666", result.Bin);
            Assert.Equal("This is fake counterparty", result.Comment);
        }

        [Fact]
        public void GetCounterpartiesByCompanyId_Returns_Counterparties()
        {
            var result = _counterpartyService.GetCounterpartiesByCurrentCompanyId("123").ToList();
            Assert.Equal("67890", result[0].Id);
            Assert.Equal("ItIsFake", result[0].Name);
            Assert.Equal("haha", result[0].ShortName);
            Assert.Equal("098765432123", result[0].Bin);
            Assert.Equal("lorem ipsum", result[0].Comment);
        }
        
        /*[Fact]
        public void IsAnyCounterpartyWithBinForCompany_Returns_True()
        {
            var result = _counterpartyService.CheckBin()
            Assert.Equal("67890", result[0].Id);
            Assert.Equal("ItIsFake", result[0].Name);
            Assert.Equal("haha", result[0].ShortName);
            Assert.Equal("098765432123", result[0].Bin);
            Assert.Equal("lorem ipsum", result[0].Comment);
        }*/
    }
}
