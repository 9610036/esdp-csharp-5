using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FinTech.MappingConfigurations.Bank.BankViewModelProfile;
using FinTech.MappingConfigurations.Company.CompanyViewModelProfile;
using FinTech.MappingConfigurations.PaymentAccount.CreatePaymentAccountViewModelProfile;
using FinTech.MappingConfigurations.PaymentAccount.PaymentAccountViewModelProfile;
using FinTech.MappingConfigurations.PaymentAccount.UpdatePaymentAccountViewModelProfile;
using FinTech.Models;
using FinTech.Models.Data;
using FinTech.Repositories.PaymentAccountRepositories;
using FinTech.Services.PaymentAccountService;
using FinTech.ViewModels.PaymentAccounts;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace FinTech.Tests.Unit
{
    public class PaymentAccountServiceTests
    {

        private EsdpContext _context;
        private IPaymentAccountService _paymentAccountService;
        private IMapper _mapper;
        
        public PaymentAccountServiceTests()
        {
            ConfigureContext();
            ConfigureMapper();
            ConfigurePaymentAccountService();
        }

        private void ConfigurePaymentAccountService()
        {
            var paymentAccountRepository = new PaymentAccountRepository(_context);
            _paymentAccountService = new PaymentAccountService(
                paymentAccountRepository,
                _mapper);
        }

        private void ConfigureMapper()
        {
            MapperConfiguration configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new PaymentAccountViewModelProfile());
                cfg.AddProfile(new CompanyViewModelProfile());
                cfg.AddProfile(new CreatePaymentAccountViewModelProfile());
                cfg.AddProfile(new UpdatePaymentAccountViewModelProfile());
                cfg.AddProfile(new BankViewModelProfile());
            });
            _mapper = new Mapper(configuration);
        }

        private void ConfigureContext()
        {
            var options = new DbContextOptionsBuilder<EsdpContext>()
                .UseInMemoryDatabase("PATestDatabase")
                .Options;
            _context = new EsdpContext(options);
            Seed();
        }

        private void Seed()
        {
            _context.Database.EnsureDeleted();
            DataSeed.DataSeed.SeedPaymentAccounts(_context);
        }
        
        [Fact]
        public void GetAccountsByCompanyId_Returns_PaymentAccountViewModels()
        {
            Seed();
            var companyId = _context.Companies.First().Id;
            List<PaymentAccountViewModel> result = _paymentAccountService.GetAccountsByCompanyId(companyId);
            Assert.NotNull(result);
            Assert.Single(result);
            Assert.IsType<List<PaymentAccountViewModel>>(result);
        }
        
        [Fact]
        public void GetPaymentAccountsByCompanyId_Returns_PaymentAccounts()
        {
            Seed();
            var companyId = _context.Companies.First().Id;
            var result = _paymentAccountService.GetPaymentAccountsByCompanyId(companyId);
            Assert.NotNull(result);
            Assert.Single(result);
            Assert.IsAssignableFrom<IQueryable<PaymentAccount>>(result);
        }
        
        [Fact]
        public void GetAccountInfoByAccountId_Returns_PaymentAccountViewModel()
        {
            Seed();
            var paymentAccountId = _context.PaymentAccounts.First().Id;
            var result = _paymentAccountService.GetAccountInfoByAccountId(paymentAccountId);
            Assert.NotNull(result);
            Assert.IsType<PaymentAccountViewModel>(result);
            Assert.Equal(paymentAccountId, result.Id);
        }
        
        [Fact]
        public void DeleteById()
        {
            Seed();
            _paymentAccountService.DeactivateById(_context.PaymentAccounts.Last().Id);
            Assert.False(_context.PaymentAccounts.Last().IsActive);
        }
    }
}