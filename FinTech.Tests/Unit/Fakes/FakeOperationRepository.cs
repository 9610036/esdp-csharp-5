﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Models.Data;
using FinTech.Repositories.OperationRepositories;

namespace FinTech.Tests.Unit.Fakes
{
    public class FakeOperationRepository : IOperationRepository
    {
        private EsdpContext _context;

        public FakeOperationRepository(EsdpContext context)
        {
            _context = context;
        }

        public void Create(Operation item)
        {
            _context.Operations.Add(item);
        }

        public void Update(Operation item)
        {
            _context.Operations.Update(item);
        }

        public void SetDisabled(Operation item)
        {
            var operation = item;
            operation.IsActive = false;
            _context.Operations.Update(operation);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public Task SaveAsync()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Operation> GetListByCompanyId(string companyId)
        {
            Company company = new Company()
            {
                Id = "1",
                Name = "TestCompany",
                Bin = "1234567890",
                Phone = "87777777777",
                Email = "test@test.com"
            };

            Bank bank = new Bank {Id = "B", Bik = "22", Name = "Bank2", IsActive = true};

            Counterparty counterparty = new Counterparty()
            {

                Id = "1",
                Bin = "31213121312",
                ShortName = "RPG",
                Name = "Too RPG",
                Comment = "Коментарий 2",
                Company = company

            };

            PaymentAccount paymentAccount = new PaymentAccount()
            {
                Id = "1",
                Iban = "3212131212",
                Amount = 123000,
                Bank = bank,
                IsActive = true
            };

            BudgetItem budgetItem = new BudgetItem()
            {
                Id = "1",
                Name = "Аренда офиса",
                Type = BudgetItemTypes.Permanent,
                Company = company
            };

           var operations = new List<Operation>()
            {
                new Operation(){Id = "1", CreationDate = DateTime.Now, Sum = 2000, 
                    Comment = "Коментарий1",IsActive = true, IsConfirmed = true, 
                    Type = OperationTypes.Financial,
                    BudgetItem = budgetItem,
                    Counterparty = counterparty,
                    PaymentAccount = paymentAccount
                }
            };
           return operations.AsQueryable();
        }

        public IQueryable<Operation> GetListByCompanyIdFromDateStartToDateFinish(Company company, DateTime start, DateTime finish)
        {
            throw new NotImplementedException();
        }

        public Operation GetByOperationId(string operationId)
        {
            return _context.Operations.FirstOrDefault(o => o.Id == operationId);
        }

        public IQueryable<Operation> GetMonthOperationsListByCompanyBin(string companyBin, DateTime requiredMonth)
        {
            throw new NotImplementedException();
        }

        public bool CheckExistById(string id)
        {
            var operation = _context.Operations.FirstOrDefault(o=>o.Id == id);
            if (operation != null)
                return true;
            return false;
            
        }
    }
}