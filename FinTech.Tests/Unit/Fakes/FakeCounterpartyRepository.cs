using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Repositories.CounterpartyRepositories;

namespace FinTech.Tests.Unit.Fakes
{
    public class FakeCounterpartyRepository : ICounterpartyRepository
    {
        public void Create(Counterparty item)
        {
            throw new System.NotImplementedException();
        }

        public void Update(Counterparty item)
        {
            throw new System.NotImplementedException();
        }

        public void SetDisabled(Counterparty item)
        {
            throw new System.NotImplementedException();
        }

        public void Save()
        {
            throw new System.NotImplementedException();
        }

        public Task SaveAsync()
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<Counterparty> GetListByCompanyId(string companyId)
        {
            var counterparties =  new List<Counterparty>()
            {
                new Counterparty
                {
                    Id = "67890",
                    Name = "ItIsFake",
                    ShortName = "haha",
                    Bin = "098765432123",
                    Comment = "lorem ipsum"
                }
            };
            return counterparties.AsQueryable();
        }

        public Counterparty GetByCounterpartyId(string counterpartyId)
        {
            return new Counterparty()
            {
                Id = "12345",
                Name = "FakeCounterparty LTD",
                ShortName = "FC",
                Bin = "333444555666",
                Comment = "This is fake counterparty"
            };
        }

        public bool IsAnyCounterpartyWithBinForCompany(string bin, Company company)
        {
            throw new System.NotImplementedException();
        }

        public bool CheckExistById(string id)
        {
            throw new System.NotImplementedException();
        }

        /*public bool IsAnyCounterpartyWithBinForCompany(string bin, Company company)
        {
            Counterparty counterparty = new Counterparty()
            {
                Id = "6789",
                Name = "Counterparty123",
                ShortName = "FakeCounterparty",
                Bin = "130840004885",
                Comment = "This is fake",
                Company = new Company()
                {
                    Id = "FakeCo",
                    Bin = "777",
                    Name = "CompanyFake",
                    Phone = "1122334455",
                    Email = "fake@fake.com"
                }
            };
            return counterparty.Bin != bin && counterparty.Company != company;
        }*/
    }
}