using System;
using System.Linq;
using AutoMapper;
using FinTech.Enums;
using FinTech.MappingConfigurations.AssetLiability;
using FinTech.MappingConfigurations.Bank.BankViewModelProfile;
using FinTech.MappingConfigurations.BudgetItem.BudgetItemViewModelProfile;
using FinTech.MappingConfigurations.Company.CompanyViewModelProfile;
using FinTech.MappingConfigurations.Counterparty.CounterpartyViewModelProfile;
using FinTech.MappingConfigurations.Operation.OperationViewModelProfile;
using FinTech.MappingConfigurations.PaymentAccount.PaymentAccountViewModelProfile;
using FinTech.Models.Data;
using FinTech.Repositories.BudgetItemRepositories;
using FinTech.Repositories.CompanyRepositories;
using FinTech.Repositories.OperationRepositories;
using FinTech.Services.BudgetItemService;
using FinTech.Services.Filtration.Operation;
using FinTech.Services.OperationService;
using FinTech.Services.Pagination.Operation;
using FinTech.Services.ProfitAndLosesServices;
using FinTech.ViewModels.ProfitAndLoses;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace FinTech.Tests.Unit
{
    public class ProfitAndLosesServiceTests
    {
        private EsdpContext _context;
        private IProfitAndLosesService _profitAndLosesService;
        private IMapper _mapper;
        
        public ProfitAndLosesServiceTests()
        {
            ConfigureContext();
            ConfigureMapper();
            ConfigureProfitAndLosesService();
        }

        [Fact]
        public void GetProfitAndLosesViewModel_Returns_ProfitAndLosesViewModel()
        {
            Seed();
            var company = _context.Companies.First();
            var result = 
                _profitAndLosesService.GetProfitAndLosesViewModel(
                    company, 
                    DateTime.MinValue, 
                    DateTime.Now);
            Assert.NotNull(result);
            Assert.IsType<ProfitAndLosesViewModel>(result);
            Assert.Equal(2, result.Operations.Count);
        }
        
        [Fact]
        public void GetMonthPnLReportByCompanyBin_Returns_MonthsPnLReportViewModel()
        {
            Seed();
            var company = _context.Companies.First();
            var result = _profitAndLosesService.GetMonthPnLReportByCompanyBin(
                company.Bin, DateTime.Now);
            Assert.NotNull(result);
            Assert.IsType<MonthsPnLReportViewModel>(result);
            Assert.Equal(0, result.OperatingProfit);
            Assert.Equal(0, result.Profitability);
        }
        
        [Fact]
        public void GetPnLByMonths_Returns_PnLViewModel()
        {
            Seed();
            var company = _context.Companies.First();
            var result = _profitAndLosesService.GetPnLByMonths(
                2021, company.Bin);
            Assert.NotNull(result);
            Assert.IsType<PnLViewModel>(result);
            Assert.Equal(12, result.YearPnLReport.Count);
        }

        [Fact]
        public void GetProfitAndLosesViewModelOperationsSum_Returns_Decimal()
        {
            Seed();
            var company = _context.Companies.First();
            var pnlViewModel = 
                _profitAndLosesService.GetProfitAndLosesViewModel(
                    company, 
                    DateTime.MinValue, 
                    DateTime.Now);
            var result = _profitAndLosesService.GetProfitAndLosesViewModelOperationsSum(
                pnlViewModel, BudgetItemTypes.Income);
            Assert.IsType<decimal>(result);
            Assert.Equal(2000, result);
        }
        
        [Fact]
        public void GetViewModelOperationsSumByBudgetTypeName_Returns_Decimal()
        {
            Seed();
            var company = _context.Companies.First();
            var pnlViewModel = 
                _profitAndLosesService.GetProfitAndLosesViewModel(
                    company, 
                    DateTime.MinValue, 
                    DateTime.Now);
            var result = _profitAndLosesService.GetViewModelOperationsSumByBudgetTypeName(
                pnlViewModel, "Зарплата");
            Assert.IsType<decimal>(result);
            Assert.Equal(2000, result);
        }
        
        [Fact]
        public void GetProfitAndLosesDifferenceByCompanyIdAndYear_Returns_Decimal()
        {
            Seed();
            var company = _context.Companies.First();
            var result = _profitAndLosesService.GetProfitAndLosesDifferenceByCompanyIdAndYear(
                company.Id, DateTime.Now);
            Assert.IsType<decimal>(result);
            Assert.Equal(0, result);
        }

        private void ConfigureContext()
        {
            var options = new DbContextOptionsBuilder<EsdpContext>()
                .UseInMemoryDatabase("PnLTestDatabase")
                .Options;
            
            _context = new EsdpContext(options);
            Seed();
        }

        private void Seed()
        {
            _context.Database.EnsureDeleted();
            DataSeed.DataSeed.SeedOperations(_context);
        }
        
        private void ConfigureMapper()
        {
            MapperConfiguration configuration = new MapperConfiguration(cfg =>
                cfg.AddProfiles(new Profile[]
                {
                    new OperationViewModelProfile(),
                    new CounterpartyViewModelProfile(),
                    new PaymentAccountViewModelProfile(),
                    new BankViewModelProfile(),
                    new CompanyViewModelProfile(),
                    new BudgetItemViewModelProfile(),
                    new AssetLiabilityViewModelProfile()
                }));
            _mapper = new Mapper(configuration);
        }
        
        private void ConfigureProfitAndLosesService()
        {
            var operationRepository = new OperationRepository(_context, _mapper);
            var companyRepository = new CompanyRepository(_context);
            _profitAndLosesService = new ProfitAndLosesService(
                operationRepository,
                new OperationService(
                    operationRepository,
                    new OperationPaginationService(),
                    new OperationFiltrationService(),
                    _mapper),
                new BudgetItemService(
                    new BudgetItemRepository(_context),
                    _mapper),
                _mapper
            );
        }
    }
}