using System;
using System.Collections.Generic;
using System.Linq;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Models.Data;

namespace FinTech.Tests.DataSeed
{
    public class DataSeed
    {

        public static void SeedBanks(EsdpContext context, bool forceSeed = false)
        {
            if (!context.Banks.Any() || forceSeed)
            {
                context.Banks.AddRange(new List<Bank>
                {
                    new Bank
                    {
                        Bik = "CASPKZKA", 
                        Name = "АО \"Дочерний Банк \"АЛЬФА-БАНК\""
                    }
                });
                context.SaveChanges();
            }
        }

        public static void SeedCompanies(EsdpContext context, bool forceSeed = false)
        {
            if (!context.Companies.Any())
            {
                context.Add(new Company
                {
                    Name = "TestCompany", 
                    Bin = "1234567890", 
                    Phone = "87777777777", 
                    Email = "test@test.com"
                });
                context.SaveChanges();
            }
        }
        
        public static void SeedCounterparties(EsdpContext context, bool forceSeed = false)
        {
            SeedCompanies(context);
            if (!context.Counterparties.Any() || forceSeed)
            {
                context.Counterparties.AddRange(new List<Counterparty>()
                {
                    new Counterparty
                    {
                        Name = "Neo Service",
                        ShortName = "NS",
                        Bin = "111111111111",
                        Company = context.Companies.First()
                    },
                    new Counterparty
                    {
                        Name = "Flowers",
                        ShortName = "FL",
                        Bin = "222222222222",
                        Company = context.Companies.First()
                    }
                });
                context.SaveChanges();
            }
        }
        
        public static void SeedBudgetItems(EsdpContext context, bool forceSeed = false)
        {
            SeedCompanies(context);
            SeedAssetLiabilities(context);
            if (!context.BudgetItems.Any() || forceSeed)
            {
                context.BudgetItems.AddRange(new List<BudgetItem>
                {
                    new BudgetItem
                    {
                        Name = "Покупка видеокарты",
                        Type = BudgetItemTypes.Expense,
                        Company = context.Companies.First(),
                        AssetLiability = context.AssetLiabilities.First()
                    },
                    new BudgetItem
                    {
                        Name = "Зарплата",
                        Type = BudgetItemTypes.Income,
                        Company = context.Companies.First()
                    }
                });
                context.SaveChanges();
            }
        }
        
        public static void SeedAssetLiabilities(EsdpContext context, bool forceSeed = false)
        {
            SeedCompanies(context);
            if (!context.AssetLiabilities.Any() || forceSeed)
            {
                context.AssetLiabilities.AddRange(new List<AssetLiability>
                {
                    new AssetLiability
                    {
                        CreationDate = DateTime.Now,
                        Name = "Расход",
                        Sum = 2000,
                        Type = AssetLiabilityTypes.Liability,
                        Company = context.Companies.First()
                    },
                    new AssetLiability
                    {
                        CreationDate = DateTime.Now,
                        Name = "Видеокарта",
                        Sum = 1000,
                        Type = AssetLiabilityTypes.Asset,
                        Company = context.Companies.First(),
                        AssetType = AssetTypes.InventorySum
                    }
                });
                context.SaveChanges();
            }
        }
        
        public static void SeedPaymentAccounts(EsdpContext context, bool forceSeed = false)
        {
            SeedCompanies(context);
            SeedBanks(context);
            if (!context.PaymentAccounts.Any() || forceSeed)
            {
                context.PaymentAccounts.Add(new PaymentAccount()
                {
                    Iban = "KZ111111111111111111",
                    Bank = context.Banks.First(),
                    Company = context.Companies.First(),
                    Amount = 1200000,
                    CurrencyType = CurrencyTypes.KZT
                });
                context.SaveChanges();
            }
        }

        public static void SeedOperations(EsdpContext context, bool forceSeed = false)
        {
            SeedCompanies(context);
            SeedPaymentAccounts(context);
            SeedCounterparties(context);
            SeedBudgetItems(context);
            SeedAssetLiabilities(context);
            if (!context.Operations.Any() || forceSeed)
            {
                context.Operations.AddRange(new List<Operation>()
                {
                    new Operation
                    {
                        CreationDate = DateTime.Now,
                        Sum = 2000,
                        Comment = "aaa",
                        Type = OperationTypes.Operational,
                        Company = context.Companies.First(),
                        PaymentAccount = context.PaymentAccounts.First(),
                        Counterparty = context.Counterparties.First(),
                        BudgetItem = context.BudgetItems.First(),
                        IsConfirmed = true
                    },
                    new Operation
                    {
                        CreationDate = DateTime.Now,
                        Sum = 2000,
                        Comment = "aaa",
                        Type = OperationTypes.Operational,
                        Company = context.Companies.First(),
                        PaymentAccount = context.PaymentAccounts.First(),
                        Counterparty = context.Counterparties.First(),
                        BudgetItem = context.BudgetItems.Last(),
                        IsConfirmed = true
                    }
                });
                context.SaveChanges();
            }
        }
    }
}