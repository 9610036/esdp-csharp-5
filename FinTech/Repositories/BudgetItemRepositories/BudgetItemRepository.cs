﻿using System.Linq;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Models.Data;

namespace FinTech.Repositories.BudgetItemRepositories
{
    public class BudgetItemRepository : IBudgetItemRepository
    {
        private readonly EsdpContext _context;

        public BudgetItemRepository(EsdpContext context)
        {
            _context = context;
        }

        public void Create(BudgetItem item)
        {
            _context.BudgetItems.Add(item);
        }

        public void Update(BudgetItem item)
        {
            _context.BudgetItems.Update(item);
        }

        public void SetDisabled(BudgetItem item)
        {
            item.IsActive = false;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public IQueryable<BudgetItem> GetListByCompanyId(string companyId)
        {
            return _context.BudgetItems.Where(bi => bi.Company.Id == companyId && bi.IsActive);
        }

        public BudgetItem GetByBudgetItemId(string budgetItemId)
        {
            return _context.BudgetItems.FirstOrDefault(bi => bi.Id == budgetItemId);
        }

        public BudgetItem GetByBudgetItemName(string budgetItemName)
        {
            return _context.BudgetItems.FirstOrDefault(bi => bi.Name == budgetItemName);
        }

        public bool CheckExistById(string id)
        {
            return _context.BudgetItems.Any(b => b.Id == id);
        }
    }
}