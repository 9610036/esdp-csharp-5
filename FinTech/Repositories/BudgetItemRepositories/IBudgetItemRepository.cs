﻿using System.Linq;
using FinTech.Models;

namespace FinTech.Repositories.BudgetItemRepositories
{
    public interface IBudgetItemRepository : IRepository<BudgetItem>
    {
        IQueryable<BudgetItem> GetListByCompanyId(string companyId);
        BudgetItem GetByBudgetItemId(string budgetItemId);
        BudgetItem GetByBudgetItemName(string budgetItemName);
        bool CheckExistById(string id);
    }
}