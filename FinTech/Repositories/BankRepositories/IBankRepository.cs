﻿using System.Linq;
using FinTech.Models;

namespace FinTech.Repositories.BankRepositories
{
    public interface IBankRepository : IRepository<Bank>
    {
        Bank GetBankById(string id);
        IQueryable<Bank> GetListBank();
        Bank GetBankByName(string name);
        bool CheckBankExistByBik(string bik);
        bool CheckBankExistById(string id);
    }
}