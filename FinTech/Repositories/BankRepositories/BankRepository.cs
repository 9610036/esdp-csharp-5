﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Models.Data;

namespace FinTech.Repositories.BankRepositories
{
    public class BankRepository : IBankRepository
    {
        private readonly EsdpContext _context;

        public BankRepository(EsdpContext context)
        {
            _context = context;
        }

        public void Create(Bank item)
        {
            try
            {
                if (!_context.Banks.Any(b => b.Name == item.Name && b.Bik == item.Bik))
                    _context.Banks.Add(item);
                else
                    _context.Banks.FirstOrDefault(b => b.Bik == item.Bik).IsActive = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void Update(Bank item)
        {
            _context.Banks.Update(item);
        }

        public void SetDisabled(Bank item)
        {
            try
            {
                _context.Banks.FirstOrDefault(b => b == item).IsActive = false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public Bank GetBankById(string id)
        {
            return _context.Banks.FirstOrDefault(b => b.Id == id);
        }

        public IQueryable<Bank> GetListBank()
        {
            return _context.Banks;
        }
        public Bank GetBankByName(string name)
        {
           return _context.Banks.FirstOrDefault(n => n.Name == name);
        }
        public bool CheckBankExistByBik(string bik)
        {
            return _context.Banks.Any(b => b.Bik == bik);
        }

        public bool CheckBankExistById(string id)
        {
            return _context.Banks.Any(b => b.Id == id);
        }
    }
}