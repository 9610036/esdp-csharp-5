﻿using System.Linq;
using FinTech.Models;

namespace FinTech.Repositories.CounterpartyRepositories
{
    public interface ICounterpartyRepository : IRepository<Counterparty>
    {
        IQueryable<Counterparty> GetListByCompanyId(string companyId);
        Counterparty GetByCounterpartyId(string counterpartyId);
        bool IsAnyCounterpartyWithBinForCompany(string bin, Company company);
        bool CheckExistById(string id);
    }
}