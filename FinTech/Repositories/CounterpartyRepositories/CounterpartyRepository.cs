﻿using System.Linq;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Models.Data;

namespace FinTech.Repositories.CounterpartyRepositories
{
    public class CounterpartyRepository : ICounterpartyRepository
    {
        private readonly EsdpContext _context;

        public CounterpartyRepository(EsdpContext context)
        {
            _context = context;
        }

        public void Create(Counterparty item)
        {
            _context.Counterparties.Add(item);
        }

        public void Update(Counterparty item)
        {
            _context.Counterparties.Update(item);

        }

        public void SetDisabled(Counterparty item)
        {
            item.IsActive = false;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public IQueryable<Counterparty> GetListByCompanyId(string companyId)
        {
            return _context.Counterparties.Where(c => c.Company.Id == companyId
            && c.IsActive);
        }

        public Counterparty GetByCounterpartyId(string counterpartyId)
        {
            return _context.Counterparties.FirstOrDefault(c => c.Id == counterpartyId);
        }

        public bool IsAnyCounterpartyWithBinForCompany(string bin, Company company)
        {
            return _context.Counterparties.Any(c => c.Bin == bin && c.Company == company);
        }

        public bool CheckExistById(string id)
        {
            return _context.Counterparties.Any(c => c.Id == id);
        }
    }
}