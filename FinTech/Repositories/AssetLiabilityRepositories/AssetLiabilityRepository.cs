﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Models.Data;

namespace FinTech.Repositories.AssetLiabilityRepositories
{
    public class AssetLiabilityRepository : IAssetLiabilityRepository
    {
        private readonly EsdpContext _context;

        public AssetLiabilityRepository(EsdpContext context)
        {
            _context = context;
        }

        public void Create(AssetLiability item)
        {
            _context.AssetLiabilities.Add(item);
        }

        public void Update(AssetLiability item)
        {
            _context.AssetLiabilities.Update(item);
        }

        public void SetDisabled(AssetLiability item)
        {
            item.IsActive = false;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public IQueryable<AssetLiability> GetByCompanyId(string companyId)
        {
            return _context.AssetLiabilities.Where(al => al.Company.Id == companyId && al.IsActive);
        }

        public AssetLiability GetById(string id)
        {
            return _context.AssetLiabilities.FirstOrDefault(al => al.Id == id);
        }

        public bool CheckExistById(string id)
        {
            return _context.AssetLiabilities.Any(a => a.Id == id);
        }

        public void Deactivate(string id)
        {
            try
            {
                var assetLiability = _context.AssetLiabilities.FirstOrDefault(al => al.Id == id);
                if (assetLiability != null) assetLiability.IsActive = false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }


        public Company GetCompanyById(string id)
        {
            return _context.Companies.FirstOrDefault(c => c.Id == id);
        }

    }
}