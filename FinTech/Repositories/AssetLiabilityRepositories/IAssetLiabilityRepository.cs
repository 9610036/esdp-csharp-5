﻿using System.Linq;
using FinTech.Models;

namespace FinTech.Repositories.AssetLiabilityRepositories
{
    public interface IAssetLiabilityRepository : IRepository<AssetLiability>
    {
        IQueryable<AssetLiability> GetByCompanyId(string companyId);
        AssetLiability GetById(string id);
        bool CheckExistById(string id);
        void Deactivate(string id);
        Company GetCompanyById(string id);
    }
}