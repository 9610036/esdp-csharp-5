﻿using System.Collections.Generic;
using FinTech.Models;

namespace FinTech.Repositories.CompanyRepositories
{
    public interface ICompanyRepository : IRepository<Company>
    {
        Company GetFirstCompany();
        Company GetByCompanyBin(string companyBin);
        Company GetByCompanyId(string companyId);
        Company GetByCompanyName(string companyName);
        public List<Company> GetAllCompanies();
    }
}