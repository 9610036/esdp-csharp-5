﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Models.Data;

namespace FinTech.Repositories.CompanyRepositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly EsdpContext _context;

        public CompanyRepository(EsdpContext context)
        {
            _context = context;
        }

        public void Create(Company item)
        {
            _context.Companies.Add(item);
        }

        public void Update(Company item)
        {
            _context.Companies.Update(item);
        }

        public void SetDisabled(Company item)
        {
            _context.Companies.Remove(item);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public Company GetFirstCompany()
        {
            return _context.Companies.First();
        }

        public Company GetByCompanyBin(string companyBin)
        {
            return _context.Companies.FirstOrDefault(c => c.Bin == companyBin);
        }

        public Company GetByCompanyId(string companyId)
        {
            return _context.Companies.FirstOrDefault(c => c.Id == companyId);
        }

        public Company GetByCompanyName(string companyName)
        {
            return _context.Companies.FirstOrDefault(c => c.Name == companyName);
        }

        public List<Company> GetAllCompanies()
        {
            return _context.Companies.ToList();
        }
    }
}