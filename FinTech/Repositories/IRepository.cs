﻿using System.Threading.Tasks;

namespace FinTech.Repositories
{
    public interface IRepository<T> where T : class
    {
        void Create(T item);
        void Update(T item);
        void SetDisabled(T item);
        void Save();
        Task SaveAsync();
    }
}