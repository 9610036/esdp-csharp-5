﻿using System;
using System.Linq;
using FinTech.Models;

namespace FinTech.Repositories.OperationRepositories
{
    public interface IOperationRepository : IRepository<Operation>
    {
        IQueryable<Operation> GetListByCompanyId(string companyId);
        IQueryable<Operation> GetListByCompanyIdFromDateStartToDateFinish(Company company, DateTime start, DateTime finish);
        Operation GetByOperationId(string operationId);
        IQueryable<Operation> GetMonthOperationsListByCompanyBin(string companyBin, DateTime requiredMonth);
        bool CheckExistById(string id);
    }
}