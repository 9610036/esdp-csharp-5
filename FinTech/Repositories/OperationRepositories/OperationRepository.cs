﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Models.Data;

namespace FinTech.Repositories.OperationRepositories
{
    public class OperationRepository : IOperationRepository
    {
        private readonly EsdpContext _context;
        private readonly IMapper _mapper;

        public OperationRepository(EsdpContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Create(Operation item)
        {
            _context.Operations.Add(item);
        }

        public void Update(Operation item)
        {
            _context.Operations.Update(item);
        }

        public void SetDisabled(Operation item)
        {
            _context.Operations.Remove(item);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public IQueryable<Operation> GetListByCompanyId(string companyId)
        {
            return _context.Operations.Where(o => o.Company.Id == companyId);
        }

        public IQueryable<Operation> GetListByCompanyIdFromDateStartToDateFinish(Company company, DateTime start, DateTime finish)
        {
            return _context.Operations.Where(o => o.CreationDate.Month >= start.Month && 
                                                  o.CreationDate.Month <= finish.Month 
                                                  && o.Company.Id == company.Id
                                                  && o.IsConfirmed
                                                  && o.Type == OperationTypes.Operational);;
        }

        public Operation GetByOperationId(string operationId)
        {
            return _context.Operations.FirstOrDefault(o => o.Id == operationId);
        }

        public IQueryable<Operation> GetMonthOperationsListByCompanyBin(string companyBin, DateTime requiredMonth)
        {
            return _context.Operations.Where(o => o.CreationDate.Month == requiredMonth.Month && 
                                                  o.CreationDate.Year == requiredMonth.Year &&
                                                  o.Company.Bin == companyBin);
        }

        public bool CheckExistById(string id)
        {
            return _context.Operations.Any(o => o.Id == id);
        }
    }
}