﻿using System.Linq;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Models.Data;

namespace FinTech.Repositories.PaymentAccountRepositories
{
    public class PaymentAccountRepository : IPaymentAccountRepository
    {
        private readonly EsdpContext _context;

        public PaymentAccountRepository(EsdpContext context)
        {
            _context = context;
        }

        public void Create(PaymentAccount item)
        {
            _context.PaymentAccounts.Add(item);
        }

        public void Update(PaymentAccount item)
        {
            _context.PaymentAccounts.Update(item);
        }

        public void SetDisabled(PaymentAccount item)
        {
            _context.PaymentAccounts.Remove(item);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public IQueryable<PaymentAccount> GetListByCompanyId(string companyId)
        {
            return _context.PaymentAccounts.Where(pa => pa.Company.Id == companyId);
        }

        public PaymentAccount GetByPaymentAccountId(string paymentAccountId)
        {
            return _context.PaymentAccounts.FirstOrDefault(pa => pa.Id == paymentAccountId);
        }

        public decimal GetPaymentAccountSumByPaymentAccountId(string paymentAccountId)
        {
            var paymentAccount = GetByPaymentAccountId(paymentAccountId);
            return paymentAccount.Amount;
        }

        public bool CheckExistById(string id)
        {
            return _context.PaymentAccounts.Any(p => p.Id == id);
        }
    }
}