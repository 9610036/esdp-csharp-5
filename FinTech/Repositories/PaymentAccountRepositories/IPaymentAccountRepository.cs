﻿using System.Linq;
using FinTech.Models;

namespace FinTech.Repositories.PaymentAccountRepositories
{
    public interface IPaymentAccountRepository : IRepository<PaymentAccount>
    {
        IQueryable<PaymentAccount> GetListByCompanyId(string companyId);
        PaymentAccount GetByPaymentAccountId(string paymentAccountId);
        decimal GetPaymentAccountSumByPaymentAccountId(string paymentAccountId);
        bool CheckExistById(string id);
    }
}