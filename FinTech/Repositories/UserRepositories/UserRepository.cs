using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Models.Data;

namespace FinTech.Repositories.UserRepositories
{
    public class UserRepository : IUserRepository
    {
        private readonly EsdpContext _context;

        public UserRepository(EsdpContext context)
        {
            _context = context;
        }

        public void Create(User item)
        {
            _context.Users.Add(item);
        }

        public void Update(User item)
        {
            _context.Users.Update(item);
        }

        public void SetDisabled(User item)
        {
            _context.Users.Remove(item);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public List<User> GetByCompanyId(string companyId)
        {
            return _context.Users.Where(u => u.Company.Id == companyId).ToList();
        }

        public User GetByUserName(string userName)
        {
            return _context.Users.FirstOrDefault(u => u.UserName == userName);
        }

        public bool CheckExistenceById(string id)
        {
            return _context.Users.Any(u => u.Id == id);
        }

        public User GetById(string id)
        {
            return _context.Users.FirstOrDefault(u => u.Id == id);
        }
    }
}