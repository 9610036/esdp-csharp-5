using System.Collections.Generic;
using FinTech.Models;

namespace FinTech.Repositories.UserRepositories
{
    public interface IUserRepository : IRepository<User>
    {
        public List<User> GetByCompanyId(string companyId);
        public User GetByUserName(string userName);
        public bool CheckExistenceById(string id);
        public User GetById(string id);
    }
}