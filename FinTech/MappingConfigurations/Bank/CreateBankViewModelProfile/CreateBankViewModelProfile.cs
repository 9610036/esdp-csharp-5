﻿using AutoMapper;
using FinTech.ViewModels.Banks;

namespace FinTech.MappingConfigurations.Bank.CreateBankViewModelProfile
{
    public class CreateBankViewModelProfile : Profile
    {
        public CreateBankViewModelProfile()
        {
            CreateMap<Models.Bank, CreateBankViewModel>()
                .ForMember(dest => dest.Bik, opt =>
                    opt.MapFrom(src => src.Bik))
                .ForMember(dst => dst.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ReverseMap();
            
            CreateMap<CreateBankViewModel, Models.Bank>()
                .ForMember(dest => dest.Bik, opt =>
                    opt.MapFrom(src => src.Bik))
                .ForMember(dst => dst.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ReverseMap();
        }
    }
}