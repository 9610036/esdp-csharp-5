﻿using AutoMapper;
using FinTech.ViewModels.Banks;

namespace FinTech.MappingConfigurations.Bank.BankViewModelProfile
{
    public class BankViewModelProfile : Profile
    {
        public BankViewModelProfile()
        {
            CreateMap<Models.Bank, BankViewModel>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Bik, opt =>
                    opt.MapFrom(src => src.Bik))
                .ForMember(dst => dst.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ForMember(dst => dst.IsActive, opt =>
                    opt.MapFrom(src => src.IsActive))
                .ReverseMap();
        }
    }
}