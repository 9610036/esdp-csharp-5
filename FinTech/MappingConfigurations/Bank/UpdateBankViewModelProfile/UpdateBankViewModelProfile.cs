﻿using AutoMapper;
using FinTech.ViewModels.Banks;

namespace FinTech.MappingConfigurations.Bank.UpdateBankViewModelProfile
{
    public class UpdateBankViewModelProfile : Profile
    {
        public UpdateBankViewModelProfile()
        {
            CreateMap<Models.Bank, UpdateBankViewModel>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Bik, opt =>
                    opt.MapFrom(src => src.Bik))
                .ForMember(dst => dst.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ReverseMap();
            CreateMap<UpdateBankViewModel, Models.Bank>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Bik, opt =>
                    opt.MapFrom(src => src.Bik))
                .ForMember(dst => dst.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ReverseMap();
        }
    }
}