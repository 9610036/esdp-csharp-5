﻿using AutoMapper;
using FinTech.MappingConfigurations.Resolvers;
using FinTech.ViewModels.PaymentAccounts;

namespace FinTech.MappingConfigurations.PaymentAccount.CreatePaymentAccountViewModelProfile
{
    public class CreatePaymentAccountViewModelProfile : Profile
    {
        public CreatePaymentAccountViewModelProfile()
        {
            CreateMap<CreatePaymentAccountViewModel, Models.PaymentAccount>()
                .ForMember(dest => dest.Iban, opt =>
                    opt.MapFrom(pa => pa.Iban))
                .ForMember(dest => dest.Amount, opt =>
                    opt.MapFrom(pa => pa.Amount))
                .ForMember(dest => dest.Bank, opt =>
                    opt.MapFrom<GetBankResolver>())
                .ForMember(dest => dest.Company, opt =>
                    opt.MapFrom<GetCompanyResolver>())
                .ForMember(dest => dest.Counterparty, opt =>
                    opt.MapFrom<GetCounterpartyResolver>())
                .ForMember(dest => dest.CurrencyType, opt =>
                    opt.MapFrom(pa => pa.CurrencyType))
                .ForMember(dest => dest.CreationDate, opt =>
                    opt.MapFrom(pa => pa.CreationDate))
                .ReverseMap();
        }
    }
}