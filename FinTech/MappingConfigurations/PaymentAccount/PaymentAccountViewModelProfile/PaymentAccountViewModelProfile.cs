using AutoMapper;
using FinTech.ViewModels.PaymentAccounts;

namespace FinTech.MappingConfigurations.PaymentAccount.PaymentAccountViewModelProfile
{
    public class PaymentAccountViewModelProfile : Profile
    {
        public PaymentAccountViewModelProfile()
        {
            CreateMap<PaymentAccountViewModel, Models.PaymentAccount>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(pa => pa.Id))
                .ForMember(dest => dest.Iban, opt =>
                    opt.MapFrom(pa => pa.Iban))
                .ForMember(dest => dest.Amount, opt =>
                    opt.MapFrom(pa => pa.Amount))
                .ForMember(dest => dest.Bank, opt =>
                    opt.MapFrom(pa => pa.Bank))
                .ForMember(dest => dest.IsActive, opt =>
                    opt.MapFrom(pa => pa.IsActive))
                .ForMember(dest => dest.Company, opt =>
                    opt.MapFrom(pa => pa.Company))
                .ForMember(dest => dest.Counterparty, opt =>
                    opt.MapFrom(pa => pa.Counterparty))
                .ForMember(dest => dest.CurrencyType, opt =>
                    opt.MapFrom(pa => pa.CurrencyType))
                .ForMember(dest => dest.CreationDate, opt =>
                    opt.MapFrom(pa => pa.CreationDate))
                .ReverseMap();
        }
    }
}