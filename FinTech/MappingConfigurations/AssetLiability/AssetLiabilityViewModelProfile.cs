using AutoMapper;
using FinTech.ViewModels.AssetLiabilities;

namespace FinTech.MappingConfigurations.AssetLiability
{
    public class AssetLiabilityViewModelProfile : Profile
    {
        public AssetLiabilityViewModelProfile()
        {
            CreateMap<Models.AssetLiability, AssetLiabilityViewModel>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(o => o.Id))
                .ForMember(dest => dest.CreationDate, opt =>
                    opt.MapFrom(o => o.CreationDate))
                .ForMember(dest => dest.Name, opt =>
                    opt.MapFrom(o => o.Name))
                .ForMember(dest => dest.Sum, opt =>
                    opt.MapFrom(o => o.Sum))
                .ForMember(dest => dest.Type, opt =>
                    opt.MapFrom(o => o.Type))
                .ForMember(dest => dest.IsActive, opt =>
                    opt.MapFrom(o => o.IsActive))
                .ForMember(dest => dest.CompanyId, opt =>
                    opt.MapFrom(o => o.Company.Id))
                .ForMember(dest => dest.AssetType, opt =>
                    opt.MapFrom(o => o.AssetType)).ReverseMap();
        }
    }
}