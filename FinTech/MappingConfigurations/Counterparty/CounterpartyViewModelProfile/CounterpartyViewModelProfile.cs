using AutoMapper;
using FinTech.ViewModels.Counterparties;

namespace FinTech.MappingConfigurations.Counterparty.CounterpartyViewModelProfile
{
    public class CounterpartyViewModelProfile : Profile
    {
        public CounterpartyViewModelProfile()
        {
            CreateMap<Models.Counterparty, CounterpartyViewModel>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(c => c.Id))
                .ForMember(dest => dest.Bin, opt =>
                    opt.MapFrom(c => c.Bin))
                .ForMember(dest => dest.Comment, opt =>
                    opt.MapFrom(c => c.Comment))
                .ForMember(dest => dest.Name, opt =>
                    opt.MapFrom(c => c.Name))
                .ForMember(dest => dest.ShortName, opt =>
                    opt.MapFrom(c => c.ShortName)).ReverseMap();
        }
    }
}