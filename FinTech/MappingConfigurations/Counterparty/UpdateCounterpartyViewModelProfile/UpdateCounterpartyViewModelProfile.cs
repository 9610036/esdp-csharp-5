﻿using AutoMapper;
using FinTech.MappingConfigurations.Resolvers;
using FinTech.ViewModels.Counterparties;

namespace FinTech.MappingConfigurations.Counterparty.UpdateCounterpartyViewModelProfile
{
    public class UpdateCounterpartyViewModelProfile : Profile
    {
        public UpdateCounterpartyViewModelProfile()
        {
            CreateMap<UpdateCounterpartyViewModel, Models.Counterparty>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Bin, opt =>
                    opt.MapFrom(src => src.Bin))
                .ForMember(dest => dest.Comment, opt =>
                    opt.MapFrom(src => src.Comment))
                .ForMember(dest => dest.Company, opt =>
                    opt.MapFrom<GetCompanyResolver>())
                .ForMember(dest => dest.ShortName, opt =>
                    opt.MapFrom(src => src.ShortName))
                .ReverseMap();
        }
    }
}