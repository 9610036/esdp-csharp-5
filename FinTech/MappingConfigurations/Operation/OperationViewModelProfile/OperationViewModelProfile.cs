using AutoMapper;
using FinTech.ViewModels.Operations;

namespace FinTech.MappingConfigurations.Operation.OperationViewModelProfile
{
    public class OperationViewModelProfile : Profile
    {
        public OperationViewModelProfile()
        {
            CreateMap<Models.Operation, OperationViewModel>()
                .ForMember(dst => dst.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.CreationDate, opt =>
                    opt.MapFrom(src => src.CreationDate))
                .ForMember(dst => dst.Sum, opt =>
                    opt.MapFrom(src => src.Sum))
                .ForMember(dst => dst.Comment, opt =>
                    opt.MapFrom(src => src.Comment))
                .ForMember(dst => dst.IsActive, opt =>
                    opt.MapFrom(src => src.IsActive))
                .ForMember(dst => dst.Company, opt =>
                    opt.MapFrom(src => src.Company))
                .ForMember(dst => dst.BudgetItem, opt =>
                    opt.MapFrom/*<GetBudgetItemResolver>*/(o => o.BudgetItem));

            CreateMap<OperationViewModel, Models.Operation>()
                .ForMember(o => o.Id, opt =>
                    opt.MapFrom(o => o.Id))
                .ForMember(o => o.Comment, opt =>
                    opt.MapFrom(o => o.Comment))
                .ForMember(o => o.Company, opt =>
                    opt.MapFrom/*<GetCompanyResolver>*/(o => o.Company))
                .ForMember(o => o.Counterparty, opt =>
                    opt.MapFrom(o => o.Counterparty))
                .ForMember(o => o.Sum, opt =>
                    opt.MapFrom(o => o.Sum))
                .ForMember(o => o.BudgetItem, opt =>
                    opt.MapFrom(o => o.BudgetItem))
                .ForMember(o => o.PaymentAccount, opt =>
                    opt.MapFrom(o => o.PaymentAccount))
                .ForMember(o => o.IsActive, opt =>
                    opt.MapFrom(o => o.IsActive))
                .ForMember(o => o.IsConfirmed, opt =>
                    opt.MapFrom(o => o.IsConfirmed))
                .ForMember(o => o.CreationDate, opt =>
                    opt.MapFrom(o => o.CreationDate))
                .ForMember(o => o.Type, opt =>
                    opt.MapFrom(o => o.Type));
        }
    }
}