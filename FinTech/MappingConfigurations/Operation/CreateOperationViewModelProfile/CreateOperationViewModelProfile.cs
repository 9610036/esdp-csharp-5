﻿using AutoMapper;
using FinTech.MappingConfigurations.Resolvers;
using FinTech.ViewModels.Operations;

namespace FinTech.MappingConfigurations.Operation.CreateOperationViewModelProfile
{
    public class CreateOperationViewModelProfile : Profile
    {
        public CreateOperationViewModelProfile()
        {
            CreateMap<CreateOperationViewModel, Models.Operation>()
                .ForMember(dest => dest.Company, opt =>
                    opt.MapFrom<GetCompanyForCreateOperationResolver>())
                .ForMember(dest => dest.BudgetItem, opt =>
                    opt.MapFrom<GetBudgetItemForCreateOperationResolver>())
                .ForMember(dest => dest.Counterparty, opt =>
                    opt.MapFrom<GetCounterpartyForCreateOperationResolver>())
                .ForMember(dest => dest.PaymentAccount, opt =>
                    opt.MapFrom<GetPaymentAccountForCreateOperationResolver>())
                .ForMember(dest => dest.CreationDate, opt =>
                    opt.MapFrom(src => src.CreationDate))
                .ForMember(dest => dest.Sum, opt =>
                    opt.MapFrom(src => src.Sum))
                .ForMember(dest => dest.Comment, opt =>
                    opt.MapFrom(src => src.Comment))
                .ForMember(dest => dest.IsConfirmed, opt =>
                    opt.MapFrom(src => src.IsConfirmed))
                .ForMember(dest => dest.Type, opt =>
                    opt.MapFrom(src => src.OperationType));
        }
    }
}