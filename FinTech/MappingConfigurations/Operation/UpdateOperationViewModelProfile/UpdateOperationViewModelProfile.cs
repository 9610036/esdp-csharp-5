﻿using AutoMapper;
using FinTech.MappingConfigurations.Resolvers;
using FinTech.ViewModels.Operations;

namespace FinTech.MappingConfigurations.Operation.UpdateOperationViewModelProfile
{
    public class UpdateOperationViewModelProfile : Profile
    {
        public UpdateOperationViewModelProfile()
        {
            CreateMap<UpdateOperationViewModel, Models.Operation>()
                .ForMember(dest => dest.BudgetItem, opt =>
                    opt.MapFrom<GetBudgetItemResolver>())
                .ForMember(dest => dest.Counterparty, opt =>
                    opt.MapFrom<GetCounterpartyResolver>())
                .ForMember(dest => dest.PaymentAccount, opt =>
                    opt.MapFrom<GetPaymentAccountResolver>())
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CreationDate, opt =>
                    opt.MapFrom(src => src.CreationDate))
                .ForMember(dest => dest.Sum, opt =>
                    opt.MapFrom(src => src.Sum))
                .ForMember(dest => dest.Comment, opt =>
                    opt.MapFrom(src => src.Comment))
                .ForMember(dest => dest.IsConfirmed, opt =>
                    opt.MapFrom(src => src.IsConfirmed)).ReverseMap();
        }
    }
}