﻿using AutoMapper;
using FinTech.MappingConfigurations.Resolvers;
using FinTech.ViewModels.BudgetItems;

namespace FinTech.MappingConfigurations.BudgetItem.BudgetItemViewModelProfile
{
    public class UpdateBudgetItemViewModelProfile : Profile
    {
        public UpdateBudgetItemViewModelProfile()
        {
            CreateMap<Models.BudgetItem, UpdateBudgetItemViewModel>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Type, opt =>
                    opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.IsActive, opt =>
                    opt.MapFrom(src => src.IsActive))
                .ForMember(dest => dest.CompanyBin, opt =>
                    opt.MapFrom(src => src.Company.Bin));
            
            CreateMap<UpdateBudgetItemViewModel, Models.BudgetItem>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Type, opt =>
                    opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.IsActive, opt =>
                    opt.MapFrom(src => src.IsActive))
                .ForMember(dest => dest.Company, opt =>
                    opt.MapFrom<GetCompanyResolver>());
        }
    }
}