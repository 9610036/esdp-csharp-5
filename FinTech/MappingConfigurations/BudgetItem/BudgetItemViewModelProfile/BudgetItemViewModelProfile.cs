﻿using AutoMapper;
using FinTech.ViewModels.BudgetItems;

namespace FinTech.MappingConfigurations.BudgetItem.BudgetItemViewModelProfile
{
    public class BudgetItemViewModelProfile : Profile
    {
        public BudgetItemViewModelProfile()
        {
            CreateMap<Models.BudgetItem, BudgetItemViewModel>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Type, opt =>
                    opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.Company, opt =>
                    opt.MapFrom(src => src.Company))
                .ForMember(dest => dest.IsActive, opt =>
                    opt.MapFrom(src => src.IsActive))
                .ForMember(dest => dest.AssetLiability, opt =>
                    opt.MapFrom(src => src.AssetLiability))
                .ReverseMap();
        }
    }
}