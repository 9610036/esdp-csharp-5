﻿using AutoMapper;
using FinTech.MappingConfigurations.Resolvers;
using FinTech.ViewModels.BudgetItems;

namespace FinTech.MappingConfigurations.BudgetItem.BudgetItemViewModelProfile
{
    public class CreateBudgetItemViewModelProfile : Profile
    {
        public CreateBudgetItemViewModelProfile()
        {
            CreateMap<Models.BudgetItem, CreateBudgetItemViewModel>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Type, opt =>
                    opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.CompanyBin, opt =>
                    opt.MapFrom(src => src.Company.Bin))
                .ForMember(dest => dest.IsActive, opt =>
                    opt.MapFrom(src => src.IsActive))

                .ForMember(dest => dest.AssetLiabilityId, opt => 
                    opt.MapFrom(src => src.AssetLiability.Id));
            
            CreateMap<CreateBudgetItemViewModel, Models.BudgetItem>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Type, opt =>
                    opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.IsActive, opt =>
                    opt.MapFrom(src => src.IsActive))
                .ForMember(dest => dest.Company, opt =>
                    opt.MapFrom<GetCompanyResolver>())
                .ForMember(dest => dest.AssetLiability, opt => 
                    opt.MapFrom<GetAssetLiabilityResolver>());
        }
    }
}