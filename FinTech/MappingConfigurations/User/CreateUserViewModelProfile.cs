using AutoMapper;
using FinTech.ViewModels.Users;

namespace FinTech.MappingConfigurations.User
{
    public class CreateUserViewModelProfile : Profile
    {
        public CreateUserViewModelProfile()
        {
            CreateMap<CreateUserViewModel, Models.User>()
                .ForMember(dest => dest.Email, opt =>
                    opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.UserName, opt =>
                    opt.MapFrom(src => src.Email));
        }
    }
}