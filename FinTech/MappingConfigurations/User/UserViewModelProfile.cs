using AutoMapper;
using FinTech.ViewModels.Users;

namespace FinTech.MappingConfigurations.User
{
    public class UserViewModelProfile : Profile
    {
        public UserViewModelProfile()
        {
            CreateMap<Models.User, UserViewModel>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserName, opt =>
                    opt.MapFrom(src => src.UserName));
        }
    }
}