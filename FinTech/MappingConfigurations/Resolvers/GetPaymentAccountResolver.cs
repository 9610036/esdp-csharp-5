﻿using AutoMapper;
using FinTech.Repositories.PaymentAccountRepositories;
using FinTech.ViewModels.Operations;

namespace FinTech.MappingConfigurations.Resolvers
{
    public class GetPaymentAccountResolver : IValueResolver<UpdateOperationViewModel, Models.Operation, Models.PaymentAccount>
    {
        private readonly IPaymentAccountRepository _paymentAccountRepository;

        public GetPaymentAccountResolver(IPaymentAccountRepository paymentAccountRepository)
        {
            _paymentAccountRepository = paymentAccountRepository;
        }

        public Models.PaymentAccount Resolve(UpdateOperationViewModel source, Models.Operation destination, Models.PaymentAccount destMember,
            ResolutionContext context)
        {
            return _paymentAccountRepository.GetByPaymentAccountId(source.PaymentAccount.Id);
        }
    }
}