﻿using AutoMapper;
using FinTech.Repositories.CompanyRepositories;
using FinTech.ViewModels.Operations;

namespace FinTech.MappingConfigurations.Resolvers
{
    public class GetCompanyForCreateOperationResolver : IValueResolver<CreateOperationViewModel, Models.Operation, Models.Company>
    {
        private readonly ICompanyRepository _companyRepository;

        public GetCompanyForCreateOperationResolver(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }
        
        public Models.Company Resolve(CreateOperationViewModel source, Models.Operation destination, Models.Company destMember,
            ResolutionContext context)
        {
            return _companyRepository.GetByCompanyBin(source.CompanyBin);
        }
    }
}