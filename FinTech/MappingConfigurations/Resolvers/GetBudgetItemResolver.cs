﻿using AutoMapper;
using FinTech.Repositories.BudgetItemRepositories;
using FinTech.ViewModels.BudgetItems;
using FinTech.ViewModels.Operations;

namespace FinTech.MappingConfigurations.Resolvers
{
    public class GetBudgetItemResolver : 
        IValueResolver<UpdateOperationViewModel, Models.Operation, Models.BudgetItem>,
        IValueResolver<Models.Operation, OperationViewModel, BudgetItemViewModel>
    {
        private readonly IBudgetItemRepository _budgetItemRepository;
        private readonly IMapper _mapper;

        public GetBudgetItemResolver(IBudgetItemRepository budgetItemRepository, IMapper mapper)
        {
            _budgetItemRepository = budgetItemRepository;
            _mapper = mapper;
        }

        public Models.BudgetItem Resolve(UpdateOperationViewModel source, Models.Operation destination, Models.BudgetItem destMember,
            ResolutionContext context)
        {
            return _budgetItemRepository.GetByBudgetItemId(source.BudgetItem.Id);
        }

        public BudgetItemViewModel Resolve(Models.Operation source, OperationViewModel destination, BudgetItemViewModel destMember,
            ResolutionContext context)
        {
            var budgetItem = _budgetItemRepository.GetByBudgetItemId(source.BudgetItem.Id);
            return _mapper.Map<BudgetItemViewModel>(budgetItem);
        }
    }
}