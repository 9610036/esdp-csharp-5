using AutoMapper;
using FinTech.Repositories.BankRepositories;
using FinTech.ViewModels.PaymentAccounts;

namespace FinTech.MappingConfigurations.Resolvers
{
    public class GetBankResolver : 
        IValueResolver<UpdatePaymentAccountViewModel, Models.PaymentAccount, Models.Bank>, 
        IValueResolver<CreatePaymentAccountViewModel, Models.PaymentAccount, Models.Bank>
    {
        private readonly IBankRepository _bankRepository;

        public GetBankResolver(IBankRepository bankRepository)
        {
            _bankRepository = bankRepository;
        }

        public Models.Bank Resolve(UpdatePaymentAccountViewModel source, Models.PaymentAccount destination, Models.Bank destMember, ResolutionContext context)
        {
            return _bankRepository.GetBankById(source.BankId);
        }

        public Models.Bank Resolve(CreatePaymentAccountViewModel source, Models.PaymentAccount destination, Models.Bank destMember,
            ResolutionContext context)
        {
            return _bankRepository.GetBankById(source.BankId);
        }
    }
}