﻿using AutoMapper;
using FinTech.Repositories.AssetLiabilityRepositories;
using FinTech.ViewModels.BudgetItems;

namespace FinTech.MappingConfigurations.Resolvers
{
    public class GetAssetLiabilityResolver : IValueResolver<CreateBudgetItemViewModel, Models.BudgetItem, Models.AssetLiability>
    {
        private readonly IAssetLiabilityRepository _assetLiabilityRepository;

        public GetAssetLiabilityResolver(IAssetLiabilityRepository assetLiabilityRepository)
        {
            _assetLiabilityRepository = assetLiabilityRepository;
        }

        public Models.AssetLiability Resolve(CreateBudgetItemViewModel source, Models.BudgetItem destination, Models.AssetLiability destMember,
            ResolutionContext context)
        {
            return _assetLiabilityRepository.GetById(source.AssetLiabilityId);
        }
    }
}