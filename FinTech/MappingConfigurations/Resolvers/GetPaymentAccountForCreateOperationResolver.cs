﻿using AutoMapper;
using FinTech.Repositories.PaymentAccountRepositories;
using FinTech.ViewModels.Operations;

namespace FinTech.MappingConfigurations.Resolvers
{
    public class GetPaymentAccountForCreateOperationResolver : IValueResolver<CreateOperationViewModel, Models.Operation, Models.PaymentAccount>
    {
        private readonly IPaymentAccountRepository _paymentAccountRepository;

        public GetPaymentAccountForCreateOperationResolver(IPaymentAccountRepository paymentAccountRepository)
        {
            _paymentAccountRepository = paymentAccountRepository;
        }
        
        public Models.PaymentAccount Resolve(CreateOperationViewModel source, Models.Operation destination, Models.PaymentAccount destMember,
            ResolutionContext context)
        {
            return _paymentAccountRepository.GetByPaymentAccountId(source.PaymentAccountId);
        }
    }
}