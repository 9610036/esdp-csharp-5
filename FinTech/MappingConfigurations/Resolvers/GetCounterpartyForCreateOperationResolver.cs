﻿using AutoMapper;
using FinTech.Repositories.CounterpartyRepositories;
using FinTech.ViewModels.Operations;

namespace FinTech.MappingConfigurations.Resolvers
{
    public class GetCounterpartyForCreateOperationResolver : IValueResolver<CreateOperationViewModel, Models.Operation, Models.Counterparty>
    {
        private readonly ICounterpartyRepository _counterpartyRepository;

        public GetCounterpartyForCreateOperationResolver(ICounterpartyRepository counterpartyRepository)
        {
            _counterpartyRepository = counterpartyRepository;
        }
        public Models.Counterparty Resolve(CreateOperationViewModel source, Models.Operation destination, Models.Counterparty destMember,
            ResolutionContext context)
        {
            return _counterpartyRepository.GetByCounterpartyId(source.CounterpartyId);
        }
    }
}