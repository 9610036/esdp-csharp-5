using AutoMapper;
using FinTech.Repositories.CompanyRepositories;
using FinTech.ViewModels.AssetLiabilities;
using FinTech.ViewModels.BudgetItems;
using FinTech.ViewModels.Counterparties;
using FinTech.ViewModels.PaymentAccounts;

namespace FinTech.MappingConfigurations.Resolvers
{
    public class GetCompanyResolver : 
        IValueResolver<AssetLiabilityViewModel, Models.AssetLiability, Models.Company>, 
        IValueResolver<UpdatePaymentAccountViewModel, Models.PaymentAccount, Models.Company?>, 
        IValueResolver<CreatePaymentAccountViewModel, Models.PaymentAccount, Models.Company?>, 
        IValueResolver<CreateBudgetItemViewModel, Models.BudgetItem, Models.Company>, 
        IValueResolver<UpdateBudgetItemViewModel, Models.BudgetItem, Models.Company>,
        IValueResolver<UpdateCounterpartyViewModel, Models.Counterparty, Models.Company>
        /*IValueResolver<OperationViewModel, Models.Operation, Models.Company>*/
    {
        private readonly ICompanyRepository _companyRepository;

        public GetCompanyResolver(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public Models.Company Resolve(AssetLiabilityViewModel source, Models.AssetLiability destination, Models.Company destMember,
            ResolutionContext context)
        {
            return _companyRepository.GetByCompanyId(source.CompanyId);
        }

        public Models.Company? Resolve(UpdatePaymentAccountViewModel source, Models.PaymentAccount destination, Models.Company? destMember,
            ResolutionContext context)
        {
            return _companyRepository.GetByCompanyId(source.CompanyId);
        }

        public Models.Company? Resolve(CreatePaymentAccountViewModel source, Models.PaymentAccount destination, Models.Company? destMember,
            ResolutionContext context)
        {
            return _companyRepository.GetByCompanyId(source.CompanyId);
        }

        public Models.Company Resolve(CreateBudgetItemViewModel source, Models.BudgetItem destination, Models.Company destMember,
            ResolutionContext context)
        {
            return _companyRepository.GetByCompanyBin(source.CompanyBin);
        }

        public Models.Company Resolve(UpdateBudgetItemViewModel source, Models.BudgetItem destination, Models.Company destMember,
            ResolutionContext context)
        {
            return _companyRepository.GetByCompanyBin(source.CompanyBin);
        }
        public Models.Company Resolve(UpdateCounterpartyViewModel source, Models.Counterparty destination, Models.Company destMember,
            ResolutionContext context)
        {
            return _companyRepository.GetByCompanyBin(source.CompanyId);
        }

        /*public Models.Company Resolve(OperationViewModel source, Models.Operation destination, Models.Company destMember, ResolutionContext context)
        {
            return _companyRepository.GetByCompanyId(source.CompanyId);
        }*/
    }
}