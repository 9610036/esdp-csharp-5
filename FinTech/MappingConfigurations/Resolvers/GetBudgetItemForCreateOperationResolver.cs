﻿using AutoMapper;
using FinTech.Repositories.BudgetItemRepositories;
using FinTech.ViewModels.Operations;

namespace FinTech.MappingConfigurations.Resolvers
{
    public class GetBudgetItemForCreateOperationResolver : IValueResolver<CreateOperationViewModel, Models.Operation, Models.BudgetItem>
    {

            private readonly IBudgetItemRepository _budgetItemRepository;

            public GetBudgetItemForCreateOperationResolver(IBudgetItemRepository budgetItemRepository)
            {
                _budgetItemRepository = budgetItemRepository;
            }
        
            public Models.BudgetItem Resolve(CreateOperationViewModel source, Models.Operation destination, Models.BudgetItem destMember,
                ResolutionContext context)
            {
                return _budgetItemRepository.GetByBudgetItemId(source.BudgetItemId);
            }
        
    }
}