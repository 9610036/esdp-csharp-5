﻿using AutoMapper;
using FinTech.Repositories.CounterpartyRepositories;
using FinTech.ViewModels.Operations;
using FinTech.ViewModels.PaymentAccounts;

namespace FinTech.MappingConfigurations.Resolvers
{
    public class GetCounterpartyResolver : 
        IValueResolver<UpdateOperationViewModel, Models.Operation, Models.Counterparty>, 
        IValueResolver<UpdatePaymentAccountViewModel, Models.PaymentAccount, Models.Counterparty?>, 
        IValueResolver<CreatePaymentAccountViewModel, Models.PaymentAccount, Models.Counterparty?>
    {
        private readonly ICounterpartyRepository _counterpartyRepository;

        public GetCounterpartyResolver(ICounterpartyRepository counterpartyRepository)
        {
            _counterpartyRepository = counterpartyRepository;
        }

        public Models.Counterparty Resolve(UpdateOperationViewModel source, Models.Operation destination, Models.Counterparty destMember,
            ResolutionContext context)
        {
            return _counterpartyRepository.GetByCounterpartyId(source.Counterparty.Id);
        }

        public Models.Counterparty? Resolve(UpdatePaymentAccountViewModel source, Models.PaymentAccount destination, Models.Counterparty? destMember,
            ResolutionContext context)
        {
            return _counterpartyRepository.GetByCounterpartyId(source.CounterpartyId);
        }

        public Models.Counterparty? Resolve(CreatePaymentAccountViewModel source, Models.PaymentAccount destination, Models.Counterparty? destMember,
            ResolutionContext context)
        {
            return _counterpartyRepository.GetByCounterpartyId(source.CounterpartyId);
        }
    }
}