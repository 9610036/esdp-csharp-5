using AutoMapper;
using FinTech.ViewModels.Companies;

namespace FinTech.MappingConfigurations.Company.CompanyViewModelProfile
{
    public class CompanyViewModelProfile : Profile
    {
        public CompanyViewModelProfile()
        {
            CreateMap<CompanyViewModel, Models.Company>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(c => c.Id))
                .ForMember(dest => dest.Bin, opt =>
                    opt.MapFrom(c => c.Bin))
                .ForMember(dest => dest.Email, opt =>
                    opt.MapFrom(c => c.Email))
                .ForMember(dest => dest.Name, opt =>
                    opt.MapFrom(c => c.Name))
                .ForMember(dest => dest.Phone, opt =>
                    opt.MapFrom(c => c.Phone))
                .ReverseMap();
        }
    }
}