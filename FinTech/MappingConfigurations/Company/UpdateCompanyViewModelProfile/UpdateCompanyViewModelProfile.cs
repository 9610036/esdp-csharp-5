﻿using AutoMapper;
using FinTech.ViewModels.Companies;

namespace FinTech.MappingConfigurations.Company.UpdateCompanyViewModelProfile
{
    public class UpdateCompanyViewModelProfile : Profile
    {
        public UpdateCompanyViewModelProfile()
        {
            CreateMap<Models.Company, UpdateCompanyViewModel>()
                .ForMember(dest => dest.Id, opt =>
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt =>
                    opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Email, opt =>
                    opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Bin, opt =>
                    opt.MapFrom(src => src.Bin))
                .ForMember(dest => dest.Phone, opt =>
                    opt.MapFrom(src => src.Phone)).ReverseMap();
        }
    }
}