﻿using AutoMapper;
using FinTech.ViewModels.Account;

namespace FinTech.MappingConfigurations.Account
{
    public class RegisterViewModelProfile : Profile
    {
        public RegisterViewModelProfile()
        {
            CreateMap<RegisterViewModel, Models.User>()
                .ForMember(dst => dst.Email, opt =>
                    opt.MapFrom(src => src.Email))
                .ForMember(dst => dst.UserName, opt =>
                    opt.MapFrom(src => src.Email));
        }
    }
}