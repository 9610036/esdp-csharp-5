using System;
using FinTech.Models.Data;
using FinTech.Services.CompanyService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FinTech
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            using var scope = host.Services.CreateScope();
            try
            {
              
                var context = scope.ServiceProvider.GetService<EsdpContext>();
                var companyService = scope.ServiceProvider.GetService<ICompanyService>();
                DataSeeding.SeedBank(context);
                DataSeeding.SeedCompany(context);
                DataSeeding.SeedSettings(context, companyService);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occured while seeding data");
            }
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}