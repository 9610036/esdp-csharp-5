﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Repositories.OperationRepositories;
using FinTech.Services.BudgetItemService;
using FinTech.Services.OperationService;
using FinTech.ViewModels.Operations;
using FinTech.ViewModels.ProfitAndLoses;

namespace FinTech.Services.ProfitAndLosesServices
{
    public class ProfitAndLosesService : IProfitAndLosesService
    {
        private readonly IOperationRepository _operationRepository;
        private readonly IOperationService _operationService;
        private readonly IBudgetItemService _budgetItemService;
        private readonly IMapper _mapper;

        public ProfitAndLosesService(
            IOperationRepository operationRepository,
            IOperationService operationService,
            IBudgetItemService budgetItemService,
            IMapper mapper)
        {
            _operationRepository = operationRepository;
            _operationService = operationService;
            _budgetItemService = budgetItemService;
            _mapper = mapper;
        }

        public ProfitAndLosesViewModel GetProfitAndLosesViewModel(Company company, DateTime start, DateTime finish)
        {
            ProfitAndLosesViewModel model = new ProfitAndLosesViewModel();

            model.CompanyId = company.Id;
            var operations = _operationRepository.GetListByCompanyIdFromDateStartToDateFinish(company, start, finish);
            var allPermanentOperations = operations.Where(o => o.BudgetItem.Type == BudgetItemTypes.Permanent).ToList();
            var allVariableOperations = operations.Where(o => o.BudgetItem.Type == BudgetItemTypes.Variable).ToList();
            var allOtherOperations = operations.Where(o => o.BudgetItem.Type == BudgetItemTypes.Expense).ToList();
            var allIncomeOperations = operations.Where(o => o.BudgetItem.Type == BudgetItemTypes.Income).ToList();

            var permanentOperations = _operationService.GetSumAndGroupByMonthAndNameList(allPermanentOperations);
            var variableOperations = _operationService.GetSumAndGroupByMonthAndNameList(allVariableOperations);
            var otherOperations = _operationService.GetSumAndGroupByMonthAndNameList(allOtherOperations);
            var incomeOperations = _operationService.GetSumAndGroupByMonthAndNameList(allIncomeOperations);

            var incomeTotalSumOperations = incomeOperations.GroupBy(m => m.CreationDate.Month).Select(no =>
            {
                var newOperation = new Operation()
                {
                    Sum = no.Sum(o => o.Sum),
                    CreationDate = no.First().CreationDate
                };
                newOperation.Sum = no.Sum(n => n.Sum);
                return newOperation;
            }).ToList();
            
            var permanentTotalSumOperations = permanentOperations.GroupBy(m => m.CreationDate.Month).Select(no =>
            {
                var newOperation = new Operation()
                {
                    Sum = no.Sum(o => o.Sum),
                    CreationDate = no.First().CreationDate
                };
                newOperation.Sum = no.Sum(n => n.Sum);
                return newOperation;
            }).ToList();
            
            var otherTotalSumOperations = otherOperations.GroupBy(m => m.CreationDate.Month).Select(no =>
            {
                var newOperation = new Operation()
                {
                    Sum = no.Sum(o => o.Sum),
                    CreationDate = no.First().CreationDate
                };
                newOperation.Sum = no.Sum(n => n.Sum);
                return newOperation;
            }).ToList();
            
            var variableTotalSumOperations = variableOperations.GroupBy(m => m.CreationDate.Month).Select(no =>
            {
                var newOperation = new Operation()
                {
                    Sum = no.Sum(o => o.Sum),
                    CreationDate = no.First().CreationDate
                };
                newOperation.Sum = no.Sum(n => n.Sum);
                return newOperation;
            }).ToList();

            var profitOperations = new List<Operation>();

            for (int i = start.Month; i <= finish.Month; i++)
            {
                
                var incomeSum = incomeTotalSumOperations.Where(o => o.CreationDate.Month == i).Sum(o => o.Sum);
                var expenseSum = permanentTotalSumOperations.Where(o => o.CreationDate.Month == i).Sum(o => o.Sum) +
                                 otherTotalSumOperations.Where(o => o.CreationDate.Month == i).Sum(o => o.Sum) +
                                 variableTotalSumOperations.Where(o => o.CreationDate.Month == i).Sum(o => o.Sum);

                Operation newOperationTotal = new Operation()
                {
                    BudgetItem = new BudgetItem()
                    {
                        Name = "Чистая прибыль"
                    },
                    CreationDate = new DateTime(start.Year, i, 1),
                    Sum = incomeSum - expenseSum
                };
                Operation newOperationIncomeTotal = new Operation()
                {
                    BudgetItem = new BudgetItem()
                    {
                        Name = "Операционый доход"
                    },
                    CreationDate = new DateTime(start.Year, i, 1),
                    Sum = incomeSum
                };
                Operation newOperationExpenceTotal = new Operation()
                {
                    BudgetItem = new BudgetItem()
                    {
                        Name = "Операционный расход"
                    },
                    CreationDate = new DateTime(start.Year, i, 1),
                    Sum = expenseSum
                };
                Operation newOperationProfit = new Operation()
                {
                    BudgetItem = new BudgetItem()
                    {
                        Name = "Рентабельность"
                    },
                    CreationDate = new DateTime(start.Year, i, 1),
                    Sum = 0
                };
                
                try
                {
                    newOperationProfit.Sum = newOperationTotal.Sum * 100 / incomeSum;
                }
                catch (DivideByZeroException e)
                {
                }

                
                
                profitOperations.Add(newOperationIncomeTotal);
                profitOperations.Add(newOperationExpenceTotal);
                profitOperations.Add(newOperationTotal);
                profitOperations.Add(newOperationProfit);
            }

            
            var emptyOperations = new List<Operation>();

            for (int i = 1; i < 12; i++)
            {
                var emptyOperation = new Operation()
                {
                    CreationDate = new DateTime(start.Year, i, 1),
                    Sum = 0,
                    BudgetItem = new BudgetItem()
                    {
                        Name = " "
                    }
                };
                emptyOperations.Add(emptyOperation);
            }

            model.Operations = _mapper.Map<List<OperationViewModel>>(operations.ToList());

            model.ReportViewModels = new List<ProfitAndLosesReportViewModel>();


            var incomeOperationsReport = new ProfitAndLosesReportViewModel
            {
                Operations = incomeOperations.Count > 0
                    ? _mapper.Map<List<OperationViewModel>>(incomeOperations)
                    : _mapper.Map<List<OperationViewModel>>(emptyOperations),
                OperationsName = "Статьи доходов",
                ReportName = "Выручка",
                OperationsSum = incomeOperations.Sum(o => o.Sum),
                IsIncome = true
            };
            model.ReportViewModels.Add(incomeOperationsReport);

            var variableOperationsReport = new ProfitAndLosesReportViewModel
            {
                Operations = variableOperations.Count > 0
                    ? _mapper.Map<List<OperationViewModel>>(variableOperations)
                    : _mapper.Map<List<OperationViewModel>>(emptyOperations),
                OperationsName = "Статьи расходов",
                ReportName = "Переменные расходы",
                OperationsSum = variableOperations.Sum(o => o.Sum)
            };
            model.ReportViewModels.Add(variableOperationsReport);

            var permanentOperationsReport = new ProfitAndLosesReportViewModel
            {
                Operations = permanentOperations.Count > 0
                    ? _mapper.Map<List<OperationViewModel>>(permanentOperations)
                    : _mapper.Map<List<OperationViewModel>>(emptyOperations),
                OperationsName = "Статьи расходов",
                ReportName = "Постоянные расходы",
                OperationsSum = permanentOperations.Sum(o => o.Sum)
            };
            model.ReportViewModels.Add(permanentOperationsReport);

            var otherOperationsReport = new ProfitAndLosesReportViewModel
            {
                Operations = otherOperations.Count > 0
                    ? _mapper.Map<List<OperationViewModel>>(otherOperations)
                    : _mapper.Map<List<OperationViewModel>>(emptyOperations),
                OperationsName = "Статьи расходов",
                ReportName = "Прочие расходы",
                OperationsSum = otherOperations.Sum(o => o.Sum)
            };
            model.ReportViewModels.Add(otherOperationsReport);
            
            var profitOperationsReport = new ProfitAndLosesReportViewModel
            {
                Operations = profitOperations.Count > 0
                    ? _mapper.Map<List<OperationViewModel>>(profitOperations)
                    : _mapper.Map<List<OperationViewModel>>(emptyOperations),
                OperationsName = "Статьи",
                ReportName = "Прибыль/Рентабельность",
                OperationsSum = profitOperations.Sum(o => o.Sum)
            };
            model.ReportViewModels.Add(profitOperationsReport);

            model.TotalExpenseSum = model.ReportViewModels.Where(o => !o.IsIncome && o.ReportName != "Прибыль/Рентабельность").Sum(o => o.OperationsSum);
            model.TotalIncomeSum = model.ReportViewModels.Where(o => o.IsIncome && o.ReportName != "Прибыль/Рентабельность").Sum(o => o.OperationsSum);
            model.Profit = model.TotalIncomeSum - model.TotalExpenseSum;

            try
            {
                model.ProfitabilityPercent = model.Profit * 100 / model.TotalIncomeSum;
            }
            catch (DivideByZeroException e)
            {
                model.ProfitabilityPercent = 0;
            }

            model.FromDate = start;
            model.ToDate = finish;
            return model;
        }

        public MonthsPnLReportViewModel GetMonthPnLReportByCompanyBin(string companyBin, DateTime reportMonth)
        {
            MonthsPnLReportViewModel monthReport = new MonthsPnLReportViewModel()
            {
                MonthsReport = new List<MonthPnLReportViewModel>(),
                DateOfReport = reportMonth
            };
            var operationsList = _operationRepository.GetMonthOperationsListByCompanyBin(companyBin, reportMonth);
            var operations = _mapper.Map<List<OperationViewModel>>(operationsList);

            foreach (var operation in operations
                .Where(o => o.IsActive && o.IsConfirmed && o.Type == OperationTypes.Operational)
                .GroupBy(o => o.BudgetItem.Name))
            {
                MonthPnLReportViewModel model = new MonthPnLReportViewModel()
                {
                    BudgetItemName = operation.Key,
                    Sum = operation
                        .Where(o => o.BudgetItem.Name == operation.Key)
                        .Sum(o => o.Sum),
                    CompanyBin = companyBin,
                    BudgetItemType = operations.Find(o => o.BudgetItem.Name == operation.Key).BudgetItem.Type
                };
                monthReport.MonthsReport.Add(model);
            }

            ;
            var profit = monthReport.MonthsReport.Where(o => o.BudgetItemType == BudgetItemTypes.Income)
                .Sum(o => o.Sum);
            var loses = monthReport.MonthsReport.Where(o => o.BudgetItemType != BudgetItemTypes.Income).Sum(o => o.Sum);
            monthReport.OperatingProfit = profit - loses;
            if (profit != 0)
            {
                monthReport.Profitability = Math.Round(monthReport.OperatingProfit * 100 / profit, 2);
            }

            return monthReport;
        }

        public PnLViewModel GetPnLByMonths(int reportYear, string companyBin)
        {
            PnLViewModel model = new PnLViewModel()
            {
                YearPnLReport = new List<MonthsPnLReportViewModel>()
            };
            var monthsList = GetMonthsList(reportYear);
            foreach (var month in monthsList)
            {
                model.YearPnLReport.Add(GetMonthPnLReportByCompanyBin(companyBin, month));
            }

            return model;
        }

        public List<DateTime> GetMonthsList(int requiredYear)
        {
            int[] monthsNum = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            List<DateTime> monthsList = new List<DateTime>();
            foreach (var monthNum in monthsNum)
            {
                monthsList.Add(new DateTime(requiredYear, monthNum, 1, 1, 0, 0));
            }

            return monthsList;
        }

        public decimal GetProfitAndLosesViewModelOperationsSum(ProfitAndLosesViewModel model,
            BudgetItemTypes budgetType)
        {
            try
            {
                if (budgetType == BudgetItemTypes.Income)
                    return model.Operations
                        .Where(o => o.BudgetItem.Type == budgetType)
                        .Sum(o => o.Sum);
                return model.Operations
                    .Where(o => o.BudgetItem.Type != budgetType)
                    .Sum(o => o.Sum);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public decimal GetViewModelOperationsSumByBudgetTypeName(ProfitAndLosesViewModel model, string budgetTypeName)
        {
            try
            {
                return model.Operations
                    .Where(o => o.BudgetItem.Name == budgetTypeName)
                    .Sum(o => o.Sum);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public decimal GetProfitAndLosesDifferenceByCompanyIdAndYear(string companyId, DateTime year)
        {
            var profits = _operationRepository.GetListByCompanyId(companyId)
                .Where(o => o.IsActive &&
                            o.IsConfirmed &&
                            o.CreationDate.Year <= year.Year &&
                            o.BudgetItem.Type == BudgetItemTypes.Income)
                .Sum(o => o.Sum);
            var loses = _operationRepository.GetListByCompanyId(companyId)
                .Where(o => o.IsActive &&
                            o.IsConfirmed &&
                            o.CreationDate.Year <= year.Year &&
                            o.BudgetItem.Type != BudgetItemTypes.Income)
                .Sum(o => o.Sum);
            return profits - loses;
        }
    }
}