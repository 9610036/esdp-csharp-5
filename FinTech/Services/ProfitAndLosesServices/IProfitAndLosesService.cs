﻿using System;
using FinTech.Enums;
using System.Collections.Generic;
using FinTech.Models;
using FinTech.ViewModels.ProfitAndLoses;

namespace FinTech.Services.ProfitAndLosesServices
{
    public interface IProfitAndLosesService
    {
        ProfitAndLosesViewModel GetProfitAndLosesViewModel(Company company, DateTime start, DateTime finish);
        decimal GetProfitAndLosesViewModelOperationsSum(ProfitAndLosesViewModel model, BudgetItemTypes budgetType);
        MonthsPnLReportViewModel GetMonthPnLReportByCompanyBin(string companyBin, DateTime reportMonth);
        PnLViewModel GetPnLByMonths(int reportYear, string companyBin);
        List<DateTime> GetMonthsList(int requiredYear);
        decimal GetViewModelOperationsSumByBudgetTypeName(ProfitAndLosesViewModel model, string budgetTypeName);
        decimal GetProfitAndLosesDifferenceByCompanyIdAndYear(string companyId, DateTime year);

    }
}