using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FinTech.Models;
using FinTech.Repositories.BankRepositories;
using FinTech.ViewModels.Banks;


namespace FinTech.Services.BankService
{
    public class BankService : IBankService
    {
        private readonly IBankRepository _bankRepository;
        private readonly IMapper _mapper;

        public BankService(IBankRepository bankRepository, IMapper mapper)
        {
            _bankRepository = bankRepository;
            _mapper = mapper;
        }

        public Bank GetFirstOrDefaultBankById(string id)
        {
            return _bankRepository.GetBankById(id);
        }

        public BankViewModel GetBankDetails(string id)
        {
             var bank = GetFirstOrDefaultBankById(id);
             return _mapper.Map<BankViewModel>(bank);
        }

        public UpdateBankViewModel GetUpdateBankViewModelById(string id)
        {
            var bank = GetFirstOrDefaultBankById(id);
            return _mapper.Map<UpdateBankViewModel>(bank);
        }

        public List<BankViewModel> GetBankViewModels()
        {
            var banks = _bankRepository.GetListBank().Where(b => b.IsActive);
            return _mapper.Map<List<BankViewModel>>(banks.ToList());
        }

        public bool CheckExistById(string id)
        {
            return _bankRepository.CheckBankExistById(id);
        }
    }
}