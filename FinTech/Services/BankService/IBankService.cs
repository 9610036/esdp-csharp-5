﻿using System.Collections.Generic;
using FinTech.Models;
using FinTech.ViewModels.Banks;

namespace FinTech.Services.BankService
{
    public interface IBankService
    {
        Bank GetFirstOrDefaultBankById(string id);
        BankViewModel GetBankDetails(string id); 
        UpdateBankViewModel GetUpdateBankViewModelById(string id);
        public List<BankViewModel> GetBankViewModels();
        bool CheckExistById(string id);
    }
}