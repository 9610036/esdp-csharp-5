﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using FinTech.Enums;
using FinTech.ViewModels.Balances;
using FinTech.ViewModels.CashFlows;
using FinTech.ViewModels.ProfitAndLoses;
using Microsoft.AspNetCore.Hosting;
using Microsoft.VisualBasic.FileIO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace FinTech.Services.ExcellReportsService
{
    public class ExcelReports
    {
        private readonly IWebHostEnvironment _hostEnvironment;
        private static string _fileDirPath;

        public ExcelReports(IWebHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        }

        public string PnlReport(ProfitAndLosesViewModel viewModel)
        {
            using (var excel = new ExcelPackage())
            {
                var page = excel.Workbook.Worksheets.Add(
                    $"PNL_report({viewModel.FromDate.ToString("Y")}-{viewModel.ToDate.ToString("Y")})");
                var monthStart = viewModel.FromDate;
                var monthRange = viewModel.ToDate.Month - (viewModel.FromDate.Month - 1);
                var currentLine = 1;


                foreach (var report in viewModel.ReportViewModels)
                {
                    var currentMonth = monthStart;
                    if (report.Operations != null && report.Operations?.Count > 0)
                    {
                        page.Cells[currentLine, 1].Value = "Период";
                        for (int i = 0; i < monthRange; i++)
                        {
                            page.Cells[currentLine, 2 + i].Value = currentMonth.ToString("MMMM yy");
                            currentMonth = currentMonth.AddMonths(1);
                        }

                        page.Cells[currentLine, monthRange + 2].Value = "Итого";

                        var reportMainHeader = page.Cells[currentLine, 1, currentLine, monthRange + 2];
                        reportMainHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        reportMainHeader.Style.Fill.BackgroundColor.SetColor(Color.Silver);
                        reportMainHeader.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        currentLine++;
                        page.Cells[currentLine, 1].Value = report.ReportName;
                        page.Cells[currentLine, 1].Style.Font.Color.SetColor(Color.White);
                        var tableNameRange = page.Cells[currentLine, 1, currentLine, monthRange + 2];
                        tableNameRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        tableNameRange.Style.Fill.BackgroundColor.SetColor(Color.RoyalBlue);

                        if (!report.Operations.Any(o => o.BudgetItem.Name.Contains("Рентабельность")))
                        {
                            currentLine++;
                            page.Cells[currentLine, 1].Value = report.OperationsName;
                            page.Cells[currentLine, 1].Style.Font.Color.SetColor(Color.White);
                            var operationsNameRange = page.Cells[currentLine, 1, currentLine, monthRange + 2];
                            operationsNameRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            operationsNameRange.Style.Fill.BackgroundColor.SetColor(Color.DimGray);
                        }
                        
                        currentLine++;
                        var operationsNames = report.Operations
                            .Where(o => o.BudgetItem.Name != "Рентабельность"
                                                                           && o.BudgetItem.Name != "Операционный расход" 
                                                                           && o.BudgetItem.Name != "Операционый доход" )
                            .GroupBy(po => po.BudgetItem.Name).ToList();

                        for (int i = 0; i < operationsNames.Count(); i++)
                        {
                            page.Cells[currentLine, 1].Value = operationsNames[i].Key;

                            currentMonth = monthStart;
                            for (int j = 0; j < monthRange; j++)
                            {
                                var monthValue = report.Operations.Where(o =>
                                    o.CreationDate.Month == currentMonth.Month &&
                                    o.BudgetItem.Name == operationsNames[i].Key).Sum(o => o.Sum);
                                page.Cells[currentLine, 2 + j].Value = monthValue;
                                page.Cells[currentLine, 2 + j].Style.Numberformat.Format = "#,##0.00";
                                currentMonth = currentMonth.AddMonths(1);
                            }

                            var monthTotal = report.Operations.Where(o => o.BudgetItem.Name == operationsNames[i].Key)
                                .Sum(o => o.Sum);
                            page.Cells[currentLine, monthRange + 2].Value = monthTotal;
                            page.Cells[currentLine, monthRange + 2].Style.Numberformat.Format = "#,##0.00";
                            currentLine++;
                        }

                        if (!report.Operations.Any(o => o.BudgetItem.Name.Contains("Рентабельность")))
                        {
                            page.Cells[currentLine, 1].Value = "Итого";
                            currentMonth = monthStart;
                            for (int i = 0; i < monthRange; i++)
                            {
                                page.Cells[currentLine, 2 + i].Value = report.Operations
                                    .Where(o => o.CreationDate.Month == currentMonth.Month).Sum(o => o.Sum);
                                page.Cells[currentLine, 2 + i].Style.Numberformat.Format = "#,##0.00";
                                currentMonth = currentMonth.AddMonths(1);
                            }

                            page.Cells[currentLine, 2 + monthRange].Value = report.OperationsSum;
                            page.Cells[currentLine, 2 + monthRange].Style.Numberformat.Format = "#,##0.00";
                            var totalRange = page.Cells[currentLine, 1, currentLine, 2 + monthRange];
                            totalRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            totalRange.Style.Fill.BackgroundColor.SetColor(Color.Teal);
                            totalRange.Style.Font.Color.SetColor(Color.White);
                        }
                        else
                        {
                            page.Cells[currentLine, 1].Value = "Рентабельность %";
                            currentMonth = monthStart;
                            for (int i = 0; i < monthRange; i++)
                            {
                                page.Cells[currentLine, 2 + i].Value = report.Operations
                                    .FirstOrDefault(d => d.CreationDate.Month == currentMonth.Month && d.BudgetItem.Name == "Рентабельность")?.Sum;
                                page.Cells[currentLine, 2 + i].Style.Numberformat.Format = "0.00";
                                currentMonth = currentMonth.AddMonths(1);
                            }

                            page.Cells[currentLine, 2 + monthRange].Value = viewModel.ProfitabilityPercent;
                            page.Cells[currentLine, 2 + monthRange].Style.Numberformat.Format = "#,##0.00";
                            var totalRange = page.Cells[currentLine, 1, currentLine, 2 + monthRange];
                            totalRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            totalRange.Style.Fill.BackgroundColor.SetColor(Color.Teal);
                            totalRange.Style.Font.Color.SetColor(Color.White); 
                        }

                    }

                    currentLine += 5;
                }

                if (viewModel.ReportViewModels.Any(o => o.IsIncome))
                {
                    page.Cells[currentLine, 1].Value = "Операционный доход";
                    page.Cells[currentLine, 2].Value = viewModel.TotalIncomeSum;
                    page.Cells[currentLine, 2].Style.Numberformat.Format = "#,##0.00";
                    currentLine++;

                    page.Cells[currentLine, 1].Value = "Операционный расход";
                    page.Cells[currentLine, 2].Value = viewModel.TotalExpenseSum;
                    page.Cells[currentLine, 2].Style.Numberformat.Format = "#,##0.00";
                    currentLine++;

                    page.Cells[currentLine, 1].Value = "Операционная прибыль";
                    page.Cells[currentLine, 2].Value = viewModel.Profit;
                    page.Cells[currentLine, 2].Style.Numberformat.Format = "#,##0.00";
                    currentLine++;

                    page.Cells[currentLine, 1].Value = "Рентабельность %";
                    page.Cells[currentLine, 2].Value = viewModel.ProfitabilityPercent;
                    page.Cells[currentLine, 2].Style.Numberformat.Format = "0.00";

                    page.Cells[currentLine - 3, 1, currentLine, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    page.Cells[currentLine - 3, 2, currentLine, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    page.Cells[currentLine, 1, currentLine, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }


                page.Cells.AutoFitColumns(0);

                var tempFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".xlsx";
                Directory.CreateDirectory(Path.Combine(_hostEnvironment.ContentRootPath, "VirtualFiles"));
                var filePath = Path.Combine(_hostEnvironment.ContentRootPath, "VirtualFiles", tempFileName);
                var xlFile = FileSystem.GetFileInfo(Path.Combine(filePath));
                excel.SaveAs(xlFile);
                return Path.Combine(tempFileName);
            }
        }

        public string CashFlowReport(CashFlowViewModel viewModel)
        {
            using (var excel = new ExcelPackage())
            {
                var page = excel.Workbook.Worksheets.Add(
                    $"CashFlow-{viewModel.Year}");
                var currentLine = 1;

                foreach (var type in Enum.GetValues(typeof(OperationTypes)))
                {
                    if ((int)type == 4)
                        continue;
                    page.Cells[currentLine, 1].Value = "Период";
                    for (int i = 1; i <= Enum.GetValues(typeof(Months)).Length; i++)
                    {
                        page.Cells[currentLine, i + 1].Value = EnumHelper.GetDescription((Months)i);
                    }

                    var reportMainHeader = page.Cells[currentLine, 1, currentLine,
                        Enum.GetValues(typeof(Months)).Length + 1];
                    reportMainHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    reportMainHeader.Style.Fill.BackgroundColor.SetColor(Color.Silver);
                    reportMainHeader.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    currentLine++;
                    page.Cells[currentLine, 1].Value =
                        EnumHelper.GetDescription((OperationTypes)type) + " деятельность";
                    for (int i = 1; i <= Enum.GetValues(typeof(Months)).Length; i++)
                    {
                        page.Cells[currentLine, i + 1].Value =
                            viewModel.IncomeOperations.Where(o => o.Type == (OperationTypes)type)
                                .Where(o => o.CreationDate.Month == i).Sum(o => o.Sum) - viewModel.ExpenseOperations
                                .Where(o => o.Type == (OperationTypes)type).Where(o => o.CreationDate.Month == i)
                                .Sum(o => o.Sum);
                    }

                    var tableNameRange = page.Cells[currentLine, 1, currentLine,
                        Enum.GetValues(typeof(Months)).Length + 1];
                    tableNameRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    tableNameRange.Style.Fill.BackgroundColor.SetColor(Color.RoyalBlue);
                    tableNameRange.Style.Font.Color.SetColor(Color.White);
                    tableNameRange.Style.Numberformat.Format = "#,##0.00";

                    currentLine++;
                    page.Cells[currentLine, 1].Value = "Поступления денежных средств";
                    page.Cells[currentLine, 1].Style.Font.Color.SetColor(Color.White);
                    var incomeOperationsNameRange = page.Cells[currentLine, 1, currentLine,
                        Enum.GetValues(typeof(Months)).Length + 1];
                    incomeOperationsNameRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    incomeOperationsNameRange.Style.Fill.BackgroundColor.SetColor(Color.DimGray);


                    if (viewModel.IncomeOperations.Where(o => o.Type == (OperationTypes)type).ToList().Count > 0)
                    {
                        foreach (var budgetItem in viewModel.IncomeOperations.Where(o => o.Type == (OperationTypes)type)
                            .GroupBy(o => o.BudgetItem.Name).Select(g => g.First()))
                        {
                            currentLine++;
                            page.Cells[currentLine, 1].Value = budgetItem.BudgetItem.Name;
                            for (int i = 1; i <= Enum.GetValues(typeof(Months)).Length; i++)
                            {
                                page.Cells[currentLine, i + 1].Value = viewModel.IncomeOperations
                                    .Where(o => o.Type == (OperationTypes)type).Where(o =>
                                        o.CreationDate.Month == i && o.BudgetItem.Name == budgetItem.BudgetItem.Name)
                                    .Sum(o => o.Sum);
                            }
                        }
                    }
                    else
                    {
                        currentLine++;
                        page.Cells[currentLine, 1].Value = "Движений ДС не было";
                    }

                    currentLine++;
                    page.Cells[currentLine, 1].Value = "Выбытие денежных средств";
                    page.Cells[currentLine, 1].Style.Font.Color.SetColor(Color.White);
                    var outOperationsNameRange = page.Cells[currentLine, 1, currentLine,
                        Enum.GetValues(typeof(Months)).Length + 1];
                    outOperationsNameRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    outOperationsNameRange.Style.Fill.BackgroundColor.SetColor(Color.DimGray);


                    if (viewModel.ExpenseOperations.Where(o => o.Type == (OperationTypes)type).ToList().Count > 0)
                    {
                        foreach (var budgetItem in viewModel.ExpenseOperations
                            .Where(o => o.Type == (OperationTypes)type).GroupBy(o => o.BudgetItem.Name)
                            .Select(g => g.First()))
                        {
                            currentLine++;
                            page.Cells[currentLine, 1].Value = budgetItem.BudgetItem.Name;
                            for (int i = 1; i <= Enum.GetValues(typeof(Months)).Length; i++)
                            {
                                page.Cells[currentLine, i + 1].Value = viewModel.ExpenseOperations
                                    .Where(o => o.Type == (OperationTypes)type).Where(o =>
                                        o.CreationDate.Month == i && o.BudgetItem.Name == budgetItem.BudgetItem.Name)
                                    .Sum(o => o.Sum);
                            }
                        }
                    }
                    else
                    {
                        currentLine++;
                        page.Cells[currentLine, 1].Value = "Движений ДС не было";
                    }


                    currentLine += 5;
                }

                var resultSum = viewModel.EndSum - viewModel.StartSum;
                page.Cells[currentLine, 1].Value = resultSum < 0
                    ? "ИТОГО УМЕНЬШЕНИЕ ДЕНЕЖНЫХ СРЕДСТВ:"
                    : "ИТОГО УВЕЛИЧЕНИЕ ДЕНЕЖНЫХ СРЕДСТВ:";
                page.Cells[currentLine, 2].Value = Math.Abs(resultSum);
                page.Cells[currentLine, 2].Style.Numberformat.Format = "#,##0.00";
                currentLine++;

                page.Cells[currentLine, 1].Value = "ДС на начало периода:";
                page.Cells[currentLine, 2].Value = viewModel.StartSum;
                page.Cells[currentLine, 2].Style.Numberformat.Format = "#,##0.00";
                currentLine++;

                page.Cells[currentLine, 1].Value = "ДС на конец периода:";
                page.Cells[currentLine, 2].Value = viewModel.EndSum;
                page.Cells[currentLine, 2].Style.Numberformat.Format = "#,##0.00";

                page.Cells[currentLine - 2, 1, currentLine, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                page.Cells[currentLine - 2, 2, currentLine, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                page.Cells[currentLine, 1, currentLine, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                page.Cells.AutoFitColumns(0);

                var tempFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".xlsx";
                Directory.CreateDirectory(Path.Combine(_hostEnvironment.ContentRootPath, "VirtualFiles"));
                var filePath = Path.Combine(_hostEnvironment.ContentRootPath, "VirtualFiles", tempFileName);
                var xlFile = FileSystem.GetFileInfo(Path.Combine(filePath));
                excel.SaveAs(xlFile);
                return Path.Combine(tempFileName);
            }
        }

        public string BalanceReport(BalanceReportViewModel viewModel)
        {
            using (var excel = new ExcelPackage())
            {
                var page = excel.Workbook.Worksheets.Add(
                    $"Balance-{viewModel.YearOfReport.Year}");
                var currentLine = 1;
                var currentRow = 1;

                var firstTableLastLinePostion = 0;
                var secondTableLastLinePosition = 0;
                var lastTablesStartLine = 0;

                page.Cells[currentLine, currentRow].Value = "Активы";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.YearOfReport.Year;
                var activeHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                activeHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                activeHeader.Style.Fill.BackgroundColor.SetColor(Color.RoyalBlue);
                activeHeader.Style.Font.Color.SetColor(Color.White);
                currentLine++;

                page.Cells[currentLine, currentRow].Value = "Денежные средства";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.SumOnPaymentAccounts;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";;
                currentLine++;

                page.Cells[currentLine, currentRow].Value = "Дебиторская задолженность";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.AccountsReceivable;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";;
                currentLine++;

                page.Cells[currentLine, currentRow].Value = "Товарно-материальные запасы";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.InventorySum;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";;
                var inventorySumHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                inventorySumHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                inventorySumHeader.Style.Fill.BackgroundColor.SetColor(Color.DimGray);
                inventorySumHeader.Style.Font.Color.SetColor(Color.White);
                currentLine++;

                if (viewModel.InventoryAssets != null)
                {
                    foreach (var asset in viewModel.InventoryAssets)
                    {
                        page.Cells[currentLine, currentRow].Value = asset.Name;
                        page.Cells[currentLine, currentRow + 1].Value = asset.Sum;
                        page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                        currentLine++;
                    }
                }

                page.Cells[currentLine, currentRow].Value = "Краткосрочные активы";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.ShortTermAssets;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                var shortTermAssetsHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                shortTermAssetsHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                shortTermAssetsHeader.Style.Fill.BackgroundColor.SetColor(Color.Teal);
                shortTermAssetsHeader.Style.Font.Color.SetColor(Color.White);

                firstTableLastLinePostion = currentLine;
                currentLine = 1;
                currentRow = 4;

                page.Cells[currentLine, currentRow].Value = "Пассивы";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.YearOfReport.Year;
                var passiveHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                passiveHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                passiveHeader.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(220, 53, 69));
                passiveHeader.Style.Font.Color.SetColor(Color.White);
                currentLine++;

                page.Cells[currentLine, currentRow].Value = "Краткоср. фин. обязательства";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.ShortTermFinancialLiabilitiesSum;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                var shortTermFinancialLiabilitiesSumHeader =
                    page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                shortTermFinancialLiabilitiesSumHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                shortTermFinancialLiabilitiesSumHeader.Style.Fill.BackgroundColor.SetColor(Color.DimGray);
                shortTermFinancialLiabilitiesSumHeader.Style.Font.Color.SetColor(Color.White);
                currentLine++;

                if (viewModel.ShortTermFinancialLiabilities != null)
                {
                    foreach (var liability in viewModel.ShortTermFinancialLiabilities)
                    {
                        page.Cells[currentLine, currentRow].Value = liability.Name;
                        page.Cells[currentLine, currentRow + 1].Value = liability.Sum;
                        page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                        currentLine++;
                    }
                }

                page.Cells[currentLine, currentRow].Value = "Кредиторская задолженность";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.AccountsPayable;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                currentLine++;

                page.Cells[currentLine, currentRow].Value = "Краткосрочные пассивы";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.ShortTermLiabilities;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                var shortTermLiabilitiesHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                shortTermLiabilitiesHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                shortTermLiabilitiesHeader.Style.Fill.BackgroundColor.SetColor(Color.Teal);
                shortTermLiabilitiesHeader.Style.Font.Color.SetColor(Color.White);

                secondTableLastLinePosition = currentLine;
                currentRow = 1;
                lastTablesStartLine = (firstTableLastLinePostion > secondTableLastLinePosition
                    ? firstTableLastLinePostion
                    : secondTableLastLinePosition) + 2;
                currentLine = lastTablesStartLine;


                page.Cells[currentLine, currentRow].Value = "Активы";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.YearOfReport.Year;
                var secondActiveHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                secondActiveHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                secondActiveHeader.Style.Fill.BackgroundColor.SetColor(Color.RoyalBlue);
                secondActiveHeader.Style.Font.Color.SetColor(Color.White);
                currentLine++;

                page.Cells[currentLine, currentRow].Value = "Основные средства";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.MainCashSum;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                var secondInventorySumHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                secondInventorySumHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                secondInventorySumHeader.Style.Fill.BackgroundColor.SetColor(Color.DimGray);
                secondInventorySumHeader.Style.Font.Color.SetColor(Color.White);
                currentLine++;

                if (viewModel.MainCashAssets != null)
                {
                    foreach (var assets in viewModel.MainCashAssets)
                    {
                        page.Cells[currentLine, currentRow].Value = assets.Name;
                        page.Cells[currentLine, currentRow + 1].Value = assets.Sum;
                        page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                        currentLine++;
                    }
                }

                page.Cells[currentLine, currentRow].Value = "Долгосрочные активы";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.MainCashSum;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                var mainCashSumHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                mainCashSumHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                mainCashSumHeader.Style.Fill.BackgroundColor.SetColor(Color.Teal);
                mainCashSumHeader.Style.Font.Color.SetColor(Color.White);
                currentLine++;

                page.Cells[currentLine, currentRow].Value = "Баланс";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.TotalAssetsSum;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                var totalAssetsSumHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                totalAssetsSumHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                totalAssetsSumHeader.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                totalAssetsSumHeader.Style.Font.Color.SetColor(Color.White);

                currentRow = 4;
                currentLine = lastTablesStartLine;

                page.Cells[currentLine, currentRow].Value = "Пассивы";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.YearOfReport.Year;
                var secondaryPassiveHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                secondaryPassiveHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                secondaryPassiveHeader.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(220, 53, 69));
                secondaryPassiveHeader.Style.Font.Color.SetColor(Color.White);
                currentLine++;

                page.Cells[currentLine, currentRow].Value = "Уставный капитал";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.AuthorizedCapital;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                currentLine++;

                page.Cells[currentLine, currentRow].Value = "Нераспределенная прибыль";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.RetainedEarnings;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                currentLine++;

                page.Cells[currentLine, currentRow].Value = "Итого собств. капитал";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.TotalCapital;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                var totalCapitalHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                totalCapitalHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                totalCapitalHeader.Style.Fill.BackgroundColor.SetColor(Color.Teal);
                totalCapitalHeader.Style.Font.Color.SetColor(Color.White);
                currentLine++;
                
                page.Cells[currentLine, currentRow].Value = "Баланс";
                page.Cells[currentLine, currentRow + 1].Value = viewModel.TotalLiabilitiesSum;
                page.Cells[currentLine, currentRow + 1].Style.Numberformat.Format = "#,##0.00";
                var totalLiabilitiesSumHeader = page.Cells[currentLine, currentRow, currentLine, currentRow + 1];
                totalLiabilitiesSumHeader.Style.Fill.PatternType = ExcelFillStyle.Solid;
                totalLiabilitiesSumHeader.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                totalLiabilitiesSumHeader.Style.Font.Color.SetColor(Color.White);


                page.Cells.AutoFitColumns(10);
                // page.Column(3).Width = 10;

                var tempFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".xlsx";
                Directory.CreateDirectory(Path.Combine(_hostEnvironment.ContentRootPath, "VirtualFiles"));
                var filePath = Path.Combine(_hostEnvironment.ContentRootPath, "VirtualFiles", tempFileName);
                var xlFile = FileSystem.GetFileInfo(Path.Combine(filePath));
                excel.SaveAs(xlFile);
                return Path.Combine(tempFileName);
            }
        }
    }
}