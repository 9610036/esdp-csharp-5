﻿using System.Collections.Generic;
using FinTech.Models;
using FinTech.ViewModels.Companies;

namespace FinTech.Services.CompanyService
{
    public interface ICompanyService
    {
        Company GetCompanyByBin(string bin);
        Company GetCompanyById(string id);
        Company GetCompanyFromUser(User user);
        void UpdateCompany(UpdateCompanyViewModel company);
        void DeleteCompany(Company company);
        public List<CompanyViewModel> GetAllCompanies();
        public void Restore(Company company);
    }
}