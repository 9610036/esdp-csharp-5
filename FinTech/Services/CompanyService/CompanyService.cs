﻿using System.Collections.Generic;
using AutoMapper;
using FinTech.Models;
using FinTech.Repositories.CompanyRepositories;
using FinTech.Services.UserService;
using FinTech.ViewModels.Companies;

namespace FinTech.Services.CompanyService
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public CompanyService(
            ICompanyRepository companyRepository, 
            IMapper mapper, 
            IUserService userService)
        {
            _companyRepository = companyRepository;
            _mapper = mapper;
            _userService = userService;
        }


        public Company GetCompanyByBin(string bin)
        {
            return _companyRepository.GetByCompanyBin(bin);
        }

        public Company GetCompanyById(string id)
        {
            return _companyRepository.GetByCompanyId(id);
        }

        public Company GetCompanyFromUser(User user)
        {
            return user.Company;
        }

        public void UpdateCompany(UpdateCompanyViewModel model)
        {
            var company = model.ConvertToCompany();
            _companyRepository.Update(company);
            _companyRepository.Save();
        }

        public void DeleteCompany(Company company)
        {
            company.IsActive = false;
            _userService.DeleteAll(company);
            _companyRepository.Save();
        }

        public void Restore(Company company)
        {
            company.IsActive = true;
            _userService.RestoreAll(company);
            _companyRepository.Save();
        }

        public List<CompanyViewModel> GetAllCompanies()
        {
            return _mapper.Map<List<CompanyViewModel>>(_companyRepository.GetAllCompanies());
        }
    }
}