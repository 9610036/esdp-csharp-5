using System.Collections.Generic;
using FinTech.Models;
using FinTech.ViewModels.Users;

namespace FinTech.Services.UserService
{
    public interface IUserService
    {
        public List<UserViewModel> GetViewModelsByCompanyId(string companyId);
        public User GetFromViewModel(CreateUserViewModel model);
        public User GetByUserName(string userName);
        public bool CheckExistenceById(string id);
        public UserEditViewModel GetEditViewModelById(string id);
        public void Edit(UserEditViewModel model);
        public void Delete(string id);
        public void DeleteAll(Company company);
        public void RestoreAll(Company company);
    }
}