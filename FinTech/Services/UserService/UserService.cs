using System.Collections.Generic;
using AutoMapper;
using FinTech.Models;
using FinTech.Repositories.UserRepositories;
using FinTech.ViewModels.Users;

namespace FinTech.Services.UserService
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(
            IUserRepository userRepository, 
            IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public List<UserViewModel> GetViewModelsByCompanyId(string companyId)
        {
            List<UserViewModel> users = _mapper.Map<List<UserViewModel>>(_userRepository.GetByCompanyId(companyId));
            return users;
        }

        public User GetFromViewModel(CreateUserViewModel model)
        {
            return _mapper.Map<User>(model);
        }

        public User GetByUserName(string userName)
        {
            return _userRepository.GetByUserName(userName);
        }

        public bool CheckExistenceById(string id)
        {
            return _userRepository.CheckExistenceById(id);
        }

        public UserEditViewModel GetEditViewModelById(string id)
        {
            var user = _userRepository.GetById(id);
            UserEditViewModel model = new UserEditViewModel()
            {
                Id = user.Id,
                Email = user.Email
            };
            return model;
        }

        public void Edit(UserEditViewModel model)
        {
            var user = _userRepository.GetById(model.Id);
            user.Email = model.Email;
            user.UserName = model.Email;
            _userRepository.Save();
        }

        public void Delete(string id)
        {
            var user = _userRepository.GetById(id);
            _userRepository.SetDisabled(user);
            _userRepository.Save();
        }

        public void DeleteAll(Company company)
        {
            var users = _userRepository.GetByCompanyId(company.Id);
            foreach (var user in users)
            {
                user.IsActive = false;
            }
            _userRepository.Save();
        }
        
        public void RestoreAll(Company company)
        {
            var users = _userRepository.GetByCompanyId(company.Id);
            foreach (var user in users)
            {
                user.IsActive = true;
            }
            _userRepository.Save();
        }
    }
}