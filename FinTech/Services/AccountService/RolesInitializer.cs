﻿using System;
using System.Linq;
using FinTech.Models;
using FinTech.Models.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FinTech.Services.AccountService
{
    public class RolesInitializer
    {
        private readonly EsdpContext _context;

        public RolesInitializer(EsdpContext context)
        {
            _context = context;
        }

        public async void SeedAdminUser()
        {
            var user = new User()
            {
                UserName = "root@admin.com",
                NormalizedUserName = "ROOT@ADMIN.COM",
                Email = "root@admin.com",
                NormalizedEmail = "ROOT@ADMIN.COM",
                EmailConfirmed = true,
                LockoutEnabled = false,
                SecurityStamp = Guid.NewGuid().ToString(),
                IsActive = true
            };

            var roleStore = new RoleStore<IdentityRole>(_context);
            var roles = new[]
            {
                new IdentityRole {Name = "root", NormalizedName = "ROOT"},
                new IdentityRole {Name = "admin", NormalizedName = "ADMIN"},
                new IdentityRole {Name = "user", NormalizedName = "USER"}
            };

            foreach (var role in roles)
            {
                if (!_context.Roles.Any(r => r.Name == role.Name))
                {
                    await roleStore.CreateAsync(role);
                }
            }

            if (!_context.Users.Any(u => u.UserName == user.UserName))
            {
                var password = new PasswordHasher<User>();
                var hashed = password.HashPassword(user, "password");
                user.PasswordHash = hashed;
                var userStore = new UserStore<User>(_context);
                await userStore.CreateAsync(user);
                await userStore.AddToRoleAsync(user, "ROOT");
            }

            await _context.SaveChangesAsync();
        }
    }
}