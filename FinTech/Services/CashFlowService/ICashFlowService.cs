﻿using FinTech.ViewModels.CashFlows;

namespace FinTech.Services.CashFlowService
{
    public interface ICashFlowService
    {
        CashFlowViewModel BuildCashFlowReportByYear(int year, string companyId);
    }
}