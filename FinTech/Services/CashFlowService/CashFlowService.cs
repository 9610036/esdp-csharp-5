﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Services.OperationService;
using FinTech.Services.PaymentAccountService;
using FinTech.ViewModels.CashFlows;
using FinTech.ViewModels.Operations;

namespace FinTech.Services.CashFlowService
{
    public class CashFlowService : ICashFlowService
    {
        private readonly IOperationService _operationService;
        private readonly IPaymentAccountService _paymentAccountService;
        private readonly IMapper _mapper;

        public CashFlowService(IOperationService operationService,
            IPaymentAccountService paymentAccountService,
            IMapper mapper)
        {
            _operationService = operationService;
            _paymentAccountService = paymentAccountService;
            _mapper = mapper;
        }

        public CashFlowViewModel BuildCashFlowReportByYear(int year, string companyId)
        {
            var operations = _operationService.GetOperationsListByCompanyId(companyId);
            var paymentAccounts = _paymentAccountService.GetPaymentAccountsByCompanyId(companyId);
            var incomeOperations = _operationService.GetOperationsListByCompanyId(companyId)
                .Where(o=>o.IsActive)
                .Where(o => o.CreationDate.Year == year)
                .Where(o => o.BudgetItem.Type == BudgetItemTypes.Income);
            var expenseOperations = _operationService.GetOperationsListByCompanyId(companyId)
                .Where(o=>o.IsActive)
                .Where(o => o.CreationDate.Year == year)
                .Where(o => o.BudgetItem.Type != BudgetItemTypes.Income);

            var model = new CashFlowViewModel
            {
                Year = year,
                StartSum = GetStartSum(operations, paymentAccounts, year),
                EndSum = GetEndSum(paymentAccounts, year),
                IncomeOperations = _mapper.Map<List<OperationViewModel>>(incomeOperations.ToList()),
                ExpenseOperations = _mapper.Map<List<OperationViewModel>>(expenseOperations.ToList())
            };
            return model;
        }

        private decimal GetStartSum(IQueryable<Operation> operations, IQueryable<PaymentAccount> paymentAccounts, int year)
        {
            var incomeSum = operations
                .Where(o=>o.IsActive)
                .Where(o => o.CreationDate.Year <= year)
                .Where(o => o.BudgetItem.Type == BudgetItemTypes.Income)
                .Sum(o => o.Sum);
            var expenseSum = operations
                .Where(o=>o.IsActive)
                .Where(o => o.CreationDate.Year <= year)
                .Where(o => o.BudgetItem.Type != BudgetItemTypes.Income)
                .Sum(o => o.Sum);
            var result = GetEndSum(paymentAccounts,year) - incomeSum + expenseSum;
            return result;
        }

        private decimal GetEndSum(IQueryable<PaymentAccount> paymentAccounts, int year)
        {
            return paymentAccounts
                .Where(o=>o.IsActive)
                .Where(p => p.CreationDate.Year <= year)
                .Sum(p => p.Amount);
        }
    }
}