﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Repositories.OperationRepositories;
using FinTech.Services.Filtration.Operation;
using FinTech.Services.Pagination.Operation;
using FinTech.ViewModels.Operations;
using FinTech.ViewModels.Operations.Configurations;
using FinTech.ViewModels.Operations.Configurations.Filtration;


namespace FinTech.Services.OperationService
{
    public class OperationService : IOperationService
    {
        private readonly IOperationRepository _operationRepository;
        private readonly IOperationPaginationService _operationPaginationService;
        private readonly IOperationFiltrationService _operationFiltrationService;
        private readonly IMapper _mapper;

        public OperationService(
            IOperationRepository operationRepository, 
            IOperationPaginationService operationPaginationService, 
            IOperationFiltrationService operationFiltrationService, 
            IMapper mapper)
        {
            _operationRepository = operationRepository;
            _operationPaginationService = operationPaginationService;
            _operationFiltrationService = operationFiltrationService;
            _mapper = mapper;
        }

        public IQueryable<Operation> GetOperationsListByCompanyId(string companyId)
        {
            return _operationRepository.GetListByCompanyId(companyId);
        }

        public Operation GetOperationById(string id)
        {
            return _operationRepository.GetByOperationId(id);
        }

        public void CreateOperation(CreateOperationViewModel model)
        {
            Operation operation = _mapper.Map<Operation>(model);
            if (operation.BudgetItem.Type != BudgetItemTypes.Income)
                operation.PaymentAccount.Amount -= model.Sum;
            else
                operation.PaymentAccount.Amount += model.Sum;
            _operationRepository.Create(operation);
            _operationRepository.Save();
        }

        public void UpdateOperation(UpdateOperationViewModel model)
        {
            Operation operation = model.ConvertToOperation();
            _operationRepository.Update(operation);
            _operationRepository.Save();
        }

        public void DeleteOperation(string operationId)
        {
            Operation operation = GetOperationById(operationId);
            if (operation != null)
            {
                operation.IsActive = false;
                if (operation.BudgetItem.Type == BudgetItemTypes.Income)
                    operation.PaymentAccount.Amount -= operation.Sum;
                else
                    operation.PaymentAccount.Amount += operation.Sum;
                _operationRepository.Update(operation);
                _operationRepository.Save();
            }
        }

        public OperationIndexViewModel GetFilteredOperations(OperationsConfigurationsViewModel configurations, Company company)
        {
            configurations.ViewModel.DeserializeLists();
            var returnOperations = GetFilteredOperationsList(configurations.ViewModel, company);
            configurations.PageViewModel = _operationPaginationService.ReturnPageViewModel(
                configurations.PageViewModel, 
                returnOperations.ToList().Count);
            returnOperations = returnOperations
                .Skip(configurations.PageViewModel.UnitsOnPage * configurations.PageViewModel.CurrentPage)
                .Take(configurations.PageViewModel.UnitsOnPage);
            var returnViewModel = new OperationIndexViewModel
            {
                Operations = _mapper.Map<List<OperationViewModel>>(returnOperations.ToList()),
                Configurations = configurations
            };
            return returnViewModel;
        }

        public bool CheckExitById(string id)
        {
            return _operationRepository.CheckExistById(id);
        }


        private IQueryable<Operation> GetFilteredOperationsList(FilterOperationsViewModel model, Company company)
        {
            var testCompanyId = company.Id;
            var filteredOperations = _operationFiltrationService.Filter(model, 
                GetOperationsListByCompanyId(testCompanyId)
                    .Where(o => o.IsActive));
            return filteredOperations;
        }
        public List<Operation> GetSumAndGroupByMonthAndNameList(List<Operation> rawOperations)
        {
            var calculatedCollection = rawOperations.ToList().GroupBy(op => new { op.CreationDate.Month, op.BudgetItem.Name }).Select(no =>
            {
                var newOperation = _mapper.Map<Operation>(no.First());
                newOperation.Sum = no.Sum(n => n.Sum);
                return newOperation;
            }).ToList();
            return calculatedCollection;
        }
    }
}