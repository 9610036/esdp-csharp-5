﻿using System.Collections.Generic;
using System.Linq;
using FinTech.Models;
using FinTech.ViewModels.Operations;
using FinTech.ViewModels.Operations.Configurations;

namespace FinTech.Services.OperationService
{
    public interface IOperationService
    {
        IQueryable<Operation> GetOperationsListByCompanyId(string companyId);
        Operation GetOperationById(string id);

        void CreateOperation(CreateOperationViewModel model);
        void UpdateOperation(UpdateOperationViewModel model);
        void DeleteOperation(string operationId);
        OperationIndexViewModel GetFilteredOperations(OperationsConfigurationsViewModel configurations, Company company);
        bool CheckExitById(string id);
        List<Operation> GetSumAndGroupByMonthAndNameList(List<Operation> rawOperations);
    }
}