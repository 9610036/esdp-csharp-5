﻿using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;

namespace FinTech.Services.EmailService
{
    public class EmailService
    {
        private readonly IOptions<InfoEmail> _options;

        public EmailService(IOptions<InfoEmail> options)
        {
            _options = options;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("csharp5", "FinTechEsdp@yandex.ru"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            var accountGmail = _options.Value.Username;
            var password = _options.Value.Password;
            var port = _options.Value.Port;
            var ssl = _options.Value.Ssl;
            var host = _options.Value.Host;

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(host, port, ssl);
                await client.AuthenticateAsync(accountGmail, password);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }
    }
}