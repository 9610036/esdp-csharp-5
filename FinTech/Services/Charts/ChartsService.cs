using System;
using System.Collections.Generic;
using System.Linq;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Services.BudgetItemService;
using FinTech.Services.CounterpartyService;
using FinTech.Services.OperationService;
using FinTech.Services.PaymentAccountService;
using FinTech.Services.ProfitAndLosesServices;
using FinTech.ViewModels.BudgetItems;
using FinTech.ViewModels.Charts;
using FinTech.ViewModels.Counterparties;

namespace FinTech.Services.Charts
{
    public class ChartsService : IChartsService
    {
        private readonly IBudgetItemService _budgetItemService;
        private readonly IOperationService _operationService;
        private readonly ICounterpartyService _counterpartyService;
        private readonly IPaymentAccountService _paymentAccountService;
        private readonly IProfitAndLosesService _profitAndLosesService;

        public ChartsService(
            IBudgetItemService budgetItemService, 
            ICounterpartyService counterpartyService, 
            IPaymentAccountService paymentAccountService, 
            IOperationService operationService, 
            IProfitAndLosesService profitAndLosesService)
        {
            _budgetItemService = budgetItemService;
            _counterpartyService = counterpartyService;
            _paymentAccountService = paymentAccountService;
            _operationService = operationService;
            _profitAndLosesService = profitAndLosesService;
        }

        public List<ChartsBudgetItemViewModel> ReturnBudgetItems(int year, string companyId, BudgetItemTypes budgetItemType)
        {
            var returnList = new List<ChartsBudgetItemViewModel>();
            List<BudgetItem> budgetItems;
            var budgetItemsList = _budgetItemService.GetBudgetItemsListByCompanyId(companyId, null);
            if (budgetItemType != BudgetItemTypes.Income)
                budgetItems = budgetItemsList
                    .Where(b => b.Type != BudgetItemTypes.Income)
                    .ToList();
            else
                budgetItems = budgetItemsList
                    .Where(b => b.Type == BudgetItemTypes.Income)
                    .ToList();
            var operations = _operationService.GetOperationsListByCompanyId(companyId)
                .Where(o => o.IsActive);
            foreach (var budgetItem in budgetItems)
            {
                ChartsBudgetItemViewModel model = new ChartsBudgetItemViewModel
                {
                    Name = budgetItem.Name,
                    SumPerMonths = new List<decimal>()
                };
                var budgetItemOperations = operations
                    .Where(o => o.BudgetItem.Name == budgetItem.Name)
                    .ToList();
                model.TotalSum = budgetItemOperations
                    .Sum(o => o.Sum);
                for (int i = 1; i < 13; i++)
                {
                    decimal sum = budgetItemOperations 
                        .Where(o => o.CreationDate.Month == i)
                        .Sum(o => o.Sum);
                    model.SumPerMonths.Add(sum);
                }
                returnList.Add(model);
            }
            return returnList
                .Distinct()
                .ToList();
        }

        public List<ChartsCounterpartyViewModel> ReturnIncomeCounterparties(int year, string companyId)
        {
            var returnList = new List<ChartsCounterpartyViewModel>();
            var operations = _operationService.GetOperationsListByCompanyId(companyId)
                .Where(o => o.IsActive &&
                            o.BudgetItem.Type == BudgetItemTypes.Income);
            var counterparties = operations
                .Select(o => o.Counterparty)
                .Distinct()
                .ToList();
            foreach (var counterparty in counterparties)
            {
                var totalSum = operations
                    .Where(o => o.Counterparty == counterparty)
                    .Sum(o => o.Sum);
                var percentage = totalSum * 100 / operations.Sum(o => o.Sum);
                var model = new ChartsCounterpartyViewModel
                {
                    Name = counterparty.Name,
                    Percentage = percentage
                };
                returnList.Add(model);
            }
            return returnList.OrderByDescending(c => c.Percentage)
                .Take(10)
                .ToList();
        }

        public ChartsReportViewModel ReturnReport(ReportTypes reportType, int year, OperationTypes type, string companyId)
        {
            var operationSums = new List<List<decimal>>();
            var incomeOperationSums = new List<decimal>();
            var expenseOperationSums = new List<decimal>();
            var differences = new List<decimal>();
            int months = 12;
            var operations = _operationService.GetOperationsListByCompanyId(companyId)
                .Where(o => o.IsActive &&
                            o.CreationDate.Year == year);
            if (type != OperationTypes.Common)
                operations = operations
                    .Where(o => o.Type == type);
            if (reportType == ReportTypes.PnL)
                operations = operations
                    .Where(o => o.IsConfirmed);
            for (int i = 1; i <= months; i++)
            {
                var monthOperations = operations
                    .Where(o => o.CreationDate.Month == i);
                incomeOperationSums.Add(monthOperations
                    .Where(o => o.BudgetItem.Type == BudgetItemTypes.Income)
                    .Sum(o => o.Sum));
                expenseOperationSums.Add(monthOperations
                    .Where(o => o.BudgetItem.Type != BudgetItemTypes.Income)
                    .Sum(o => o.Sum));
                differences.Add(incomeOperationSums[i - 1] - expenseOperationSums[i - 1]);
            }
            operationSums.AddRange(new List<List<decimal>>
            {
                incomeOperationSums,
                expenseOperationSums
            });
            var names = new List<string>();
            if (reportType == ReportTypes.PnL)
                names = Enum.GetValues<PnlChartLabels>()
                    .Select(e => e.GetDescription())
                    .ToList();
            else
                names = Enum.GetValues<CashFlowChartLabels>()
                    .Select(e => e.GetDescription())
                    .ToList();
            var returnModel = new ChartsReportViewModel
            {
                Names = names,
                SumsPerMonth = operationSums,
                TotalSums = new List<decimal>
                {
                    incomeOperationSums.Sum(),
                    expenseOperationSums.Sum()
                },
                DifferencesPerMonth = differences,
                TotalDifference = incomeOperationSums.Sum() - 
                                  expenseOperationSums.Sum()
            };
            if (reportType == ReportTypes.PnL)
            {
                if (returnModel.TotalSums[0] != 0)
                    returnModel.Profitability =
                        Math.Round(returnModel.TotalDifference * 100 / returnModel.TotalSums[0], 2);
                else
                    returnModel.Profitability = 100;
            }
            return returnModel;
        }
    }
}