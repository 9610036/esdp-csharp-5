using System.Collections.Generic;
using FinTech.Enums;
using FinTech.ViewModels.BudgetItems;
using FinTech.ViewModels.Charts;
using FinTech.ViewModels.Counterparties;

namespace FinTech.Services.Charts
{
    public interface IChartsService
    {
        public List<ChartsBudgetItemViewModel> ReturnBudgetItems(int year, string companyId,
            BudgetItemTypes budgetItemType);
        public List<ChartsCounterpartyViewModel> ReturnIncomeCounterparties(int year, string companyId);
        public ChartsReportViewModel ReturnReport(ReportTypes reportType, int year, OperationTypes type, string companyId);
    }
}