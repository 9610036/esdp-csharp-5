﻿using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace FinTech.Services.FileService
{
    public class FileService: IFileService
    {
        public FileResult GetFileStream(string filePath, string fileType, string fileClientName)
        {
            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.DeleteOnClose);
            return new FileStreamResult(fs, new MediaTypeHeaderValue(fileType))
            {
                FileDownloadName = fileClientName,
            };
        }

        public async void UploadFile(string path, string fileName, IFormFile file)
        {
            await using var stream = new FileStream(Path.Combine(path, fileName), FileMode.Create);
            await file.CopyToAsync(stream);
        }
    }
}