﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.Services.FileService
{
    public interface IFileService
    {
        FileResult GetFileStream(string filePath, string fileType, string fileClientName);
        void UploadFile(string path, string fileName, IFormFile file);
    }
}