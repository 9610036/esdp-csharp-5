﻿using System.Linq;
using FinTech.Models;
using FinTech.ViewModels.Counterparties;

namespace FinTech.Services.CounterpartyService
{
    public interface ICounterpartyService
    {
        IQueryable<Counterparty> GetCounterpartiesByCurrentCompanyId(string companyId);
        void CreateCounterpartyForCompany(CreateCounterpartyViewModel viewModel);
        Counterparty GetCounterpartyById(string counterpartyId);
        void UpdateCounterparty(UpdateCounterpartyViewModel viewModel);
        void DeleteCounterpartyById(string counterpartyId);
        bool CheckBin(string bin, Company company);
        bool CheckExistById(string id);
    }
}