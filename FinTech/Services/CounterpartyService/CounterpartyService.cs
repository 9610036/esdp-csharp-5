﻿using System.Linq;
using AutoMapper;
using FinTech.Models;
using FinTech.Repositories.CounterpartyRepositories;
using FinTech.Services.CompanyService;
using FinTech.ViewModels.Counterparties;

namespace FinTech.Services.CounterpartyService
{
    public class CounterpartyService : ICounterpartyService
    {
        private readonly ICounterpartyRepository _counterpartyRepository;
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;

        public CounterpartyService(ICounterpartyRepository counterpartyRepository, 
            ICompanyService companyService,
            IMapper mapper)
        {
            _counterpartyRepository = counterpartyRepository;
            _companyService = companyService;
            _mapper = mapper;
        }

        public IQueryable<Counterparty> GetCounterpartiesByCurrentCompanyId(string companyId)
        {
            return _counterpartyRepository.GetListByCompanyId(companyId);
        }

        public void CreateCounterpartyForCompany(CreateCounterpartyViewModel viewModel)
        {
            Counterparty counterparty = new Counterparty()
            {
                Name = viewModel.Name,
                ShortName = viewModel.ShortName,
                Comment = viewModel.Comment,
                Bin = viewModel.Bin,
                Company = _companyService.GetCompanyByBin(viewModel.CompanyBin)
            };
            _counterpartyRepository.Create(counterparty);
            _counterpartyRepository.Save();
        }

        public Counterparty GetCounterpartyById(string counterpartyId)
        {
            return _counterpartyRepository.GetByCounterpartyId(counterpartyId);
        }

        public void UpdateCounterparty(UpdateCounterpartyViewModel viewModel)
        {
            
            var counterparty = viewModel.ConvertToCounterparty();
            _counterpartyRepository.Update(counterparty);
            _counterpartyRepository.Save();
        }

        public void DeleteCounterpartyById(string counterpartyId)
        {
            var counterparty = GetCounterpartyById(counterpartyId);
            _counterpartyRepository.SetDisabled(counterparty);
            _counterpartyRepository.Save();
        }

        public bool CheckBin(string bin, Company company)
        {
            return _counterpartyRepository.IsAnyCounterpartyWithBinForCompany(bin, company);
        }

        public bool CheckExistById(string id)
        {
            return _counterpartyRepository.CheckExistById(id);
        }
    }
}