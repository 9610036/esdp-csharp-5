using System.Linq;
using FinTech.ViewModels.Operations.Configurations.Filtration;

namespace FinTech.Services.Filtration.Operation
{
    public interface IOperationFiltrationService
    {
        public IQueryable<Models.Operation> Filter(
            FilterOperationsViewModel viewModel,
            IQueryable<Models.Operation> collection);
    }
}