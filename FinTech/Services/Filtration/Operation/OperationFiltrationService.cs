using System;
using System.Collections.Generic;
using System.Linq;
using FinTech.Enums;
using FinTech.ViewModels.Operations.Configurations.Filtration;

namespace FinTech.Services.Filtration.Operation
{
    public class OperationFiltrationService : IOperationFiltrationService
    {
        public IQueryable<Models.Operation> Filter(FilterOperationsViewModel viewModel, IQueryable<Models.Operation> collection)
        {
            var filteredOperations = collection;
            List<Models.Operation> filteredOperationRanges = new List<Models.Operation>();
            if (viewModel.Type != null)
            {
                if (viewModel.Type == BudgetItemTypes.Income)
                {
                    filteredOperations = filteredOperations
                        .Where(o => o.BudgetItem.Type == BudgetItemTypes.Income);
                }
                else
                {
                    filteredOperations = filteredOperations
                        .Where(o => o.BudgetItem.Type != BudgetItemTypes.Income);
                }
            }
            if (viewModel.PaymentAccounts != null && viewModel.PaymentAccounts.Count != 0)
            {
                foreach (var paymentAccount in viewModel.PaymentAccounts)
                {
                    filteredOperationRanges.AddRange(collection
                        .Where(o => o.PaymentAccount.Id == paymentAccount)
                        .ToList());
                }
                filteredOperations = filteredOperationRanges.Distinct().AsQueryable();
                filteredOperationRanges = new List<Models.Operation>();
            }

            if (viewModel.Counterparties != null && viewModel.Counterparties.Count != 0)
            {
                foreach (var counterparty in viewModel.Counterparties)
                {
                    filteredOperationRanges.AddRange(collection
                        .Where(o => o.Counterparty.Id == counterparty)
                        .ToList());
                }
                filteredOperations = filteredOperationRanges.Distinct().AsQueryable();
            }

            if (viewModel.DateFrom != null)
            {
                filteredOperations = filteredOperations
                    .Where(o => o.CreationDate >= viewModel.DateFrom);
            }
            if (viewModel.DateUntil != null)
            {
                filteredOperations = filteredOperations
                    .Where(o => o.CreationDate <= viewModel.DateUntil);
            }
            if (viewModel.SumFrom != null)
            {
                filteredOperations = filteredOperations
                    .Where(o => o.Sum >= Convert.ToDecimal(viewModel.SumFrom));
            }
            if (viewModel.SumUntil != null)
            {
                filteredOperations = filteredOperations
                    .Where(o => o.Sum <= Convert.ToDecimal(viewModel.SumUntil));
            }
            return filteredOperations
                .OrderByDescending(o => o.CreationDate);
        }
    }
}