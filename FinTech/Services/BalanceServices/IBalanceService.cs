using FinTech.Models;
using FinTech.ViewModels.Balances;

namespace FinTech.Services.BalanceServices
{
    public interface IBalanceService
    {
        BalanceReportViewModel BuildBalanceReportByYear(int year, Company company);
    }
}