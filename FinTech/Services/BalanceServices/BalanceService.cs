using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Services.AssetLiabilityService;
using FinTech.Services.OperationService;
using FinTech.Services.PaymentAccountService;
using FinTech.Services.ProfitAndLosesServices;
using FinTech.ViewModels.AssetLiabilities;
using FinTech.ViewModels.Balances;
using FinTech.ViewModels.Operations;

namespace FinTech.Services.BalanceServices
{
    public class BalanceService : IBalanceService
    {
        
        private readonly IAssetLiabilityService _assetLiabilityService;
        private readonly IProfitAndLosesService _profitAndLosesService;
        private readonly IPaymentAccountService _paymentAccountService;
        private readonly IOperationService _operationService;
        private readonly IMapper _mapper;
        
        public BalanceService(
            IAssetLiabilityService assetLiabilityService, 
            IProfitAndLosesService profitAndLosesService, 
            IOperationService operationService, 
            IMapper mapper, 
            IPaymentAccountService paymentAccountService)
        {
            _assetLiabilityService = assetLiabilityService;
            _profitAndLosesService = profitAndLosesService;
            _operationService = operationService;
            _mapper = mapper;
            _paymentAccountService = paymentAccountService;
        }

        public BalanceReportViewModel BuildBalanceReportByYear(int year, Company company)
        {
            List<OperationViewModel> operationViewModels =
                _mapper.Map<List<OperationViewModel>>(_operationService.GetOperationsListByCompanyId(company.Id)
                    .Where(o => o.IsActive)
                    .ToList());
            DateTime convertedYear = Convert.ToDateTime($"01-01-{year}");
            BalanceReportViewModel model = BuildViewModel(company, convertedYear);
            return model;
        }

        private BalanceReportViewModel BuildViewModel(Company company, DateTime convertedYear)
        {
            IQueryable<Operation> operations = _operationService.GetOperationsListByCompanyId(company.Id)
                .Where(o => o.IsActive);
            IQueryable<AssetLiability> assets = _assetLiabilityService.GetAssetLiabilitiesByCompanyId(company.Id)
                .Where(al => al.Type == AssetLiabilityTypes.Asset);
            IQueryable<AssetLiability> liabilities = _assetLiabilityService.GetAssetLiabilitiesByCompanyId(company.Id)
                .Where(al => al.Type == AssetLiabilityTypes.Liability);
            BalanceReportViewModel model = new BalanceReportViewModel
            {
                YearOfReport = convertedYear
            };
            CalculateSumOnPaymentAccounts(ref model, company.Id);
            CalculateInventorySum(ref model, operations, assets);
            CalculateMainCashSum(ref model, operations, assets);
            CalculateAuthorizedCapital(ref model, operations, liabilities);
            CalculateShortTermFinancialLiabilities(ref model, operations, liabilities);
            CalculateAccountsReceivable(ref model, operations);
            CalculateAccountsPayable(ref model, operations);
            CalculateRetainedEarnings(ref model, company.Id, convertedYear);
            CalculateTotalCapital(ref model);
            CalculateTotalLiabilitiesSum(ref model);
            CalculateTotalAssetsSum(ref model);
            CalculateShortTermAssets(ref model);
            CalculateShortTermLiabilities(ref model);
            return model;
        }

        private void CalculateSumOnPaymentAccounts(
            ref BalanceReportViewModel model,
            string companyId)
        {
            var year = model.YearOfReport.Year;
            var paymentAccounts = _paymentAccountService.GetAccountsByCompanyId(companyId)
                .Where(pa => pa.CreationDate.Year <= year);
            var operations = _operationService.GetOperationsListByCompanyId(companyId)
                .Where(o => o.IsActive);
            var operationsFromFuture = operations
                .Where(o => o.CreationDate.Year > DateTime.Now.Year);
            model.SumOnPaymentAccounts = paymentAccounts
                .Sum(pa => pa.Amount) - operationsFromFuture
                .Where(o => o.BudgetItem.Type == BudgetItemTypes.Income)
                .Sum(o => o.Sum) + operationsFromFuture
                .Where(o => o.BudgetItem.Type != BudgetItemTypes.Income)
                .Sum(o => o.Sum);
            var paymentAccountsList = paymentAccounts.ToList();
            foreach (var paymentAccount in paymentAccountsList)
            {
                var operationsFromPresent = operations
                    .Where(o => o.CreationDate.Year >= paymentAccount.CreationDate.Year &&
                                o.PaymentAccount.Id == paymentAccount.Id);
                model.SumOnPaymentAccountsOnCreationDate += paymentAccount.Amount - operationsFromPresent
                    .Where(o => o.BudgetItem.Type == BudgetItemTypes.Income)
                    .Sum(o => o.Sum) + operationsFromPresent
                    .Where(o => o.BudgetItem.Type != BudgetItemTypes.Income)
                    .Sum(o => o.Sum);
            }
        }

        private void CalculateInventorySum(
            ref BalanceReportViewModel model, 
            IQueryable<Operation> operationCollection, 
            IQueryable<AssetLiability> assetCollection)
        {
            var year = model.YearOfReport.Year;
            var assetLiabilitiesList = assetCollection
                .Where(al => al.AssetType == AssetTypes.InventorySum &&
                             al.CreationDate.Year <= year)
                .ToList();
            var operations = operationCollection
                .Where(o => o.IsConfirmed &&
                            o.BudgetItem.AssetLiability.Type == AssetLiabilityTypes.Asset &&
                            o.BudgetItem.AssetLiability.AssetType == AssetTypes.InventorySum &&
                            o.Type == OperationTypes.Operational && 
                            o.CreationDate.Year <= year);
            model.InventoryAssets = new List<AssetLiabilityViewModel>();
            for (int i = 0; i != assetLiabilitiesList.Count; i++)
            {
                var incomeOperations = operations
                    .Where(o => o.BudgetItem.Type == BudgetItemTypes.Income &&
                                o.BudgetItem.AssetLiability == assetLiabilitiesList[i])
                    .ToList();
                var expenseOperations = operations
                    .Where(o => o.BudgetItem.Type != BudgetItemTypes.Income &&
                                o.BudgetItem.AssetLiability == assetLiabilitiesList[i])
                    .ToList();
                var incomeOperationsSum = incomeOperations
                    .Sum(o => o.Sum);
                var expenseOperationsSum = expenseOperations
                    .Sum(o => o.Sum);
                assetLiabilitiesList[i].Sum += expenseOperationsSum - incomeOperationsSum;
                var asset = assetLiabilitiesList[i];
                model.InventoryAssets.Add(_mapper.Map<AssetLiabilityViewModel>(asset));
            }
            model.InventorySum = model.InventoryAssets
                .Sum(al => al.Sum);
        }
        private void CalculateMainCashSum(
            ref BalanceReportViewModel model, 
            IQueryable<Operation> operationCollection, 
            IQueryable<AssetLiability> assetCollection)
        {
            var year = model.YearOfReport.Year;
            var assetLiabilitiesList = assetCollection
                .Where(al => al.AssetType == AssetTypes.MainCash &&
                             al.CreationDate.Year <= year)
                .ToList();
            var operations = operationCollection
                .Where(o => o.IsConfirmed &&
                            o.BudgetItem.AssetLiability.Type == AssetLiabilityTypes.Asset &&
                            o.BudgetItem.AssetLiability.AssetType == AssetTypes.MainCash &&
                            o.Type == OperationTypes.Operational &&
                            o.CreationDate.Year <= year);
            model.MainCashAssets = new List<AssetLiabilityViewModel>();
            for (int i = 0; i != assetLiabilitiesList.Count; i++)
            {
                var incomeOperations = operations
                    .Where(o => o.BudgetItem.Type == BudgetItemTypes.Income &&
                                o.BudgetItem.AssetLiability == assetLiabilitiesList[i])
                    .ToList();
                var expenseOperations = operations
                    .Where(o => o.BudgetItem.Type != BudgetItemTypes.Income &&
                                o.BudgetItem.AssetLiability == assetLiabilitiesList[i])
                    .ToList();
                var incomeOperationsSum = incomeOperations
                    .Sum(o => o.Sum);
                var expenseOperationsSum = expenseOperations
                    .Sum(o => o.Sum);
                assetLiabilitiesList[i].Sum += expenseOperationsSum - incomeOperationsSum;
                var asset = assetLiabilitiesList[i];
                model.MainCashAssets.Add(_mapper.Map<AssetLiabilityViewModel>(asset));
            }
            model.MainCashSum = model.MainCashAssets
                .Sum(al => al.Sum);
        }

        private void CalculateShortTermFinancialLiabilities(
            ref BalanceReportViewModel model,
            IQueryable<Operation> operationsCollection,
            IQueryable<AssetLiability> liabilitiesCollection)
        {
            var year = model.YearOfReport.Year;
            var liabilities = liabilitiesCollection
                .Where(l => l.Name.ToLower() != "уставный капитал" && 
                            l.CreationDate.Year <= year)
                .ToList();
            var operations = operationsCollection
                .Where(o => o.IsConfirmed &&
                            o.CreationDate.Year <= year &&
                            o.BudgetItem.AssetLiability.Type == AssetLiabilityTypes.Liability &&
                            o.Type == OperationTypes.Financial
                );
            var incomeOperations = operations
                .Where(o => o.BudgetItem.Type == BudgetItemTypes.Income)
                .ToList();
            var expenseOperations = operations
                .Where(o => o.BudgetItem.Type != BudgetItemTypes.Income)
                .ToList();
            var incomeOperationsSum = incomeOperations
                .Sum(o => o.Sum);
            var expenseOperationsSum = expenseOperations
                .Sum(o => o.Sum);
            model.ShortTermFinancialLiabilities = new List<AssetLiabilityViewModel>();
            for (int i = 0; i != liabilities.Count; i++)
            {
                liabilities[i].Sum += incomeOperationsSum - expenseOperationsSum;
                model.ShortTermFinancialLiabilities.Add(_mapper.Map<AssetLiabilityViewModel>(liabilities[i]));
            }
            model.ShortTermFinancialLiabilitiesSum = model.ShortTermFinancialLiabilities
                .Sum(o => o.Sum);
        }

        private void CalculateAuthorizedCapital(
            ref BalanceReportViewModel model,
            IQueryable<Operation> operationCollection,
            IQueryable<AssetLiability> liabilities)
        {
            string liabilityName = "уставный капитал";
            var year = model.YearOfReport.Year;
            var authorizedCapitalLiability = liabilities
                .FirstOrDefault(l => l.Name.ToLower() == liabilityName &&
                                     l.CreationDate.Year <= year);
            if (authorizedCapitalLiability != null)
            {
                var operations = operationCollection
                    .Where(o => o.IsConfirmed &&
                                o.BudgetItem.AssetLiability.Id == authorizedCapitalLiability.Id &&
                                o.CreationDate.Year <= year);
                var incomeOperationsSum = operations
                    .Where(o => o.BudgetItem.Type == BudgetItemTypes.Income)
                    .Sum(o => o.Sum);
                var expenseOperationsSum = operations
                    .Where(o => o.BudgetItem.Type != BudgetItemTypes.Income)
                    .Sum(o => o.Sum);
                model.AuthorizedCapital = authorizedCapitalLiability.Sum += incomeOperationsSum - expenseOperationsSum;
            }
            else
                model.AuthorizedCapital = 0;
        }

        private void CalculateAccountsReceivable(
            ref BalanceReportViewModel model,
            IQueryable<Operation> collection)
        {
            var year = model.YearOfReport.Year;
            model.AccountsReceivable = collection
                .Where(o => o.IsConfirmed == false &&
                            o.BudgetItem.Type != BudgetItemTypes.Income &&
                            o.CreationDate.Year <= year)
                .Sum(o => o.Sum);
        }

        private void CalculateAccountsPayable(
            ref BalanceReportViewModel model,
            IQueryable<Operation> collection)
        {
            var year = model.YearOfReport.Year;
            model.AccountsPayable = collection
                .Where(o => o.IsConfirmed == false &&
                            o.BudgetItem.Type == BudgetItemTypes.Income &&
                            o.CreationDate.Year <= year)
                .Sum(o => o.Sum);
        }

        private void CalculateRetainedEarnings(
            ref BalanceReportViewModel model, 
            string companyId, 
            DateTime year) => model.RetainedEarnings =
            _profitAndLosesService.GetProfitAndLosesDifferenceByCompanyIdAndYear(companyId, year) + 
            model.SumOnPaymentAccountsOnCreationDate + model.InventorySum + 
            model.MainCashSum - 
            model.ShortTermFinancialLiabilitiesSum - 
            model.AuthorizedCapital;

        private void CalculateTotalCapital(ref BalanceReportViewModel model) =>
            model.TotalCapital = model.AuthorizedCapital + model.RetainedEarnings;

        private void CalculateTotalLiabilitiesSum(ref BalanceReportViewModel model) =>
            model.TotalLiabilitiesSum = model.ShortTermFinancialLiabilitiesSum +
                                        model.AccountsPayable +
                                        model.TotalCapital;
        private void CalculateTotalAssetsSum(ref BalanceReportViewModel model) =>
            model.TotalAssetsSum = model.SumOnPaymentAccounts +
                                        model.AccountsReceivable +
                                        model.InventorySum +
                                        model.MainCashSum;
        private void CalculateShortTermAssets(ref BalanceReportViewModel model) =>
            model.ShortTermAssets = model.InventorySum +
                                    model.AccountsReceivable +
                                    model.SumOnPaymentAccounts;
        private void CalculateShortTermLiabilities(ref BalanceReportViewModel model) =>
            model.ShortTermLiabilities = model.ShortTermFinancialLiabilitiesSum +
                                    model.AccountsPayable;

    }
}