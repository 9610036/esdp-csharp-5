﻿using System.Linq;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Repositories.BudgetItemRepositories;
using FinTech.ViewModels.BudgetItems;

namespace FinTech.Services.BudgetItemService
{
    public class BudgetItemService : IBudgetItemService
    {
        private readonly IBudgetItemRepository _budgetItemRepository;
        private readonly IMapper _mapper;

        public BudgetItemService(IBudgetItemRepository budgetItemRepository, IMapper mapper)
        {
            _budgetItemRepository = budgetItemRepository;
            _mapper = mapper;
        }


        public BudgetItem GetBudgetItemById(string budgetItemId)
        {
            return _budgetItemRepository.GetByBudgetItemId(budgetItemId);
        }

        public BudgetItem GetBudgetItemByName(string name)
        {
            return _budgetItemRepository.GetByBudgetItemName(name);
        }

        public IQueryable<BudgetItem> GetBudgetItemsListByCompanyId(string companyId, BudgetItemTypes? budgetItemType = null)
        {
            var budgetItems = _budgetItemRepository.GetListByCompanyId(companyId);
            if (budgetItemType is null)
                return budgetItems;
            if (budgetItemType == BudgetItemTypes.Income)
                return budgetItems.Where(bi => bi.Type == BudgetItemTypes.Income);
            return budgetItems.Where(bi => bi.Type != BudgetItemTypes.Income);
        }

        public void CreateBudgetItem(CreateBudgetItemViewModel model)
        {
            var budgetItem = _mapper.Map<BudgetItem>(model);
            _budgetItemRepository.Create(budgetItem);
            _budgetItemRepository.Save();
        }

        public void UpdateBudgetItem(UpdateBudgetItemViewModel model)
        {
            var budgetItem = model.ConvertToBudgetItem(_mapper);
            _budgetItemRepository.Update(budgetItem);
            _budgetItemRepository.Save();
        }

        public void DeleteBudgetItemById(string budgetItemId)
        {
            var budgetItem = GetBudgetItemById(budgetItemId);
            _budgetItemRepository.SetDisabled(budgetItem);
            _budgetItemRepository.Save();
        }
        
        public bool CheckExistById(string id)
        {
            return _budgetItemRepository.CheckExistById(id);
        }
    }
}