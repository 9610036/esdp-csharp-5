﻿using System.Linq;
using FinTech.Enums;
using FinTech.Models;
using FinTech.ViewModels.BudgetItems;

namespace FinTech.Services.BudgetItemService
{
    public interface IBudgetItemService
    {
        BudgetItem GetBudgetItemById(string id);
        BudgetItem GetBudgetItemByName(string name);
        IQueryable<BudgetItem> GetBudgetItemsListByCompanyId(string companyId, BudgetItemTypes? budgetItemType);
        void CreateBudgetItem(CreateBudgetItemViewModel viewModel);
        void UpdateBudgetItem(UpdateBudgetItemViewModel viewModel);
        void DeleteBudgetItemById(string budgetItemId);
        bool CheckExistById(string id);
    }
}