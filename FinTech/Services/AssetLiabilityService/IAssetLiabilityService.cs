﻿using System;
using System.Collections.Generic;
using System.Linq;
using FinTech.Enums;
using FinTech.Models;
using FinTech.ViewModels.AssetLiabilities;

namespace FinTech.Services.AssetLiabilityService
{
    public interface IAssetLiabilityService
    {
        AssetLiability GetAssetLiabilityById(string id);
        IQueryable<AssetLiability> GetAssetLiabilitiesByCompanyId(string companyId);
        void CreateAssetLiability(CreateAssetLiabilityViewModel viewModel);
        void UpdateAssetLiability(AssetLiabilityViewModel viewModel);
        void DeactivateAssetLiability(string assetLiabilityId);
        List<AssetLiabilityViewModel> GetAssetLiabilitiesByCompanyId_Year_Type(string companyId, DateTime year,
            AssetLiabilityTypes type);
        bool CheckExist(string id);
        public IQueryable<AssetLiability> GetAssetLiabilitiesByCompanyIdAndType(string companyId, AssetLiabilityTypes assetLiabilityType);
    }
}