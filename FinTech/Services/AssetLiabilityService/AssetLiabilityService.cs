﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Repositories.AssetLiabilityRepositories;
using FinTech.Repositories.CompanyRepositories;
using FinTech.ViewModels.AssetLiabilities;
namespace FinTech.Services.AssetLiabilityService
{
    public class AssetLiabilityService: IAssetLiabilityService
    {
        private readonly IAssetLiabilityRepository _assetLiabilityRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IMapper _mapper;

        public AssetLiabilityService(IAssetLiabilityRepository assetLiabilityRepository, 
            ICompanyRepository companyRepository, IMapper mapper)
        {
            _assetLiabilityRepository = assetLiabilityRepository;
            _companyRepository = companyRepository;
            _mapper = mapper;
        }

        public AssetLiability GetAssetLiabilityById(string id)
        {
            var assetLiability = _assetLiabilityRepository.GetById(id);
            return assetLiability;
        }
        

        public IQueryable<AssetLiability> GetAssetLiabilitiesByCompanyId(string companyId)
        {
            return _assetLiabilityRepository.GetByCompanyId(companyId);
        }

        public void CreateAssetLiability(CreateAssetLiabilityViewModel viewModel)
        {
            AssetLiability assetLiability = new AssetLiability 
            {
                Name = viewModel.Name, 
                CreationDate = DateTime.Now,
                Sum = viewModel.Sum, 
                Type = viewModel.Type, 
                Company = _companyRepository.GetByCompanyId(viewModel.CompanyId),
                AssetType = viewModel.AssetType
            }; 
            _assetLiabilityRepository.Create(assetLiability); 
            _assetLiabilityRepository.Save();
        }

        public void UpdateAssetLiability(AssetLiabilityViewModel viewModel)
        {
            AssetLiability assetLiability = viewModel.ConvertToAssetLiability();
            _assetLiabilityRepository.Update(assetLiability); 
            _assetLiabilityRepository.Save();
        }

        public bool CheckExist(string id)
        {
            return _assetLiabilityRepository.CheckExistById(id);
        }

        public void DeactivateAssetLiability(string assetLiabilityId)
        {
            _assetLiabilityRepository.Deactivate(assetLiabilityId);
            _assetLiabilityRepository.Save();
        }

        public List<AssetLiabilityViewModel> GetAssetLiabilitiesByCompanyId_Year_Type(string companyId, DateTime year, AssetLiabilityTypes type)
        {
            var assetLiabilities = _assetLiabilityRepository.GetByCompanyId(companyId)
                .Where(a => a.Type == type &&
                            a.CreationDate.Year <= year.Year);
            var returnList = _mapper.Map<List<AssetLiabilityViewModel>>(assetLiabilities.ToList());
            return returnList;
        }
        
        public IQueryable<AssetLiability> GetAssetLiabilitiesByCompanyIdAndType(string companyId, AssetLiabilityTypes assetLiabilityType)
        {
            return _assetLiabilityRepository.GetByCompanyId(companyId).Where(al => al.Type == assetLiabilityType);
        }
    }
}