using FinTech.ViewModels.Pagination;

namespace FinTech.Services.Pagination.Operation
{
    public class OperationPaginationService : IOperationPaginationService
    {
        public PageViewModel ReturnPageViewModel(PageViewModel viewModel, int collectionCount)
        {
            if (viewModel == null)
                viewModel = new PageViewModel(0);
            viewModel.NextPageAvailability(collectionCount);
            return viewModel;
        }
    }
}