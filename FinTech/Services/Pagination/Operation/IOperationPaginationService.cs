using FinTech.ViewModels.Pagination;

namespace FinTech.Services.Pagination.Operation
{
    public interface IOperationPaginationService
    {
        PageViewModel ReturnPageViewModel(PageViewModel viewModel, int collectionCount);
    }
}