using System.Collections.Generic;
using System.Linq;
using FinTech.Models;
using FinTech.ViewModels.PaymentAccounts;

namespace FinTech.Services.PaymentAccountService
{
    public interface IPaymentAccountService
    {
        List<PaymentAccountViewModel> GetAccountsByCompanyId(string companyId);
        PaymentAccountViewModel GetAccountInfoByAccountId(string accountId);
        PaymentAccount GetById(string id);
        public IQueryable<PaymentAccount> GetPaymentAccountsByCompanyId(string companyId);
        void Update(UpdatePaymentAccountViewModel viewModel);
        void Create(CreatePaymentAccountViewModel viewModel);
        void DeactivateById(string id);
        bool CheckExistById(string id);
    }
}