﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FinTech.Models;
using FinTech.Repositories.PaymentAccountRepositories;
using FinTech.ViewModels.PaymentAccounts;

namespace FinTech.Services.PaymentAccountService
{
    public class PaymentAccountService : IPaymentAccountService
    {
        private readonly IPaymentAccountRepository _paymentAccountRepository;
        private readonly IMapper _mapper;

        public PaymentAccountService(IPaymentAccountRepository paymentAccountRepository, IMapper mapper)
        {
            _paymentAccountRepository = paymentAccountRepository;
            _mapper = mapper;
        }


        public List<PaymentAccountViewModel> GetAccountsByCompanyId(string companyId)
        {
            var paymentAccounts = _paymentAccountRepository.GetListByCompanyId(companyId);
            return _mapper.Map<List<PaymentAccountViewModel>>(paymentAccounts.ToList());;
        }
        
        public IQueryable<PaymentAccount> GetPaymentAccountsByCompanyId(string companyId)
        {
            return _paymentAccountRepository.GetListByCompanyId(companyId);
        }

        public PaymentAccountViewModel GetAccountInfoByAccountId(string accountId)
        {
            var paymentAccount = _paymentAccountRepository.GetByPaymentAccountId(accountId);
            return _mapper.Map<PaymentAccountViewModel>(paymentAccount);
        }

        public PaymentAccount GetById(string id)
        {
            return _paymentAccountRepository.GetByPaymentAccountId(id);
        }

        public void Update(UpdatePaymentAccountViewModel viewModel)
        {
            var paymentAccount = viewModel.ConvertToPaymentAccount();
            _paymentAccountRepository.Update(paymentAccount);
            _paymentAccountRepository.Save();
        }

        public void Create(CreatePaymentAccountViewModel viewModel)
        {
            var paymentAccount = _mapper.Map<PaymentAccount>(viewModel);
            _paymentAccountRepository.Create(paymentAccount);
            _paymentAccountRepository.Save();
        }

        public void DeactivateById(string id)
        {
            PaymentAccount paymentAccount = GetById(id);
            paymentAccount.IsActive = false;
            _paymentAccountRepository.Save();
        }

        public bool CheckExistById(string id)
        {
            return _paymentAccountRepository.CheckExistById(id);
        }
    }
}