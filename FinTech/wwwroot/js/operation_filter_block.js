﻿let arrowUp = '<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-caret-up-fill" viewBox="0 0 16 16">\n' +
    '  <path d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>\n' +
    '</svg>';
let arrowDown = '<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">\n' +
    '  <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>\n' +
    '</svg>';

let enabledStatus = 'on';
let disabledStatus = 'off';
let visibleFilterListBlockStatus = 'filter-list-block';
let visibleFilterListStatus = 'filter-list';
let visibleFilterListCallStatus = 'filter-list-call';
let invisibleStatus = 'd-none';
let companyId;

let counterparties = [];
let paymentAccounts = [];

let filterListCallButton = $('#filter-list-call-button');
let filterListCallButtonContainer = $('#filter-list-call-button-container');
let switchButton1 = $('#switch-button-1');
let switchButton2 = $('#switch-button-2');
let paymentAccountDropdown = $('#payment-account-dropdown-container');
let counterpartyDropdown = $('#counterparty-dropdown-container');
let serializedCounterpartiesContainer = $('#serialized-counterparties-container');
let serializedPaymentAccountsContainer = $('#serialized-payment-accounts-container');
let resetFormButton = $('#reset-form-button');
let resetForm = $('#reset-form');
let typeInput = $('#type-input');

let setUp = function(){
    getDropdownData(paymentAccountDropdown);
    getDropdownData(counterpartyDropdown);
    changeStatus(filterListCallButtonContainer, visibleFilterListCallStatus, invisibleStatus);
    changeArray(null, [], serializedCounterpartiesContainer);
    changeArray(null, [], serializedPaymentAccountsContainer);
}

let switchButtons = function(firstButton, secondButton){
    $(typeInput).val($(firstButton).val());
    $(secondButton).prop("checked", false);
}

let changeArray = function (element, array, container){
    if (element !== null)
    {
        if ($(element).prop('checked') === true)
            array.push($(element).val());
        else
            array.splice(paymentAccounts.indexOf($(element).val()), 1);
    }
    $(container).val(JSON.stringify(array));
}

let changeStatus = function(arrow, statusToAdd, statusToRemove){
    $(arrow).addClass(statusToAdd);
    $(arrow).removeClass(statusToRemove);
}

let getDropdownData = function(container){
    $.ajax({
        url : $(container).attr('url'),
        method : 'GET'
    }).done(function(data){
        $(container).html(data);
        
        $('.payment-account-checkbox').click(function(){
            changeArray($(this), paymentAccounts, serializedPaymentAccountsContainer);
        });

        $('.counterparty-checkbox').click(function(){
            changeArray($(this), counterparties, serializedCounterpartiesContainer);
        });
    })
}

let buttonClick = function(object, childIndex, visibleStatus){
    let block = $(object).parent().parent().children()[childIndex];
    if($(object).hasClass(enabledStatus))
    {
        $(object).html(arrowDown);
        changeStatus($(object), disabledStatus, enabledStatus);
        changeStatus(block, invisibleStatus, visibleStatus);
    }
    else
    {
        $(object).html(arrowUp);
        changeStatus($(object), enabledStatus, disabledStatus);
        changeStatus(block, visibleStatus, invisibleStatus);
    }
};


$(document).ready(function(){
    setUp();
    $('.filter-list-dropdown').click(function(){
        buttonClick($(this),1, visibleFilterListBlockStatus);
    });

    $(filterListCallButton).click(function(){
        getDropdownData(paymentAccountDropdown);
        getDropdownData(counterpartyDropdown);
        buttonClick($(this),0, visibleFilterListStatus);
    });

    $(switchButton1).focus(function(){
        switchButtons(switchButton1, switchButton2);
    });

    $(switchButton2).focus(function(){
        switchButtons(switchButton2, switchButton1);
    });

    $(resetFormButton).click(function(){
        $(resetForm).trigger('submit');
    });
});
