﻿let paymentAccountsBlock = $('#paymentAccountsBlock');

let loadPaymentAccounts = function(){
    $.ajax({
        url : $(paymentAccountsBlock).attr('url'),
        method : 'GET',
        data : {
            id : $(paymentAccountsBlock).attr('modelId'),
            type : $(paymentAccountsBlock).attr('type')
        }
    }).done(function(data){
        $(paymentAccountsBlock).html(data);
        
        let deleteForm = $('#deletePaymentAccountForm');
        let deleteModal = $('#deletePaymentAccountModal');
        let deleteButton = $('.deletePaymentAccountButton');
        let paymentAccountIdInput = $('#paymentAccountIdInput');
        
        $(deleteButton).click(function(){
            $(deleteModal).modal('show');
            $(paymentAccountIdInput).val($(this).attr('payment-account-id'));
        });

        $(deleteForm).submit(function(event){
            event.preventDefault();
            $(deleteModal).modal('hide');
            $.ajax({
                url : $(this).attr('url'),
                method : 'POST',
                data : $(this).serialize()
            }).done(function(data){
                sessionStorage.removeItem('iban');
                sessionStorage.removeItem('amount');
                loadPaymentAccounts();
            })
        });
    })
};

loadPaymentAccounts();