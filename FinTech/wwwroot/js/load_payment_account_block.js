let loadPaymentAccountBlock = function(){
    $.get($('.balance').attr('url'), function (data) {
        $('.balances').html(data);
        let defaultIban = $('#default-iban');
        let defaultAmount = $('#default-amount');
        updateValue(
            defaultIban, 
            defaultAmount, 
            $(defaultIban).attr('default-value'), 
            $(defaultAmount).attr('default-value'));

        $('.account').on('click', function () {
            let id = $(this).attr('id');
            let newIban = $(`#iban-${id}`).html();
            let newAmount = $(`#amount-${id}`).html();
            updateValue(
                defaultIban,
                defaultAmount,
                newIban,
                newAmount);
        });
    });
}

let updateValue = function (ibanElement, amountElement, ibanValue, amountValue){
    $(ibanElement).html(ibanValue);
    $(amountElement).html(amountValue);
}

$(document).ready(function (){
    loadPaymentAccountBlock();
});

$('.update-payment-account-block').submit(function(){
    setTimeout(function (){
        loadPaymentAccountBlock();
    }, 3);
});