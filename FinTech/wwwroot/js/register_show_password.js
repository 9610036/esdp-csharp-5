﻿function showPassword() {
    let password = document.getElementById("show-password");
    let eye = document.getElementById("eye");
    let repeatPassword = document.getElementById("repeat");
    if (password.type === "password"){
        password.type = "text";
        repeatPassword.type = "text";
        eye.className = "form-control border-left-0 rounded-0 pt-2 fa fa-eye-slash";
    }
    else{
        password.type = "password";
        repeatPassword.type = "password";
        eye.className = "form-control border-left-0 rounded-0 pt-2 fa fa-eye";
    }
}