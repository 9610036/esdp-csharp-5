﻿function showPassword() {
    let password = document.getElementById("show-password");
    let eye = document.getElementById("eye");
    if (password.type === "password"){
        password.type = "text";
        eye.className = "form-control border-left-0 rounded-0 pt-2 fa fa-eye-slash";
    }
    else{
        password.type = "password";
        eye.className = "form-control border-left-0 rounded-0 pt-2 fa fa-eye";
    }
}