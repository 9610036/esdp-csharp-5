let getBudgetItemsLink = $('#income-operations-chart-bar').attr('url');
let getCounterpartiesLink = $('#income-counterparties-chart-bar').attr('url');
let getPnlLink = $('#pnl-chart').attr('url');
let getCashFlowLink = $('#cashflow-chart').attr('url');
let incomeBudgetItemsChartBarContext = document.getElementById("income-operations-chart-bar").getContext('2d');
let incomeBudgetItemsChartPieContext = document.getElementById("income-operations-chart-pie").getContext('2d');
let expenseBudgetItemsChartBarContext = document.getElementById("expense-operations-chart-bar").getContext('2d');
let expenseBudgetItemsChartPieContext = document.getElementById("expense-operations-chart-pie").getContext('2d');
let incomeCounterpartiesChartBarContext = document.getElementById("income-counterparties-chart-bar").getContext('2d');
let pnlChartContext = document.getElementById("pnl-chart").getContext('2d');
let cashFlowChartContext = document.getElementById("cashflow-chart").getContext('2d');
let cashflowChartInfoContainer = $('#cashflow-chart-info');
let pnlChartInfoContainer = $('#pnl-chart-info');
let months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
let cashFlowChart = new Chart();

let generateRandomColor = function(){
    let randomColor = '#' + Math.floor(Math.random()*16777215).toString(16);
    return randomColor;
}

let loadContainerInfo = function (container, stringifiedViewModel){
    $.ajax({
        url: $(container).attr('url'),
        method: 'GET',
        data: {
            viewModel: stringifiedViewModel
        }
    }).done(function(data){
        $(container).html(data);
    });
}
let generateRandomColors = function (iterationsNumber)
{
    let colors = []
    for (let i = 0; i !== iterationsNumber; i++)
    {
        let color = generateRandomColor();
        colors.push(color);
    }
    return colors;
}

let buildBudgetItemsChart = function(year, budgetItemType, barChartContext, pieChartContext){
    let chartData;
    $.ajax({
        url : getBudgetItemsLink,
        method : 'GET',
        data : {
            year : year,
            budgetItemType : budgetItemType
        }
    }).done(function (data){
        chartData = JSON.parse(data);
        let datasetsCollectionBar = [];
        let chartDataNames = chartData.map(ch => ch.Name);
        let chartDataTotalSums = chartData.map(ch => ch.TotalSum);
        let randomColors = generateRandomColors(chartData.length);
        let datasetsCollectionPie = [
            {
                data: chartDataTotalSums,
                backgroundColor: randomColors,
                hoverBackgroundColor: randomColors
            }
        ];
        for (let i = 0; i !== chartData.length; i++)
        {
            let randomColor = generateRandomColor();
            datasetsCollectionBar.push({
                label: chartData[i].Name,
                data: chartData[i].SumPerMonths,
                backgroundColor: randomColors[i],
                borderColor: [
                    randomColors[i],
                ],
                borderWidth: 2,
                pointBorderColor: randomColors[i],
                pointBackgroundColor: randomColors[i]
            });
        }
        let budgetItemIncomeChartBar = new Chart(barChartContext, {
            type: 'bar',
            data: {
                labels: months,
                datasets: datasetsCollectionBar
            },
            options: {
                responsive: true
            }
        });
        let budgetItemIncomeChartPie = new Chart(pieChartContext, {
            type: 'doughnut',
            data: {
                labels: chartDataNames,
                datasets: datasetsCollectionPie
            },
            options: {
                responsive: true
            }
        });
    });
}
let buildCounterpartiesCharts = function(year, counterpartyBarContext){
    $.ajax({
        url : getCounterpartiesLink,
        method : 'GET',
        data : {
            year : year
        }
    }).done(function(data){
        let chartData = JSON.parse(data);
        console.log(chartData);
        let randomColors = generateRandomColors(chartData.length);
        let counterpartiesChart = new Chart(counterpartyBarContext, {
            type: 'bar',
            data: {
                labels: chartData.map(cd => cd.Name),
                datasets: [{
                    label: 'Контрагенты, %',
                    data: chartData.map(cd => cd.Percentage),
                    backgroundColor: randomColors,
                    borderColor: [
                        randomColors,
                    ],
                    pointBorderColor: randomColors,
                    pointBackgroundColor: randomColors
                }]
            },
            options: {
                responsive: true
            }
        });
    });
}

let buildPnlChart = function(year, context){
    $.ajax({
        url : getPnlLink,
        method : 'GET',
        data : {
            year : year
        }
    }).done(function(data){
        let chartData = JSON.parse(data);
        console.log(chartData);
        let datasets = [];
        let randomColors = generateRandomColors(chartData.Names.length + 1);
        for (let i = 0; i !== chartData.Names.length; i++)
        {
            datasets.push({
                label: chartData.Names[i],
                data: chartData.SumsPerMonth[i],
                order: 2,
                backgroundColor: randomColors[i],
                borderColor: [
                    randomColors[i],
                ],
                borderWidth: 1,
                pointBorderColor: randomColors[i],
                pointBackgroundColor: randomColors[i]
            })
        }
        datasets.push({
            type: 'line',
            label: 'Чистая прибыль',
            order: 1,
            data: chartData.DifferencesPerMonth,
            backgroundColor: randomColors[randomColors.length - 1],
            borderColor: [
                randomColors[randomColors.length - 1],
            ],
            borderWidth: 3,
            pointBorderColor: randomColors[randomColors.length - 1],
            pointBackgroundColor: randomColors[randomColors.length - 1]
        })
        loadContainerInfo(pnlChartInfoContainer, data);
        let pnlChart = new Chart(context, {
            type: 'bar',
            data : {
                labels: months,
                datasets: datasets
            },
            options: {
                responsive: true
            }
        });
    })
}

let buildCashFlowChart = function(year, type, context){
    $.ajax({
        url : getCashFlowLink,
        method : 'GET',
        data : {
            year : year,
            type : type
        }
    }).done(function(data){
        let chartData = JSON.parse(data);
        console.log(chartData);
        let datasets = [];
        let randomColors = generateRandomColors(chartData.Names.length + 1);
        for (let i = 0; i !== chartData.Names.length; i++)
        {
            datasets.push({
                label: chartData.Names[i],
                data: chartData.SumsPerMonth[i],
                order: 2,
                backgroundColor: randomColors[i],
                borderColor: [
                    randomColors[i],
                ],
                borderWidth: 1,
                pointBorderColor: randomColors[i],
                pointBackgroundColor: randomColors[i]
            })
        }
        datasets.push({
            type: 'line',
            label: 'Разница',
            order: 1,
            data: chartData.DifferencesPerMonth,
            backgroundColor: randomColors[randomColors.length - 1],
            borderColor: [
                randomColors[randomColors.length - 1],
            ],
            borderWidth: 3,
            pointBorderColor: randomColors[randomColors.length - 1],
            pointBackgroundColor: randomColors[randomColors.length - 1]
        })
        loadContainerInfo(cashflowChartInfoContainer, data);
        cashFlowChart.destroy();
        cashFlowChart = new Chart(context, {
            type: 'bar',
            data : {
                labels: months,
                datasets: datasets
            },
            options: {
                responsive: true
            }
        });
    })
}

$(document).ready(function(){
    buildBudgetItemsChart(2021, 1, incomeBudgetItemsChartBarContext, incomeBudgetItemsChartPieContext);
    buildBudgetItemsChart(2021, 2, expenseBudgetItemsChartBarContext, expenseBudgetItemsChartPieContext);
    buildCounterpartiesCharts(2021, incomeCounterpartiesChartBarContext);
    buildPnlChart(2021, pnlChartContext);
    buildCashFlowChart(2021, 4, cashFlowChartContext);
    $('.cashflow-button').click(function(){
        let type = $(this).attr('chartType');
        buildCashFlowChart(2021, type, cashFlowChartContext);
    });
})