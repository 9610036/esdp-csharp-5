let sumInput = $('#sumInput');
let sumValue = $('#sumValue');
$(sumInput).change(function(){
    let convertedValue = $(sumInput).val().toString();
    convertedValue = convertedValue.replace('.', ',');
    $(sumValue).val(convertedValue);
});