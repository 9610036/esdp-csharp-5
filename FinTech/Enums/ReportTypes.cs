using System.ComponentModel;

namespace FinTech.Enums
{
    public enum ReportTypes
    {
        [Description("Прибыль и убытки")]PnL = 1,
        [Description("Денежный поток")]CashFlow = 2,
    }
}