﻿using System.ComponentModel;

namespace FinTech.Enums
{
    public enum AssetLiabilityTypes
    {
        [Description("Активы")] Asset = 1,
        [Description("Обязательства")] Liability = 2
    }
}