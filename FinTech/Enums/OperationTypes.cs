﻿using System.ComponentModel;

namespace FinTech.Enums
{
    public enum OperationTypes
    {
        [Description("Операционная")] Operational = 1,
        [Description("Инвестиционная")] Investment = 2,
        [Description("Финансовая")] Financial = 3,
        [Description("Общий")] Common = 4
    }
}