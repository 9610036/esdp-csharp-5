﻿using System.ComponentModel;

namespace FinTech.Enums
{
    public enum CurrencyTypes
    {
        [Description("Казахстанский тенге")] KZT = 1,
        [Description("Российский рубль")] RUB = 2,
        [Description("Доллар США")] USD = 3
    }
}