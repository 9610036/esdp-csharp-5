﻿using System.ComponentModel;

namespace FinTech.Enums
{
    public enum BudgetItemTypes
    {
        [Description("Доходы")] Income = 1,
        [Description("Расходы")] Expense = 2,
        [Description("Переменные расходы")] Variable = 3,
        [Description("Постоянные расходы")] Permanent = 4
    }
}