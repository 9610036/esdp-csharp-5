using System.ComponentModel;

namespace FinTech.Enums
{
    public enum AssetTypes
    {
        [Description("ТМЦ (Товарно-материальные ценности)")] InventorySum = 1,
        [Description("ОС (Основные средства)")] MainCash = 2,
        [Description("Прочие активы")] Other = 3
    }
}