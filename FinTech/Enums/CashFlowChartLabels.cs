using System.ComponentModel;
using System.Runtime.Serialization;

namespace FinTech.Enums
{
    public enum CashFlowChartLabels
    {
        [EnumMember(Value = "Поступления")]
        [Description("Поступления")]Incomes = 1,
        [EnumMember(Value = "Выплаты")]
        [Description("Выплаты")]Payments = 2
    }
}