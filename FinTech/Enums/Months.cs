﻿using System.ComponentModel;

namespace FinTech.Enums
{
    public enum Months
    {
        [Description("январь")] January = 1,
        [Description("февраль")] February = 2,
        [Description("март")] March = 3,
        [Description("апрель")] May = 4,
        [Description("май")] April = 5,
        [Description("июнь")] June = 6,
        [Description("июль")] July = 7,
        [Description("август")] August = 8,
        [Description("сентябрь")] September = 9,
        [Description("октябрь")] October = 10,
        [Description("ноябрь")] November = 11,
        [Description("декабрь")] December = 12
    }
}