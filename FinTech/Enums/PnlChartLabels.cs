using System.ComponentModel;
using System.Runtime.Serialization;

namespace FinTech.Enums
{
    public enum PnlChartLabels
    {
        [EnumMember(Value = "Прибыль")]
        [Description("Прибыль")]Incomes = 1,
        [EnumMember(Value = "Убытки")]
        [Description("Убытки")]Expenses = 2,
    }
}