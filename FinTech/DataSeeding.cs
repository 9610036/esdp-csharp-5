using System;
using System.Collections.Generic;
using System.Linq;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Models.Data;
using FinTech.Services.CompanyService;
using Microsoft.AspNetCore.Identity;

namespace FinTech
{
    public class DataSeeding
    {
        public static void SeedCompany(EsdpContext context)
        {
            if (!context.Companies.Any())
            {
                var providers = new List<Company>
                {
                    new Company
                    {
                        Name = "TestCompany", 
                        Bin = "1234567890", 
                        Phone = "87777777777", 
                        Email = "test@test.com",
                        IsActive = true
                    },
                    new Company()
                    {
                        Id = "1",
                        Name = "Integration test company",
                        Bin = "999999999000",
                        Phone = "87055555555",
                        Email = "integration@test.com",
                        IsActive = true
                    }
                };
                context.AddRange(providers);
                context.SaveChanges();
            }
        }

        public static void SeedBank(EsdpContext context)
        {
            List<Bank> kzBanks = new List<Bank>
            {
                new Bank {Bik = "ALFAKZKA", Name = "АО \"Дочерний Банк \"АЛЬФА-БАНК\""},
                new Bank {Bik = "ALMNKZKA", Name = "АО \"АТФБанк\" (ДБ АО \"First Heartland Jusan Bank\")\""},
                new Bank {Bik = "ATYNKZKA", Name = "АО \"Altyn Bank\" (ДБ China Citic Bank Corporation Limited)\""},
                new Bank {Bik = "BKCHKZKA", Name = "АО ДБ \"БАНК КИТАЯ В КАЗАХСТАНЕ\""},
                new Bank {Bik = "CASPKZKA", Name = "АО \"KASPI BANK\""},
                new Bank {Bik = "CEDUKZKA", Name = "АО \"Центральный Депозитарий Ценных Бумаг\""},
                new Bank {Bik = "CITIKZKA", Name = "АО \"Ситибанк Казахстан\""},
                new Bank {Bik = "DVKAKZKA", Name = "АО \"Банк Развития Казахстана\""},
                new Bank {Bik = "EABRKZKA", Name = "\"ЕВРАЗИЙСКИЙ БАНК РАЗВИТИЯ\""},
                new Bank {Bik = "EURIKZKA", Name = "АО \"Евразийский Банк\""},
                new Bank {Bik = "GCVPKZ2A", Name = "НАО \"Государственная корпорация \"Правительство для граждан\""},
                new Bank {Bik = "HCSKKZKA", Name = "АО \"Жилстройсбербанк Казахстана\""},
                new Bank {Bik = "HLALKZKZ", Name = "АО \"Исламский Банк \"Al Hilal\""},
                new Bank {Bik = "HSBKKZKX", Name = "АО \"Народный Банк Казахстана\""},
                new Bank {Bik = "ICBKKZKX", Name = "АО \"Торгово-промышленный Банк Китая в г. Алматы\""},
                new Bank {Bik = "INEARUMM", Name = "г.Москва \"Межгосударственный Банк\""},
                new Bank {Bik = "INLMKZKA", Name = "ДБ АО \"Хоум Кредит энд Финанс Банк\""},
                new Bank {Bik = "IRTYKZKA", Name = "АО \"ForteBank\""},
                new Bank {Bik = "KCJBKZKX", Name = "АО \"Банк ЦентрКредит\""},
                new Bank {Bik = "KICEKZKX", Name = "АО \"Казахстанская фондовая биржа\""},
                new Bank {Bik = "KINCKZKA", Name = "АО \"Банк \"Bank RBK\""},
                new Bank {Bik = "KISCKZKX", Name = "РГП \"Казахстанский центр межбанковских расчетов НБРК\""},
                new Bank {Bik = "KKMFKZ2A", Name = "РГУ \"Комитет казначейства Министерства финансов РК\""},
                new Bank {Bik = "KPSTKZKA", Name = "АО \"КАЗПОЧТА\""},
                new Bank {Bik = "KSNVKZKA", Name = "АО \"Банк Фридом Финанс Казахстан\""},
                new Bank {Bik = "KZIBKZKA", Name = "АО \"ДБ \"КАЗАХСТАН-ЗИРААТ ИНТЕРНЕШНЛ БАНК\""},
                new Bank {Bik = "LARIKZKA", Name = "АО \"AsiaCredit Bank (АзияКредит Банк)\""},
                new Bank {Bik = "NBPAKZKA", Name = "АО ДБ \"Национальный Банк Пакистана\" в Казахстане\""},
                new Bank {Bik = "NBPFKZKX", Name = "Банк-кастодиан АО \"ЕНПФ\""},
                new Bank {Bik = "NBRKKZKX", Name = "РГУ \"Национальный Банк Республики Казахстан\""},
                new Bank {Bik = "NURSKZKX", Name = "АО \"Нурбанк\""},
                new Bank {Bik = "SABRKZKA", Name = "ДБ АО \"Сбербанк\""},
                new Bank {Bik = "SHBKKZKA", Name = "АО \"Шинхан Банк Казахстан\""},
                new Bank {Bik = "TBKBKZKA", Name = "АО \"Capital Bank Kazakhstan\""},
                new Bank {Bik = "TSESKZKA", Name = "АО \"First Heartland Jusan Bank\""},
                new Bank {Bik = "VTBAKZKZ", Name = "ДО АО \"Банк ВТБ (Казахстан)\""},
                new Bank {Bik = "ZAJSKZ22", Name = "АО \"Исламский банк \"Заман-Банк\""},
            };
            foreach (var bank in kzBanks)
            {
                if (!context.Banks.Any(b => b.Bik == bank.Bik))
                {
                    context.Banks.Add(bank);
                }
            }
            context.SaveChanges();
        }

        public static void SeedSettings(EsdpContext context, ICompanyService companyService)
        {
            try
            {
                if (context.PaymentAccounts.ToList().Count == 0)
                {
                    context.PaymentAccounts.Add(new PaymentAccount()
                    {
                        Iban = "KZ111111111111111111",
                        Bank = context.Banks.FirstOrDefault(b => b.Bik == "CASPKZKA"),
                        Company = companyService.GetCompanyByBin("1234567890"),
                        Amount = 1200000,
                        CurrencyType = CurrencyTypes.KZT
                    });
                }

                if (context.Counterparties.ToList().Count == 0)
                {
                    context.Counterparties.AddRange(new List<Counterparty>()
                    {
                        new Counterparty
                        {
                            Name = "Neo Service",
                            ShortName = "NS",
                            Bin = "111111111111",
                            Company = companyService.GetCompanyByBin("1234567890")
                        },
                        new Counterparty
                        {
                            Name = "Flowers",
                            ShortName = "FL",
                            Bin = "222222222222",
                            Company = companyService.GetCompanyByBin("1234567890")
                        },
                        new Counterparty
                        {
                            Name = "Жилищно-коммунальное хозяйство",
                            ShortName = "ЖКХ",
                            Bin = "333333333333",
                            Company = companyService.GetCompanyByBin("1234567890")
                        },
                        new Counterparty
                        {
                            Name = "Kazakhtelecom",
                            ShortName = "KT",
                            Bin = "444444444444",
                            Company = companyService.GetCompanyByBin("1234567890")
                        },
                        new Counterparty
                        {
                            Name = "Advanced Micro Devices",
                            ShortName = "AMD",
                            Bin = "555555555555",
                            Company = companyService.GetCompanyByBin("1234567890")
                        },
                        new Counterparty
                        {
                            Name = "Nvidia",
                            ShortName = "Nvidia",
                            Bin = "111111111111",
                            Company = companyService.GetCompanyByBin("1234567890")
                        },
                        new Counterparty
                        {
                            Id = "1",
                            Name = "Hostinger",
                            ShortName = "Hostinger Corp",
                            Bin = "112233445566",
                            Company = companyService.GetCompanyById("1")
                        },
                        new Counterparty
                        {
                            Id = "2",
                            Name = "PS Company",
                            ShortName = "PS",
                            Bin = "112233445577",
                            Company = companyService.GetCompanyById("1")
                        },
                        new Counterparty
                        {
                            Id = "3",
                            Name = "Super Corp",
                            ShortName = "SuperC",
                            Bin = "112233445515",
                            Comment = "Company to be deleted",
                            Company = companyService.GetCompanyById("1")
                        },
                        new Counterparty
                        {
                            Id = "4",
                            Name = "Play Station",
                            ShortName = "PS4",
                            Bin = "112233445505",
                            Company = companyService.GetCompanyById("1")
                        }
                    });
                }

                if (context.BudgetItems.ToList().Count == 0)
                {
                    context.BudgetItems.AddRange(new List<BudgetItem>
                    {
                        new BudgetItem
                        {
                            Name = "Покупка видеокарты",
                            Type = BudgetItemTypes.Variable,
                            Company = companyService.GetCompanyByBin("1234567890"),
                        },
                        new BudgetItem
                        {
                            Name = "Коммунальные услуги",
                            Type = BudgetItemTypes.Permanent,
                            Company = companyService.GetCompanyByBin("1234567890"),
                        },
                        new BudgetItem
                        {
                            Name = "Покупка цветов",
                            Type = BudgetItemTypes.Variable,
                            Company = companyService.GetCompanyByBin("1234567890"),
                        },
                        new BudgetItem
                        {
                            Name = "Покупка процессора",
                            Type = BudgetItemTypes.Variable,
                            Company = companyService.GetCompanyByBin("1234567890"),
                        },
                        new BudgetItem
                        {
                            Name = "Зарплата",
                            Type = BudgetItemTypes.Income,
                            Company = companyService.GetCompanyById("1")
                        }
                    });
                }

                context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static void SeedUser(EsdpContext context, ICompanyService companyService)
        {
            if (!context.Users.Any())
            {
                var user = new User
                {
                    ConcurrencyStamp = DateTime.Now.Ticks.ToString(),
                    Email = "user@test.com",
                    NormalizedEmail = "user@test.com",
                    NormalizedUserName = "user@test.com",
                    EmailConfirmed = true,
                    UserName = "user@test.com",
                    Id = "47d90476-8de1-4a71-b0f0-9beaf4d89c98",
                    IsActive = true
                };
                var password = new PasswordHasher<User>();
                var hashed = password.HashPassword(user, "password");
                user.PasswordHash = hashed;
                user.Company = companyService.GetCompanyByBin("999999999000");

                var role = new IdentityRole
                {
                    ConcurrencyStamp = DateTime.Now.Ticks.ToString(), 
                    Id = "admin", 
                    Name = "admin"
                };

                var userRole = new IdentityUserRole<string>
                {
                    RoleId = "admin", 
                    UserId = user.Id
                };

                context.Users.Add(user);
                context.Roles.Add(role);
                context.UserRoles.Add(userRole);
                context.SaveChanges();
            }
        }

        public static void SeedBudgetItems(EsdpContext context, ICompanyService companyService)
        {
            if (!context.BudgetItems.Any())
            {
                context.BudgetItems.AddRange(new List<BudgetItem>
                {
                    new BudgetItem
                    {
                        Name = "Покупка видеокарты",
                        Type = BudgetItemTypes.Expense,
                        Company = companyService.GetCompanyByBin("999999999000"),
                        AssetLiability = context.AssetLiabilities.First()
                    },
                    new BudgetItem
                    {
                        Name = "Зарплата",
                        Type = BudgetItemTypes.Income,
                        Company = companyService.GetCompanyByBin("999999999000")
                    }
                });
                context.SaveChanges();
            }
        }
        
        public static void SeedAssetLiabilities(EsdpContext context, ICompanyService companyService)
        {
            if (!context.AssetLiabilities.Any())
            {
                context.AssetLiabilities.AddRange(new List<AssetLiability>
                {
                    new AssetLiability
                    {
                        CreationDate = DateTime.Now,
                        Name = "Расход",
                        Sum = 2000,
                        Type = AssetLiabilityTypes.Liability,
                        Company = companyService.GetCompanyByBin("999999999000")
                    },
                    new AssetLiability
                    {
                        CreationDate = DateTime.Now,
                        Name = "Видеокарта",
                        Sum = 1000,
                        Type = AssetLiabilityTypes.Asset,
                        Company = companyService.GetCompanyByBin("999999999000"),
                        AssetType = AssetTypes.InventorySum
                    }
                });
                context.SaveChanges();
            }
        }
        
        public static void SeedPaymentAccounts(EsdpContext context, ICompanyService companyService)
        {
            if (!context.PaymentAccounts.Any())
            {
                context.PaymentAccounts.Add(new PaymentAccount()
                {
                    Iban = "KZ111111111111111111",
                    Bank = context.Banks.First(),
                    Company = companyService.GetCompanyByBin("999999999000"),
                    Amount = 1200000,
                    CurrencyType = CurrencyTypes.KZT
                });
                context.SaveChanges();
            }
        }
        
        public static void SeedOperations(EsdpContext context, ICompanyService companyService)
        {
            if (!context.Operations.Any())
            {
                context.Operations.AddRange(new List<Operation>()
                {
                    new Operation
                    {
                        CreationDate = DateTime.Now,
                        Sum = 200456,
                        Comment = "aaa",
                        Type = OperationTypes.Operational,
                        Company = companyService.GetCompanyByBin("999999999000"),
                        PaymentAccount = context.PaymentAccounts.First(),
                        Counterparty = context.Counterparties.FirstOrDefault(c => c.Id == "2"),
                        BudgetItem = context.BudgetItems.First(),
                        IsConfirmed = true,
                        IsActive = true
                    },
                    new Operation
                    {
                        CreationDate = DateTime.Now,
                        Sum = 104568,
                        Comment = "aaa",
                        Type = OperationTypes.Operational,
                        Company = companyService.GetCompanyByBin("999999999000"),
                        PaymentAccount = context.PaymentAccounts.First(),
                        Counterparty = context.Counterparties.FirstOrDefault(c => c.Id == "1"),
                        BudgetItem = context.BudgetItems.Last(),
                        IsConfirmed = true,
                        IsActive = true
                    }
                });
                context.SaveChanges();
            }
        }
    }
}