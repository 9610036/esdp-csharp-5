﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FinTech.Models;
using FinTech.Services.AssetLiabilityService;
using FinTech.Services.CompanyService;
using FinTech.ViewModels.AssetLiabilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace FinTech.Controllers
{
    [Authorize]
    public class AssetLiabilityController : Controller
    {
        private readonly IAssetLiabilityService _assetLiabilityService;
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private static readonly Logger Logger = LogManager.GetLogger("AssetLiabilityLogger");

        public AssetLiabilityController(
            IAssetLiabilityService assetLiabilityService,
            ICompanyService companyService, 
            IMapper mapper, 
            UserManager<User> userManager)
        {
            _assetLiabilityService = assetLiabilityService;
            _companyService = companyService;
            _mapper = mapper;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult GetFormAssetLiability(string assetLiability)
        {
            try
            {
                CreateAssetLiabilityViewModel model = new CreateAssetLiabilityViewModel();
                if (assetLiability == "Asset")
                    return PartialView("~/Views/PartialViews/AssetLiability/_AssetPartialView.cshtml", model);
                if (assetLiability == "Liability")
                    return PartialView("~/Views/PartialViews/AssetLiability/_LiabilityPartialView.cshtml", model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetFormAssetLiability:\n {e}");
                return BadRequest(400);
            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAssetLiability(CreateAssetLiabilityViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные CreateAssetLiability\n {model}");
                var user = await _userManager.GetUserAsync(User);
                model.CompanyId = user.Company.Id;
                
                if(ModelState.IsValid)
                {
                    _assetLiabilityService.CreateAssetLiability(model);
                }
                else
                {
                    foreach (var modelState in ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }
                }
                ModelState.Clear();
                return View("/Views/BudgetItem/CreateBudgetItem.cshtml");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе CreateAssetLiability:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public IActionResult EditAssetLiability(string id)
        {
            try
            {
                if (_assetLiabilityService.CheckExist(id))
                {
                    AssetLiability assetLiability = _assetLiabilityService.GetAssetLiabilityById(id);
                    var assetLiabilityViewModel = _mapper.Map<AssetLiabilityViewModel>(assetLiability);
                    return View(assetLiabilityViewModel);
                }
                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе EditAssetLiability:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        [Authorize(Roles = "root, admin")]
        public async Task<IActionResult> EditAssetLiability(AssetLiabilityViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные EditAssetLiability:\n {model}");
                var user = await _userManager.GetUserAsync(User);
                model.CompanyId = user.Company.Id;
                if (_assetLiabilityService.CheckExist(model.Id))
                {
                    _assetLiabilityService.UpdateAssetLiability(model);
                    return RedirectToAction("GetListAssetLiability");
                }
                ModelState.Clear();
                return View();

            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе EditAssetLiability:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public IActionResult DeactivateAssetLiability(string id)
        {
            try
            {
                if (_assetLiabilityService.CheckExist(id))
                {
                    _assetLiabilityService.DeactivateAssetLiability(id);
                    return RedirectToAction("GetListAssetLiability");
                }

                return NotFound("Статья не найдена");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе DeleteAssetLiability:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetListAssetLiability()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var testCompanyId = _companyService.GetCompanyFromUser(user).Id;
                var assetLiabilities = _assetLiabilityService.GetAssetLiabilitiesByCompanyId(testCompanyId);
                return View(_mapper.Map<List<AssetLiabilityViewModel>>(assetLiabilities.ToList()));
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetListAssetLiability:\n {e}");
                return BadRequest(400);
            }
        }
    }
}