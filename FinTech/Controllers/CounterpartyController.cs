﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FinTech.Models;
using FinTech.Services.CompanyService;
using FinTech.Services.CounterpartyService;
using FinTech.ViewModels.Counterparties;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FinTech.Controllers
{
    [Authorize]
    public class CounterpartyController : Controller
    {
        private readonly ICounterpartyService _counterpartyService;
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private static readonly Logger Logger = LogManager.GetLogger("CounterpartyLogger");

        public CounterpartyController(
            ICounterpartyService counterpartyService, 
            ICompanyService companyService, 
            IMapper mapper, UserManager<User> userManager)
        {
            _counterpartyService = counterpartyService;
            _companyService = companyService;
            _mapper = mapper;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var companyId = user.Company.Id;
                var counterparties = _counterpartyService.GetCounterpartiesByCurrentCompanyId(companyId);
                return View(_mapper.Map<List<CounterpartyViewModel>>(counterparties.ToList()));
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Index\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public IActionResult AddNewCounterparty()
        {
            try
            {
                CreateCounterpartyViewModel model = new CreateCounterpartyViewModel();
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе AddNewCounterparty\n {e}");
                return BadRequest(400);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> AddNewCounterparty(CreateCounterpartyViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные в методе AddNewCounterparty:\n {model}");
                var user = await _userManager.GetUserAsync(User);
                var company = _companyService.GetCompanyFromUser(user);
                model.CompanyBin = company.Bin;
                _counterpartyService.CreateCounterpartyForCompany(model);
                ModelState.Clear();
                return RedirectToAction("Index", "Counterparty");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе AddNewCounterparty\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public IActionResult GetDetails(string id)
        {
            try
            {
                if (_counterpartyService.CheckExistById(id))
                {
                    var counterparty = _counterpartyService.GetCounterpartyById(id);
                    return View(_mapper.Map<CounterpartyViewModel>(counterparty));
                }

                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetDetails\n {e}");
                return BadRequest(400);
            }
            
        }

        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public IActionResult Edit(string id)
        {
            try
            {
                if (_counterpartyService.CheckExistById(id))
                {
                    var counterparty = _counterpartyService.GetCounterpartyById(id);
                    var model = _mapper.Map<UpdateCounterpartyViewModel>(counterparty);
                    return View(model);
                }

                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetDetails\n {e}");
                return BadRequest(400);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "root, admin")]
        public async Task<IActionResult> Edit(UpdateCounterpartyViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные с метода Edit:\n {model}");
                var user = await _userManager.GetUserAsync(User);
                model.CompanyId = user.Company.Id;
                
                if (_counterpartyService.CheckExistById(model.Id))
                {
                    _counterpartyService.UpdateCounterparty(model);
                    ModelState.Clear();
                    return RedirectToAction("Index", "Counterparty");
                }

                return View();

            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Edit\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public IActionResult Delete(string id)
        {
            try
            {
                if (_counterpartyService.CheckExistById(id))
                {
                    _counterpartyService.DeleteCounterpartyById(id);
                    return RedirectToAction("Index", "Counterparty");
                }

                return NotFound();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Delete\n {e}");
                return BadRequest(400);
            }
        }
    }
}