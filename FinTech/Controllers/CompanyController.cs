using System;
using System.Threading.Tasks;
using AutoMapper;
using FinTech.Models;
using FinTech.Services.CompanyService;
using FinTech.ViewModels.Companies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FinTech.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private static readonly Logger Logger = LogManager.GetLogger("CompanyLogger");

        public CompanyController(
            ICompanyService companyService,
            IMapper mapper, UserManager<User> userManager)
        {
            _companyService = companyService;
            _mapper = mapper;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var company = _companyService.GetCompanyFromUser(user);
                return View(company);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Index\n {e}");
                return BadRequest(400);
            }
            
        }

        [HttpGet]
        public IActionResult Details(string companyId)
        {
            try
            {
                var company = _companyService.GetCompanyById(companyId);
                if (company != null)
                    return View(_mapper.Map<CompanyViewModel>(company));
                return NotFound("Компании с таким id не найдено");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Details\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public IActionResult Edit(string companyId)
        {
            try
            {
                var company = _companyService.GetCompanyById(companyId);
                if (company != null)
                {
                    var model = _mapper.Map<UpdateCompanyViewModel>(company);
                    return PartialView("~/Views/PartialViews/Company/_CompanyEditFormPartialView.cshtml", model);
                }

                return NotFound("Компании с таким id не найдено");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Edit\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        [Authorize(Roles = "root, admin")]
        public IActionResult Edit(UpdateCompanyViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные в методе Edit:\n {model}");
                _companyService.UpdateCompany(model);
                ModelState.Clear();
                if (User.IsInRole("root"))
                {
                    return RedirectToAction("GetCompanies", "Company");
                }
                return RedirectToAction("Index", "Company");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Edit\n {e}");
                return BadRequest(400);
            }
        }


        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public IActionResult Delete(string companyId)
        {
            try
            {
                var company = _companyService.GetCompanyById(companyId);
                _companyService.DeleteCompany(company);
                if (User.IsInRole("root"))
                {
                    return RedirectToAction("GetCompanies", "Company");
                }
                return RedirectToAction("Index", "Company");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Delete\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        [Authorize(Roles = "root")]
        public IActionResult GetCompanies()
        {
            try
            {
                var model = _companyService.GetAllCompanies();
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetCompanies\n {e}");
                return BadRequest(400);
            }
        }
        
        [HttpGet]
        [Authorize(Roles = "root")]
        public IActionResult Restore(string companyId)
        {
            try
            {
                Company company = _companyService.GetCompanyById(companyId);
                _companyService.Restore(company);
                return RedirectToAction("GetCompanies", "Company");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Restore\n {e}");
                return BadRequest(400);
            }
        }
    }
}