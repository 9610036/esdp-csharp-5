﻿using System;
using System.IO;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Services.CompanyService;
using FinTech.Services.ExcellReportsService;
using FinTech.Services.FileService;
using FinTech.Services.ProfitAndLosesServices;
using FinTech.ViewModels.ProfitAndLoses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FinTech.Controllers
{
    [Authorize]
    public class ProfitAndLosesController : Controller
    {
        private readonly IProfitAndLosesService _profitAndLosesService;
        private readonly ICompanyService _companyService;
        private readonly UserManager<User> _userManager;
        private readonly ExcelReports _excelReports;
        private readonly IFileService _fileService;
        private readonly IWebHostEnvironment _hostEnvironment;
        private static readonly Logger Logger = LogManager.GetLogger("ProfitAndLosesLogger");

        public ProfitAndLosesController(IProfitAndLosesService profitAndLosesService, ICompanyService companyService, UserManager<User> userManager, ExcelReports excelReports, IFileService fileService, IWebHostEnvironment hostEnvironment)
        {
            _profitAndLosesService = profitAndLosesService;
            _companyService = companyService;
            _userManager = userManager;
            _excelReports = excelReports;
            _fileService = fileService;
            _hostEnvironment = hostEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> ProfitAndLosesReport()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var company = user.Company;
                ProfitAndLosesViewModel model = new ProfitAndLosesViewModel()
                {
                    CompanyId = company.Id
                };

                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ProfitAndLosesReport:\n {e}");
                return BadRequest(400);
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> ProfitAndLosesReport(ProfitAndLosesViewModel profitAndLosesViewModel)
        {
            try
            {
                Logger.Info($"Входные данные в методе ProfitAndLosesReport:\n {profitAndLosesViewModel}");
                var user = await _userManager.GetUserAsync(User);
                var company = _companyService.GetCompanyFromUser(user);
                if (company != null)
                {
                    ProfitAndLosesViewModel modelNull = new ProfitAndLosesViewModel();
                    if (profitAndLosesViewModel.FromDate != DateTime.MinValue &&
                        profitAndLosesViewModel.ToDate != DateTime.MinValue)
                    {
                        ProfitAndLosesViewModel model = _profitAndLosesService.GetProfitAndLosesViewModel(company,
                            profitAndLosesViewModel.FromDate,profitAndLosesViewModel.ToDate);
                        return View(model);
                    }
                    return View(modelNull);
                }
                return NotFound();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ProfitAndLosesReport:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        public async Task<IActionResult> GetExcelReport(ProfitAndLosesViewModel model)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var company = _companyService.GetCompanyFromUser(user);
                var viewModel = _profitAndLosesService.GetProfitAndLosesViewModel(company, model.FromDate,model.ToDate);
            
                var fileName = _excelReports.PnlReport(viewModel);
                var filePath = Path.Combine(_hostEnvironment.ContentRootPath, "VirtualFiles", fileName);
                string fileType = "application/xlsx";
                return _fileService.GetFileStream(filePath, fileType, fileName);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetExcelReport:\n {e}");
                return BadRequest(400);
            }
        }
        
    }
}