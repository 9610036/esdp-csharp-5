﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Services.AssetLiabilityService;
using FinTech.Services.BudgetItemService;
using FinTech.ViewModels.AssetLiabilities;
using FinTech.ViewModels.BudgetItems;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FinTech.Controllers
{
    [Authorize]
    public class BudgetItemController : Controller
    {
        private readonly IBudgetItemService _budgetItemService;
        private readonly IMapper _mapper;
        private readonly IAssetLiabilityService _assetLiabilityService;
        private readonly UserManager<User> _userManager;
        private static readonly Logger Logger = LogManager.GetLogger("BudgetItemLogger");

        public BudgetItemController(
            IBudgetItemService budgetItemService, 
            IMapper mapper, 
            IAssetLiabilityService assetLiabilityService,
            UserManager<User> userManager)
        {
            _budgetItemService = budgetItemService;
            _mapper = mapper;
            _userManager = userManager;
            _assetLiabilityService = assetLiabilityService;
        }

        [HttpGet]
        public IActionResult CreateBudgetItem()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetFormBudgetItem(string budgetType)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var companyId = user.Company.Id;
                var assets = _assetLiabilityService.GetAssetLiabilitiesByCompanyIdAndType(companyId, AssetLiabilityTypes.Asset);
                var liabilities = _assetLiabilityService.GetAssetLiabilitiesByCompanyIdAndType(companyId, AssetLiabilityTypes.Liability);
                CreateBudgetItemViewModel model = new CreateBudgetItemViewModel()
                {
                    Assets = _mapper.Map<List<AssetLiabilityViewModel>>(assets.ToList()),
                    Liabilities = _mapper.Map<List<AssetLiabilityViewModel>>(liabilities.ToList()),
                    CompanyBin = user.Company.Bin
                };
                Logger.Info($"Входные данные GetFormBudgetItem\n {model}");
                if (budgetType == "Income")
                    return PartialView("~/Views/PartialViews/BudgetItem/_BudgetItemIncome.cshtml", model);
                if (budgetType == "Expense")
                    return PartialView("~/Views/PartialViews/BudgetItem/_BudgetItemExpense.cshtml", model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetFormBudgetItem:\n {e}");
                return BadRequest(400);
            }

            return NotFound();
        }

        [HttpPost]
        public IActionResult CreateBudgetItem(CreateBudgetItemViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные GetFormBudgetItem\n {model}");
                if (ModelState.IsValid)
                    _budgetItemService.CreateBudgetItem(model);
                ModelState.Clear();
                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе CreateBudgetItem:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public IActionResult EditBudgetItem(string id)
        {
            try
            {
                if (_budgetItemService.CheckExistById(id))
                {
                    var budgetItem = _budgetItemService.GetBudgetItemById(id);
                    var model = _mapper.Map<BudgetItemViewModel>(budgetItem);
                    return View(model);
                }
                return View();

            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе CreateBudgetItem:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        [Authorize(Roles = "root, admin")]
        public async Task<IActionResult> EditBudgetItem(UpdateBudgetItemViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные в методе EditBudgetItem:\n {model}");
                if (_budgetItemService.CheckExistById(model.Id))
                {
                    var user = await _userManager.GetUserAsync(User);
                    model.CompanyId = user.Company.Id;
                    _budgetItemService.UpdateBudgetItem(model);
                    return RedirectToAction("GetBudgetIemList");
                }
                ModelState.Clear();
                return View();

            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе EditBudgetItem:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public IActionResult DeleteBudgetItem(string id)
        {
            try
            {
                if (_budgetItemService.CheckExistById(id))
                {
                    _budgetItemService.DeleteBudgetItemById(id);
                    return RedirectToAction("GetBudgetIemList"); 
                }

                return NotFound();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе DeleteBudgetItem:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetBudgetIemList()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var testCompanyId = user.Company.Id;
                var budgetItems = _budgetItemService.GetBudgetItemsListByCompanyId(testCompanyId, null);
                var budgetItemsList = _mapper.Map<List<BudgetItemViewModel>>(budgetItems.ToList());
                return View(budgetItemsList);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetBudgetIemList:\n {e}");
                return BadRequest(400);
            }
        }
    }
}