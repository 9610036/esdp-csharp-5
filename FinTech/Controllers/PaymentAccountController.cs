﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Services.BankService;
using FinTech.Services.CompanyService;
using FinTech.Services.PaymentAccountService;
using FinTech.ViewModels.PaymentAccounts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FinTech.Controllers
{
    [Authorize]
    public class PaymentAccountController : Controller
    {
        private readonly IPaymentAccountService _paymentAccountService;
        private readonly ICompanyService _companyService;
        private readonly IBankService _bankService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private static readonly Logger Logger = LogManager.GetLogger("PaymentAccountLogger");

        public PaymentAccountController(
            IPaymentAccountService paymentAccountService,
            ICompanyService companyService,
            IBankService bankService,
            IMapper mapper, UserManager<User> userManager)
        {
            _paymentAccountService = paymentAccountService;
            _companyService = companyService;
            _bankService = bankService;
            _mapper = mapper;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var company = _companyService.GetCompanyFromUser(user);
                ViewBag.Company = company;
                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Index:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public IActionResult GetPaymentAccounts(string companyId)
        {
            try
            {
                var paymentAccountViewModels = _paymentAccountService.GetAccountsByCompanyId(companyId)
                    .Where(pa => pa.IsActive)
                    .ToList();
                return PartialView("~/Views/PartialViews/PaymentAccount/_ReturnPaymentAccounts.cshtml",
                    paymentAccountViewModels);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetPaymentAccounts:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public IActionResult ShowDetails(string accountId)
        {
            try
            {
                if (_paymentAccountService.CheckExistById(accountId))
                {
                    var viewModel = _paymentAccountService.GetAccountInfoByAccountId(accountId);
                    return View(viewModel);
                }

                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ShowDetails:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public IActionResult Create(string companyId, string redirectAction)
        {
            try
            {
                var model = new CreatePaymentAccountViewModel
                {
                    CompanyId = companyId,
                    CurrencyType = CurrencyTypes.KZT,
                    CreationDate = DateTime.Now,
                    Banks = _bankService.GetBankViewModels()
                };
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Create:\n {e}");
                return BadRequest(400);
            }
        }


        [HttpPost]
        public IActionResult Create(CreatePaymentAccountViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные в методе Create:\n {model}");
                if (ModelState.IsValid)
                {
                    _paymentAccountService.Create(model);
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Create:\n {e}");
                return BadRequest(400);
            }

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public IActionResult Edit(string id, string redirectAction)
        {
            try
            {
                if (_paymentAccountService.CheckExistById(id))
                {
                    var paymentAccount = _paymentAccountService.GetById(id);
                    var model = _mapper.Map<UpdatePaymentAccountViewModel>(paymentAccount);
                    return View(model);
                }

                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Edit:\n {e}");
                return NotFound();
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [Authorize(Roles = "root, admin")]
        public IActionResult Edit(UpdatePaymentAccountViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные в методе Create:\n {model}" );
                if (_paymentAccountService.CheckExistById(model.Id))
                {
                    _paymentAccountService.Update(model);
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }

                return View();

            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Edit:\n {e}");
                return BadRequest(400);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [Authorize(Roles = "root, admin")]
        public IActionResult Delete(string id)
        {
            try
            {
                if (_paymentAccountService.CheckExistById(id))
                {
                    _paymentAccountService.DeactivateById(id);
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }

                return NotFound("Расчетный счет не найден");

            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Delete:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetCompanyPaymentAccounts()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var company = _companyService.GetCompanyFromUser(user);
                List<PaymentAccountViewModel> model = _paymentAccountService.GetAccountsByCompanyId(company.Id);
                return PartialView("~/Views/PartialViews/PaymentAccount/_AccountBalancePartialView.cshtml", model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Index:\n {e}");
                return null;
            }
        }
    }
}