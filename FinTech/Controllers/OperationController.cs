﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Services.BudgetItemService;
using FinTech.Services.CompanyService;
using FinTech.Services.CounterpartyService;
using FinTech.Services.OperationService;
using FinTech.Services.Pagination.Operation;
using FinTech.Services.PaymentAccountService;
using FinTech.ViewModels.BudgetItems;
using FinTech.ViewModels.Counterparties;
using FinTech.ViewModels.Operations;
using FinTech.ViewModels.Operations.Configurations;
using FinTech.ViewModels.Operations.Configurations.Filtration;
using FinTech.ViewModels.Pagination;
using FinTech.ViewModels.PaymentAccounts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FinTech.Controllers
{
    [Authorize]
    public class OperationController : Controller
    {
        private readonly IOperationService _operationService;
        private readonly IBudgetItemService _budgetItemService;
        private readonly ICompanyService _companyService;
        private readonly ICounterpartyService _counterpartyService;
        private readonly IPaymentAccountService _paymentAccountService;
        private readonly IOperationPaginationService _operationPaginationService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private static readonly Logger Logger = LogManager.GetLogger("OperationLogger");

        public OperationController(IOperationService operationService,
            IBudgetItemService budgetItemService,
            ICompanyService companyService,
            ICounterpartyService counterpartyService,
            IPaymentAccountService paymentAccountService,
            IOperationPaginationService operationPaginationService,
            IMapper mapper, UserManager<User> userManager)
        {
            _operationService = operationService;
            _budgetItemService = budgetItemService;
            _companyService = companyService;
            _counterpartyService = counterpartyService;
            _paymentAccountService = paymentAccountService;
            _operationPaginationService = operationPaginationService;
            _mapper = mapper;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var companyId = user.Company.Id;
                IQueryable<Operation> returnOperations = _operationService.GetOperationsListByCompanyId(companyId)
                    .Where(o => o.IsActive)
                    .OrderByDescending(o => o.CreationDate);
                var pageModel = _operationPaginationService
                    .ReturnPageViewModel(new PageViewModel(), returnOperations.ToList().Count);
                var model = new OperationIndexViewModel
                {
                    Operations = _mapper.Map<List<OperationViewModel>>(returnOperations
                        .Skip(pageModel.UnitsOnPage * pageModel.CurrentPage)
                        .Take(pageModel.UnitsOnPage).ToList()),
                    Configurations = new OperationsConfigurationsViewModel
                    {
                        PageViewModel = pageModel,
                        ViewModel = new FilterOperationsViewModel()
                    }
                };
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Index:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Index(OperationsConfigurationsViewModel configurations)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var company = user.Company;
                var model = _operationService.GetFilteredOperations(configurations, company);
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Index:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public async Task<PartialViewResult> ReturnPaymentAccounts()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                string companyId = _companyService.GetCompanyFromUser(user).Id;
                List<PaymentAccountViewModel> paymentAccounts = new List<PaymentAccountViewModel>();
                paymentAccounts = _paymentAccountService.GetAccountsByCompanyId(companyId).ToList();

                return PartialView(
                    "~/Views/PartialViews/Operation/Filtration/Dropdowns/_ReturnPaymentAccounts.cshtml",
                    paymentAccounts);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ReturnPaymentAccounts\n {e}");
                return null;
            }
        }

        [HttpGet]
        public async Task<PartialViewResult> ReturnCounterparties()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                string companyId = _companyService.GetCompanyFromUser(user).Id;
                List<CounterpartyViewModel> counterparties = new List<CounterpartyViewModel>();
                counterparties = _mapper.Map<List<CounterpartyViewModel>>(
                    _counterpartyService.GetCounterpartiesByCurrentCompanyId(companyId));

                return PartialView(
                    "~/Views/PartialViews/Operation/Filtration/Dropdowns/_ReturnCounterparties.cshtml",
                    counterparties);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ReturnCounterparties\n {e}");
                return null;
            }
        }

        [HttpGet]
        public async Task<string> GetCompanyId()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var companyId = user.Company.Id;
                return companyId;
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetCompanyId\n {e}");
                return null;
            }
        }


        [HttpGet]
        public async Task<IActionResult> AddOperation(BudgetItemTypes budgetItemType)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var company = user.Company;
                var budgetItems = _budgetItemService.GetBudgetItemsListByCompanyId(company.Id, budgetItemType);
                var counterparties = _counterpartyService.GetCounterpartiesByCurrentCompanyId(company.Id);
                var paymentAccounts = _paymentAccountService.GetPaymentAccountsByCompanyId(company.Id);

                CreateOperationViewModel model = new CreateOperationViewModel()
                {
                    CreationDate = DateTime.Now,
                    CompanyBin = company.Bin,
                    BudgetItems = _mapper.Map<List<BudgetItemViewModel>>(budgetItems.ToList()),
                    Counterparties =
                        _mapper.Map<List<CounterpartyViewModel>>(counterparties.ToList()),
                    PaymentAccounts = _mapper.Map<List<PaymentAccountViewModel>>(paymentAccounts.ToList()),
                    BudgetItemType = budgetItemType
                };
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе AddOperation:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        public IActionResult AddOperation(CreateOperationViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Logger.Info($"Входные данные CreateOperationViewModel: \n {model}");
                    _operationService.CreateOperation(model);
                    ModelState.Clear();
                    return RedirectToAction("Index", "Operation");
                }

                return NotFound();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе AddOperation:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public async Task<IActionResult> EditOperation(string operationId)
        {
            try
            {
                if (_operationService.CheckExitById(operationId))
                {
                    var user = await _userManager.GetUserAsync(User);
                    var testCompanyId = user.Company.Id;
                    Operation operation = _operationService.GetOperationById(operationId);
                    var budgetItems = _mapper.Map<List<BudgetItemViewModel>>(_budgetItemService
                        .GetBudgetItemsListByCompanyId(testCompanyId, operation.BudgetItem.Type).ToList());
                    var counterparties = _mapper.Map<List<CounterpartyViewModel>>(_counterpartyService
                        .GetCounterpartiesByCurrentCompanyId(testCompanyId).ToList());
                    var paymentAccounts = _mapper.Map<List<PaymentAccountViewModel>>(_paymentAccountService
                        .GetAccountsByCompanyId(testCompanyId).ToList());
                    UpdateOperationViewModel model = _mapper.Map<UpdateOperationViewModel>(operation);
                    model.BudgetItems = budgetItems;
                    model.Counterparties = counterparties;
                    model.PaymentAccounts = paymentAccounts;
                    Logger.Info($"Обновленные данные операции: {model}");
                    return View(model);
                }

                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе EditOperation:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        [Authorize(Roles = "root, admin")]
        public async Task<IActionResult> EditOperation(UpdateOperationViewModel model)
        {
            try
            {
                if (_operationService.CheckExitById(model.Id))
                {
                    if (ModelState.IsValid)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        model.CompanyId = user.Company.Id;
                        _operationService.UpdateOperation(model);
                        ModelState.Clear();
                        return RedirectToAction("Index", "Operation");
                    }
                }

                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе EditOperation:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        [Authorize(Roles = "root, admin")]
        public IActionResult Delete(string operationId)
        {
            try
            {
                if (_operationService.CheckExitById(operationId))
                {
                    var operation = _operationService.GetOperationById(operationId);
                    if (operation != null && operation.IsConfirmed == false)
                    {
                        _operationService.DeleteOperation(operationId);
                        ModelState.Clear();
                        return RedirectToAction("Index", "Operation");
                    }
                }
                return NotFound("Подтвержденную операцию нельзя удалить или не верный Id операции");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Delete\n {e}");
                return BadRequest(400);
            }
        }
    }
}