﻿using System;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Services.EmailService;
using FinTech.Services.UserService;
using FinTech.ViewModels.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using NLog;

namespace FinTech.Controllers
{
    [Authorize(Roles = "admin, root")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly UserManager<User> _userManager;
        private readonly IOptions<InfoEmail> _optionsEmail;
        private static readonly Logger Logger = LogManager.GetLogger("UserLogger");


        public UserController(
            IUserService userService, 
            UserManager<User> userManager, 
            IOptions<InfoEmail> optionsEmail)
        {
            _userService = userService;
            _userManager = userManager;
            _optionsEmail = optionsEmail;
        }
        
        [HttpGet]
        public IActionResult Index(string companyId)
        {
            try
            {
                var users = _userService.GetViewModelsByCompanyId(companyId);
                return View(users);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Index:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public IActionResult AddNewUser()
        {
            try
            {
                CreateUserViewModel model = new CreateUserViewModel();
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе AddNewUser:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddNewUser(CreateUserViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _userService.GetFromViewModel(model);
                    var admin = _userService.GetByUserName(User.Identity.Name);
                
                    user.Company = admin.Company;
                    user.IsActive = true;
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, "user");
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.Action(
                            "ConfirmEmail",
                            "Account",
                            new { userId = user.Id, code = code },
                            protocol: HttpContext.Request.Scheme);
                        EmailService emailService = new EmailService(_optionsEmail);
                        await emailService.SendEmailAsync(model.Email, "Confirm your account",
                            $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>link</a>");
 
                        return Content("Для завершения регистрации проверьте электронную почту и перейдите по ссылке, указанной в письме");
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                
                }
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе AddNewUser:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                if (_userService.CheckExistenceById(id))
                {
                    var model = _userService.GetEditViewModelById(id);
                    return View(model);
                }

                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Edit\n {e}");
                return BadRequest(400);
            }
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UserEditViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные с метода Edit:\n {model}");
                if (_userService.CheckExistenceById(model.Id))
                {
                    _userService.Edit(model);
                    var user = _userService.GetByUserName(model.Email);
                    if (model.RoleName == "admin")
                    {
                        await _userManager.RemoveFromRoleAsync(user, "user");
                        await _userManager.AddToRoleAsync(user, "admin");
                    }
                    if (model.RoleName == "user")
                    {
                        await _userManager.RemoveFromRoleAsync(user, "admin");
                        await _userManager.AddToRoleAsync(user, "user");
                    }
                    ModelState.Clear();
                    return RedirectToAction("Index", "Company");
                }

                return View();

            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Edit\n {e}");
                return BadRequest(400);
            }
        }
        
        [HttpGet]
        public IActionResult Delete(string id)
        {
            try
            {
                if (_userService.CheckExistenceById(id))
                {
                    _userService.Delete(id);
                    return RedirectToAction("Index", "Company");
                }

                return NotFound();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Delete\n {e}");
                return BadRequest(400);
            }
        }
    }
}