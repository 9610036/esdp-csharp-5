﻿using Microsoft.AspNetCore.Mvc;

namespace FinTech.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Error()
        {
            return View();
        }
    }
}