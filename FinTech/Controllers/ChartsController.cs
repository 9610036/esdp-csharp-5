using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinTech.Enums;
using FinTech.Models;
using FinTech.Services.Charts;
using FinTech.ViewModels.BudgetItems;
using FinTech.ViewModels.Charts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NLog;

namespace FinTech.Controllers
{
    public class ChartsController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IChartsService _chartsService;
        private static readonly Logger Logger = LogManager.GetLogger("ChartsLogger");

        public ChartsController(
            UserManager<User> userManager, 
            IChartsService chartsService
            )
        {
            _userManager = userManager;
            _chartsService = chartsService;
        }
        
        public IActionResult Index()
        {
            try
            {
                if (User.Identity.Name is null)
                    return RedirectToAction("Login","Account");
                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Index:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public async Task<string> ReturnBudgetItemViewModels(int year, int budgetItemType)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var currentCompany = user.Company;
                List<ChartsBudgetItemViewModel> returnCollection = new List<ChartsBudgetItemViewModel>();
                if (budgetItemType == 1)
                    returnCollection = _chartsService.ReturnBudgetItems(
                        year, 
                        currentCompany.Id,
                        BudgetItemTypes.Income);
                else
                    returnCollection = _chartsService.ReturnBudgetItems(
                        year, 
                        currentCompany.Id,
                        BudgetItemTypes.Expense);
                return JsonConvert.SerializeObject(returnCollection);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ReturnBudgetItemViewModels:\n {e}");
                return null;
            }
        }
        
        [HttpGet]
        public async Task<string> ReturnCounterpartyViewModels(int year)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var currentCompany = user.Company;
                var returnCollection = _chartsService
                    .ReturnIncomeCounterparties(year, currentCompany.Id);
                return JsonConvert.SerializeObject(returnCollection);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ReturnCounterpartyViewModels:\n {e}");
                return null;
            }
        }

        [HttpGet]
        public async Task<string> ReturnPnlReport(int year)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var currentCompany = user.Company;
                var returnModel = _chartsService
                    .ReturnReport(ReportTypes.PnL, year, OperationTypes.Operational, currentCompany.Id);
                return JsonConvert.SerializeObject(returnModel);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ReturnPnlReport:\n {e}");
                return null;
            }
        }
        
        [HttpGet]
        public async Task<string> ReturnCashFlowReport(int year, OperationTypes type)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var currentCompany = user.Company;
                var returnModel = _chartsService
                    .ReturnReport(ReportTypes.CashFlow, year, type, currentCompany.Id);
                return JsonConvert.SerializeObject(returnModel);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ReturnCashFlowReport:\n {e}");
                return null;
            }
        }

        [HttpGet]
        public PartialViewResult ReturnReportInfoPartialView(string viewModel)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<ChartsReportViewModel>(viewModel);
                if (model != null)
                {
                    if (model.Profitability != null)
                        return PartialView("~/Views/PartialViews/Charts/_PnlChartInfo.cshtml", model);
                    return PartialView("~/Views/PartialViews/Charts/_CashflowChartInfo.cshtml", model);
                }
                return null;
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ReturnReportInfoPartialView:\n {e}");
                return null;
            }
        }
    }
}