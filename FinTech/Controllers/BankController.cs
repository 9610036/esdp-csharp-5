﻿using System;
using AutoMapper;
using FinTech.Models;
using FinTech.Repositories.BankRepositories;
using FinTech.Services.BankService;
using FinTech.ViewModels.Banks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FinTech.Controllers
{
    [Authorize(Roles = "root")]
    public class BankController : Controller
    {
        private readonly IBankService _bankService;
        private readonly IBankRepository _bankRepository;
        private readonly IMapper _mapper;
        private static readonly Logger Logger = LogManager.GetLogger("BankLogger");


        public BankController(
            IBankService bankService, 
            IBankRepository bankRepository, 
            IMapper mapper)
        {
            _bankService = bankService;
            _bankRepository = bankRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                var listBanks = _bankService.GetBankViewModels();
                return View(listBanks);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Index:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public IActionResult CreateNewBank()
        {
            try
            {
                CreateBankViewModel model = new CreateBankViewModel();
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе CreateNewBank:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        public IActionResult CreateNewBank(CreateBankViewModel model)
        {
           
            try
            {
                Logger.Info($"Входные данные в методе CreateNewBank:\n {model}");
                if (ModelState.IsValid)
                {
                    var bank = _mapper.Map<Bank>(model);
                    _bankRepository.Create(bank);
                    _bankRepository.Save();
                    ModelState.Clear();
                    return RedirectToAction("Index", "Bank");
                }

                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе CreateNewBank:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public IActionResult ShowDetails(string id)
        {
            try
            {
                var model = _bankService.GetBankDetails(id);
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ShowDetails:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpGet]
        public IActionResult UpdateBank(string id)
        {
            try
            {
                if (_bankService.CheckExistById(id))
                {
                    UpdateBankViewModel model = _bankService.GetUpdateBankViewModelById(id);
                    return View(model);
                }
                return View();

            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе UpdateBank:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        public IActionResult UpdateBank(UpdateBankViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные в методе UpdateBank:\n {model}");
                if (ModelState.IsValid)
                {
                    if (_bankService.CheckExistById(model.Id))
                    {
                        var bank = model.ConvertToBank();
                        _bankRepository.Update(bank);
                        _bankRepository.Save();
                        ModelState.Clear();
                        return RedirectToAction("Index");
                    }
                    return View();
                }
                return View();
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе UpdateBank:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        public IActionResult DeleteBank(string id)
        {
            try
            {
                if (_bankService.CheckExistById(id))
                {
                    var bank = _bankService.GetFirstOrDefaultBankById(id);
                    _bankRepository.SetDisabled(bank);
                    _bankRepository.Save();
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }

                return NotFound("Банк не найден, возможно ону же был удален");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе DeleteBank:\n {e}");
                return BadRequest(400);
            }
        }
    }
}