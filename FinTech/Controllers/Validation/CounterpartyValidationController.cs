﻿using FinTech.Models;
using FinTech.Services.CompanyService;
using FinTech.Services.CounterpartyService;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.Controllers.Validation
{
    public class CounterpartyValidationController:Controller
    {
        private readonly ICounterpartyService _counterparty;
        private readonly ICompanyService _companyService;
        private readonly UserManager<User> _userManager;

        public CounterpartyValidationController(ICounterpartyService counterparty, ICompanyService companyService, UserManager<User> userManager)
        {
            _counterparty = counterparty;
            _companyService = companyService;
            _userManager = userManager;
        }

        public bool ValidationBin(string bin)
        {
            var user = _userManager.GetUserAsync(User).Result;
            var company = _companyService.GetCompanyFromUser(user);
            return !_counterparty.CheckBin(bin, company);
        }
    }
}