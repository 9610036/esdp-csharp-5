﻿using System.Linq;
using FinTech.Repositories.PaymentAccountRepositories;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.Controllers.Validation
{
    public class PaymentAccountValidationController : Controller
    {
        
        private readonly IPaymentAccountRepository _paymentAccountRepository;

        public PaymentAccountValidationController(IPaymentAccountRepository paymentAccountRepository)
        {
            _paymentAccountRepository = paymentAccountRepository;
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool ValidateBankNameAndIban(string bankName, string iban, string companyId)
        {
            return !_paymentAccountRepository.GetListByCompanyId(companyId)
                .Any(p => p.Iban == iban && p.Bank.Name == bankName);
        }
    }
}