﻿using System.Linq;
using FinTech.Models.Data;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.Controllers.Validation
{
    public class BankValidationController
    {
        private EsdpContext _db { get;}

        public BankValidationController(EsdpContext db)
        {
            _db = db;
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool ValidateBankByBik(string id, string bik)
        {
            return !_db.Banks.Any(b => b.Id != id && b.Bik == bik);
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool ValidateBankByName(string id, string name)
        {
            return !_db.Banks.Any(b => b.Id != id && b.Name == name);
        }
    }
}