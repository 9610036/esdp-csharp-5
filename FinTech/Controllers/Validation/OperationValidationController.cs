﻿using System;
using FinTech.Enums;
using FinTech.Repositories.OperationRepositories;
using FinTech.Repositories.PaymentAccountRepositories;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.Controllers.Validation
{
    public class OperationValidationController : Controller
    {
        private readonly IPaymentAccountRepository _paymentAccountRepository;
        private readonly IOperationRepository _operationRepository;

        public OperationValidationController(
            IPaymentAccountRepository paymentAccountRepository, 
            IOperationRepository operationRepository)
        {
            _paymentAccountRepository = paymentAccountRepository;
            _operationRepository = operationRepository;
        }

        [AcceptVerbs("GET", "POST")]
        public bool FutureDateValidation(DateTime creationDate)
        {
            return creationDate < DateTime.Now;
        }

        [AcceptVerbs("GET", "POST")]
        public bool SumLimitValidationOnOperationCreating(decimal sum, BudgetItemTypes budgetItemType, string paymentAccountId, DateTime creationDate)
        {
            var paymentAccount = _paymentAccountRepository.GetByPaymentAccountId(paymentAccountId);
            if (creationDate < paymentAccount.CreationDate.Date)
                return false;
            if (budgetItemType != BudgetItemTypes.Income)
                return sum <= paymentAccount.Amount;
            return true;
        }
    }
}