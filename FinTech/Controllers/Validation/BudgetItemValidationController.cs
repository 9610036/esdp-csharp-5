﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using FinTech.Repositories.BudgetItemRepositories;
using FinTech.Services.CompanyService;

namespace FinTech.Controllers.Validation
{
    public class BudgetItemValidationController : Controller
    {
        private readonly IBudgetItemRepository _budgetItemRepository;
        private readonly ICompanyService _companyService;

        public BudgetItemValidationController(IBudgetItemRepository budgetItemRepository, ICompanyService companyService)
        {
            _budgetItemRepository = budgetItemRepository;
            _companyService = companyService;
        }

        [AcceptVerbs("GET", "POST")]
        public ActionResult ValidateBudgetItem(string name, string companyBin)
        {
            var company = _companyService.GetCompanyByBin(companyBin);
            if (name == null)
                return Json("Имя должно быть заполнено!");
            if (_budgetItemRepository.GetListByCompanyId(company.Id)
                .Any(b => b.Name == name))
                return Json("Статья уже занята!");
            return Json(true);
        }
    }
}