﻿using System.Linq;
using FinTech.Enums;
using FinTech.Models.Data;
using FinTech.Services.CompanyService;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.Controllers.Validation
{
    public class AssetLiabilityValidationController : Controller
    {
        private readonly EsdpContext _context;
        private readonly ICompanyService _companyService;

        public AssetLiabilityValidationController(EsdpContext context, ICompanyService companyService)
        {
            _context = context;
            _companyService = companyService;
        }

        [AcceptVerbs("GET", "POST")]
        public bool ValidateAssetLiability(string name)
        {
            var company = _companyService.GetCompanyByBin("1234567890");
            return !_context.AssetLiabilities.Any(b => b.Name == name && b.Company == company);
        }

        [AcceptVerbs("GET", "POST")]
        public bool ValidateAssetType(AssetTypes assetType, AssetLiabilityTypes type)
        {
            if (type == AssetLiabilityTypes.Asset)
            {
                if (assetType == 0)
                    return false;
                return true;
            }
            return true;
        }
    }
}