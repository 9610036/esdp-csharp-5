﻿using System;

namespace FinTech.Controllers.Validation
{
    public class CashFlowValidationController
    {
        public bool YearValidation(int year)
        {
            if (year < 1900 || year > DateTime.Now.Year)
                return false;
            return true;
        }
    }
}