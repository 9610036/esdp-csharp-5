﻿using System;
using System.IO;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Services.CashFlowService;
using FinTech.Services.ExcellReportsService;
using FinTech.Services.FileService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FinTech.Controllers
{
    public class CashFlowController : Controller
    {
        private readonly ICashFlowService _cashFlowService;
        private readonly UserManager<User> _userManager;
        private static readonly Logger Logger = LogManager.GetLogger("CashFlowLogger");
        private readonly ExcelReports _excelReports;
        private readonly IFileService _fileService;
        private readonly IWebHostEnvironment _hostEnvironment;
        
        public CashFlowController(ICashFlowService cashFlowService,
            UserManager<User> userManager, ExcelReports excelReports, IFileService fileService, IWebHostEnvironment hostEnvironment)
        {
            _cashFlowService = cashFlowService;
            _userManager = userManager;
            _excelReports = excelReports;
            _fileService = fileService;
            _hostEnvironment = hostEnvironment;
        }

        [HttpGet]
        public IActionResult CashFlowReport()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CashFlowReport(int year)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var model = _cashFlowService.BuildCashFlowReportByYear(year, user.Company.Id);
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе CashFlowReport\n {e}");
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> GetExcelReport(int year)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var model = _cashFlowService.BuildCashFlowReportByYear(year, user.Company.Id);
                var fileName = _excelReports.CashFlowReport(model);
                var filePath = Path.Combine(_hostEnvironment.ContentRootPath, "VirtualFiles", fileName);
                string fileType = "application/xlsx";
                return _fileService.GetFileStream(filePath, fileType, fileName);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetExcelReport\n {e}");
                throw;
            }
        }
    }
}