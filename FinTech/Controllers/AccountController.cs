﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Castle.Core.Internal;
using FinTech.Models;
using FinTech.Services.EmailService;
using FinTech.ViewModels.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using NLog;

namespace FinTech.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _mapper;
        private readonly IOptions<InfoEmail> _optionsEmail;

        private static readonly Logger Logger = LogManager.GetLogger("AccountLogger");

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, IMapper mapper, IOptions<InfoEmail> optionsEmail)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _optionsEmail = optionsEmail;
        }
        

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            try
            {
                if (userId.IsNullOrEmpty() || code.IsNullOrEmpty())
                    return View("Error");
                var user = await _userManager.FindByIdAsync(userId);
                if (user is null)
                    return View("Error");
                var result = await _userManager.ConfirmEmailAsync(user, code);
                if(result.Succeeded)
                    return RedirectToAction("Index", "Charts");
                return View("Error");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ConfirmEmail:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _mapper.Map<User>(model);
                    user.IsActive = true;
                    user.Company = new Company()
                    {
                        Bin = model.Bin,
                        Name = model.CompanyName,
                        IsActive = true
                    };
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, "admin");
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.Action(
                            "ConfirmEmail",
                            "Account",
                            new { userId = user.Id, code = code },
                            protocol: HttpContext.Request.Scheme);
                        EmailService emailService = new EmailService(_optionsEmail);
                        await emailService.SendEmailAsync(model.Email, "Confirm your account",
                            $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>link</a>");
                        return Content("Для завершения регистрации проверьте электронную почту и перейдите по ссылке, указанной в письме");
                    }
                    foreach (var error in result.Errors)
                        ModelState.AddModelError(string.Empty, error.Description);
                }
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Register:\n {e}");
                return BadRequest();
            }
            
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            try
            {
                return View(new LoginViewModel { ReturnUrl = returnUrl });
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Login:\n {e}");
                return BadRequest(400);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByNameAsync(model.Email);
                    if (user != null)
                    {
                        if (!await _userManager.IsEmailConfirmedAsync(user))
                        {
                            Logger.Info($"У пользователя {model.Email} email не подтвержден");
                            ModelState.AddModelError(string.Empty, "Вы не подтвердили свой email");
                            return View(model);
                        }
                        if (user.IsActive == false)
                        {
                            Logger.Info($"У пользователя {model.Email} аккаунт удален");
                            ModelState.AddModelError(string.Empty, "Аккаунт удален. Для восстановления обратитесь к администратору.");
                            return View(model);
                        }
                    }
                    var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                    if (result.Succeeded)
                    {
                        Logger.Info($"Пользователь {model.Email} успешно авторизовался");
                        return RedirectToAction("Index", "Charts");
                    }
                    Logger.Info("Неудачная попытка входа");
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                }
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Login:\n {e}");
                return BadRequest(400);
            }
            
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            try
            {
                await _signInManager.SignOutAsync();
                return RedirectToAction("Login", "Account");
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Logout:\n {e}");
                return BadRequest(400);
            }
        }
        
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }
        
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                Logger.Info($"Входные данные для сброса пароля {model.Email}");
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByEmailAsync(model.Email);
                    if (user is null || !(await _userManager.IsEmailConfirmedAsync(user)))
                        return View("ForgotPasswordConfirmation");
                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                    EmailService emailService = new EmailService(_optionsEmail);
                    await emailService.SendEmailAsync(model.Email, "Reset Password",
                        $"Для сброса пароля пройдите по ссылке: <a href='{callbackUrl}'>link</a>");
                    Logger.Info($"Отправлено письмо для сброса пароля на {model.Email}");
                    return View("ForgotPasswordConfirmation");
                }
                Logger.Info($"Письмо не отправлено на  {model.Email}");
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ForgotPassword:\n {e}");
                return BadRequest();
            }
        }
        
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string userId, string code = null)
        {
            try
            {
                var model = new ResetPasswordViewModel();
                if (!userId.IsNullOrEmpty())
                    model.Email  = _userManager.Users.FirstOrDefault(u => u.Id == userId)?.Email; 
                return code == null ? View("Error") : View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ResetPassword:\n {e}");
                return BadRequest(400);
            }
        }
 
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(model);
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user is null)
                    return View("ResetPasswordConfirmation");
                var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
                if (result.Succeeded)
                    return View("ResetPasswordConfirmation");
                foreach (var error in result.Errors)
                    ModelState.AddModelError(string.Empty, error.Description);
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе ResetPassword:\n {e}");
                return BadRequest();
            }
        }
    }
}