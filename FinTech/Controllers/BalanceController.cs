using System;
using System.IO;
using System.Threading.Tasks;
using FinTech.Models;
using FinTech.Services.BalanceServices;
using FinTech.Services.CompanyService;
using FinTech.Services.ExcellReportsService;
using FinTech.Services.FileService;
using FinTech.ViewModels.Balances;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FinTech.Controllers
{
    [Authorize]
    public class BalanceController : Controller
    {
        private readonly IBalanceService _balanceService;
        private readonly ICompanyService _companyService;
        private readonly UserManager<User> _userManager;
        private readonly ExcelReports _excelReports;
        private readonly IFileService _fileService;
        private readonly IWebHostEnvironment _hostEnvironment;
        private static readonly Logger Logger = LogManager.GetLogger("BalanceLogger");


        public BalanceController(IBalanceService balanceService, ICompanyService companyService, UserManager<User> userManager, ExcelReports excelReports, IFileService fileService, IWebHostEnvironment hostEnvironment)
        {
            _balanceService = balanceService;
            _companyService = companyService;
            _userManager = userManager;
            _excelReports = excelReports;
            _fileService = fileService;
            _hostEnvironment = hostEnvironment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(int year)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var company = _companyService.GetCompanyFromUser(user);
                BalanceReportViewModel model = _balanceService.BuildBalanceReportByYear(year, company);
                ModelState.Clear();
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе Index\n {e}");
                throw;
            }
            
        }

        [HttpPost]
        public async Task<IActionResult> GetExcelReport(BalanceReportViewModel viewModel)
        {
            try
            {
                Logger.Info($"Входные данные GetExcelReport: \n {viewModel}");
                var user = await _userManager.GetUserAsync(User);
                var company = _companyService.GetCompanyFromUser(user);
                var model = _balanceService.BuildBalanceReportByYear(viewModel.YearOfReport.Year, company);
                var fileName = _excelReports.BalanceReport(model);
                var filePath = Path.Combine(_hostEnvironment.ContentRootPath, "VirtualFiles", fileName);
                string fileType = "application/xlsx";
                return _fileService.GetFileStream(filePath, fileType, fileName);
            }
            catch (Exception e)
            {
                Logger.Error($"Произошла ошибка в методе GetExcelReport\n {e}");
                throw;
            }
        }
    }
}