﻿using System.ComponentModel.DataAnnotations;
using FinTech.Models;

namespace FinTech.ViewModels.Counterparties
{
    public class UpdateCounterpartyViewModel
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Введите название контрагента")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Введите краткое название контрагента")]
        public string ShortName { get; set; }
        [Required(ErrorMessage = "Введите БИН контрагента")]
        public string Bin { get; set; }
        public string Comment { get; set; }
        public string CompanyId { get; set; }
        
        
        public Counterparty ConvertToCounterparty()
        {
            return new Counterparty()
            {
                Id = Id,
                Name = Name,
                ShortName = ShortName,
                Bin = Bin,
                Comment = Comment,
                CompanyId = CompanyId,
            };
        }

        public override string ToString()
        {
            return  $"Bin:{Bin}, Comment:{Comment}, Name:{Name}, CompanyId:{CompanyId}, " +
                    $"ShortName:{ShortName}";
        }
    }
}