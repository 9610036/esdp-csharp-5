namespace FinTech.ViewModels.Counterparties
{
    public class ChartsCounterpartyViewModel
    {
        public string Name { get; set; }
        public decimal Percentage { get; set; }
    }
}