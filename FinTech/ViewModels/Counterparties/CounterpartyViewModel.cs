using FinTech.ViewModels.Companies;

namespace FinTech.ViewModels.Counterparties
{
    public class CounterpartyViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Bin { get; set; }
        public string Comment { get; set; }
        public CompanyViewModel Company { get; set; }
    }
}