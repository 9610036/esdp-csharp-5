﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.Counterparties
{
    public class CreateCounterpartyViewModel
    {
        [Required(ErrorMessage = "Введите название контрагента")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Введите краткое название контрагента")]
        public string ShortName { get; set; }
        
        [Required(ErrorMessage = "Введите БИН контрагента")]
        [MinLength(12,ErrorMessage = "Введите 12-тизначный цифровой код")]
        [MaxLength(12,ErrorMessage = "Введите 12-тизначный цифровой код")]
        [Remote("ValidationBin", "CounterpartyValidation", ErrorMessage = "Контрагент с таким бином уже есть")]
        [RegularExpression("^[0-9]+$",ErrorMessage = "Доступны только цифры для ввода")]
        public string Bin { get; set; }
        public string Comment { get; set; }

        public string CompanyBin { get; set; }

        public override string ToString()
        {
            return $"Bin:{Bin}, Comment:{Comment},Name:{Name}," +
            $"CompanyBin:{CompanyBin},ShortName:{ShortName}";
        }
    }
}