using System.Collections.Generic;

namespace FinTech.ViewModels.Charts
{
    public class ChartsReportViewModel
    {
        public List<string> Names { get; set; }
        public List<List<decimal>> SumsPerMonth { get; set; }
        public List<decimal> DifferencesPerMonth { get; set; }
        public decimal TotalDifference { get; set; }
        public List<decimal> TotalSums { get; set; }
        public decimal? Profitability { get; set; }
    }
}