using System.Collections.Generic;

namespace FinTech.ViewModels.BudgetItems
{
    public class ChartsBudgetItemViewModel
    {
        public string Name { get; set; }
        public List<decimal> SumPerMonths { get; set; }
        public decimal TotalSum { get; set; }
    }
}