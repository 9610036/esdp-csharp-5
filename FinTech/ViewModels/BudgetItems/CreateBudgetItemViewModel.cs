﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FinTech.Enums;
using FinTech.ViewModels.AssetLiabilities;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.BudgetItems
{
    public class CreateBudgetItemViewModel
    {
        public string Id { get; set;}
        
        [Remote("ValidateBudgetItem", "BudgetItemValidationController")]
        [Required(ErrorMessage = "Введите название статьи")]
        public string Name { get; set; }
        public string? AssetLiabilityId { get; set; }
        public BudgetItemTypes Type { get; set; }
        public bool IsActive { get; set; } = true;
        public string CompanyBin { get; set; }
        public List<AssetLiabilityViewModel> Assets { get; set; }
        public List<AssetLiabilityViewModel> Liabilities { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}, Assets: {Assets}, " +
                   $"Name: {Name}, Type: {Type}, CompanyBin: {CompanyBin}, IsActive: {IsActive}, " +
                   $"AssetLiabilityId: {AssetLiabilityId}";
        }
    }
}