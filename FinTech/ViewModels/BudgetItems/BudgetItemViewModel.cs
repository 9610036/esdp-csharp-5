﻿using FinTech.Enums;
using FinTech.ViewModels.AssetLiabilities;
using FinTech.ViewModels.Companies;

namespace FinTech.ViewModels.BudgetItems
{
    public class BudgetItemViewModel
    {
        public string Id { get; set;}
        public string Name { get; set; }
        
        public string Article { get; set; }
        public BudgetItemTypes Type { get; set; }
        public string AssetLiabilityId { get; set; }
        public AssetLiabilityViewModel AssetLiability { get; set; }
        public bool IsActive { get; set; } = true;
        public CompanyViewModel Company { get; set; }
        public string BudgetTypeId { get; set; }
    }
}