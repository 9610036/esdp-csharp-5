using System.ComponentModel.DataAnnotations;
using AutoMapper;
using FinTech.Enums;
using FinTech.Models;
using FinTech.ViewModels.AssetLiabilities;
using FinTech.ViewModels.Companies;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.BudgetItems
{
    public class UpdateBudgetItemViewModel
    {
        public string Id { get; set;}
        
        [Remote("ValidateBudgetItem", "BudgetItemValidationController", ErrorMessage = "Статья уже занята")]
        [Required(ErrorMessage = "Введите название статьи")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Выберите тип статьи: доход или расход")]
        public BudgetItemTypes Type { get; set; }
        public string? AssetLiabilityId { get; set; }
        public AssetLiabilityViewModel AssetLiability { get; set; }
        public string CompanyBin { get; set; }
        public string CompanyId { get; set; }
        public CompanyViewModel Company { get; set; }
        public bool IsActive { get; set; } = true;
        
        public BudgetItem ConvertToBudgetItem(IMapper mapper)
        {
            return new BudgetItem()
            {
                Id = Id,
                Name = Name,
                AssetLiability = mapper.Map<AssetLiability>(AssetLiability),
                Company = mapper.Map<Company>(Company),
                Type = Type
            };
        }

        public override string ToString()
        {
            return $"Company: {Company}, Id: {Id}, " +
                   $"Name: {Name}, Type: {Type}, AssetLiability: {AssetLiability}, " +
                   $"CompanyBin: {CompanyBin}, CompanyId: {CompanyId},  IsActive: {IsActive}, " +
                   $"AssetLiabilityId:{AssetLiabilityId}";
        }
    }
}