﻿using System.ComponentModel.DataAnnotations;

namespace FinTech.ViewModels.Account
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Введите адрес электронной почты")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Введите пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
 
        [Required(ErrorMessage = "Повторите введенный пароль")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }

        [Required(ErrorMessage = "Введите названиев вашей компании")]
        [DataType(DataType.Text)]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Введите БИН вашей компании")]
        [DataType(DataType.Text)]
        [MaxLength(12)]
        public string Bin { get; set; }
    }
}