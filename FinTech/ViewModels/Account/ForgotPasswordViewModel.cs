﻿using System.ComponentModel.DataAnnotations;

namespace FinTech.ViewModels.Account
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Обязательное поле Email")]
        [EmailAddress]
        public string Email { get; set; }
    }
}