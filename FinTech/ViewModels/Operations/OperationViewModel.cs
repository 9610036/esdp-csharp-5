﻿using System;
using FinTech.Enums;
using FinTech.ViewModels.BudgetItems;
using FinTech.ViewModels.Companies;
using FinTech.ViewModels.Counterparties;
using FinTech.ViewModels.PaymentAccounts;

namespace FinTech.ViewModels.Operations
{
    public class OperationViewModel
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public decimal Sum { get; set; }
        public string Comment { get; set; }
        public bool IsActive { get; set; } = true;
        public CompanyViewModel Company { get; set; }
        public BudgetItemViewModel BudgetItem { get; set; }
        public string CounterpartyId { get; set; }
        public CounterpartyViewModel Counterparty { get; set; }
        public string PaymentAccountId { get; set; }
        public PaymentAccountViewModel PaymentAccount { get; set; }
        public bool IsConfirmed { get; set; }
        public OperationTypes Type { get; set; }
        
    }
}