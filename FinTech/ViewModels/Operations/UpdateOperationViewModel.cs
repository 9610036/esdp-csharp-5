using System;
using System.Collections.Generic;
using FinTech.Enums;
using FinTech.Models;
using FinTech.ViewModels.BudgetItems;
using FinTech.ViewModels.Counterparties;
using FinTech.ViewModels.PaymentAccounts;

namespace FinTech.ViewModels.Operations
{
    public class UpdateOperationViewModel
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public decimal Sum { get; set; }
        public bool IsConfirmed { get; set; }
        public OperationTypes Type { get; set; }
        public string Comment { get; set; }
        public int BudgetItemType { get; set; }
        public string BudgetItemId { get; set; }
        public BudgetItemViewModel BudgetItem { get; set; }
        public string CounterpartyId { get; set; }
        public CounterpartyViewModel Counterparty { get; set; }
        public string PaymentAccountId { get; set; }
        public PaymentAccountViewModel PaymentAccount { get; set; }
        public string CompanyId { get; set; }
        public List<BudgetItemViewModel> BudgetItems { get; set; }
        public List<CounterpartyViewModel> Counterparties { get; set; }
        public List<PaymentAccountViewModel> PaymentAccounts { get; set; }
        
        public Operation ConvertToOperation()
        {
            return new Operation()
            {
               Id = Id,
               Comment = Comment,
               CreationDate = CreationDate,
               Type = Type,
               Sum = Sum,
               CompanyId = CompanyId,
               BudgetItemId = BudgetItemId,
               CounterpartyId = CounterpartyId,
               PaymentAccountId = PaymentAccountId,
               IsConfirmed = IsConfirmed
            };
        }

        public override string ToString()
        {
            return $"Date:{CreationDate} Sum:{Sum} Comment:{Comment} Confirmed:{IsConfirmed} BudgetItemId:{BudgetItemId} " +
                   $"BudgetItemType:{BudgetItemType} CounterpartyId:{CounterpartyId} " +
                   $"PaymentAccountId:{PaymentAccountId} Id:{Id}";
        }
    }
}