using System.Collections.Generic;
using FinTech.ViewModels.Operations.Configurations;

namespace FinTech.ViewModels.Operations
{
    public class OperationIndexViewModel
    {
        public OperationsConfigurationsViewModel Configurations { get; set; }
        public List<OperationViewModel> Operations { get; set; }
    }
}