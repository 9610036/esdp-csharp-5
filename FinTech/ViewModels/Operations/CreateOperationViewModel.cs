using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FinTech.Enums;
using FinTech.ViewModels.BudgetItems;
using FinTech.ViewModels.Counterparties;
using FinTech.ViewModels.PaymentAccounts;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.Operations
{
    public class CreateOperationViewModel
    {
        [Required(ErrorMessage = "Введите дату создания операции")]
        [Remote("FutureDateValidation", "OperationValidation", ErrorMessage = "Дата не может быть позднее текущей")]
        public DateTime CreationDate { get; set; }
        [Required(ErrorMessage = "Введите сумму операции")]
        [Remote("SumLimitValidationOnOperationCreating", "OperationValidation",
            AdditionalFields = "BudgetItemType, PaymentAccountId, CreationDate", ErrorMessage = "Недостаточно средств на счете на дату операции")]
        [Range(1, Double.PositiveInfinity, ErrorMessage = "Сумма не может быть меньше 1")]
        public decimal Sum { get; set; }

        public string Comment { get; set; }
        public bool IsConfirmed { get; set; }
        public string BudgetItemId { get; set; }
        public BudgetItemTypes BudgetItemType { get; set; }
        public string CounterpartyId { get; set; }
        public string PaymentAccountId { get; set; }
        public string CompanyBin { get; set; }
        public OperationTypes OperationType { get; set; }
        public List<BudgetItemViewModel> BudgetItems { get; set; } 
        public List<CounterpartyViewModel> Counterparties { get; set; } 
        public List<PaymentAccountViewModel> PaymentAccounts { get; set; }

        public override string ToString()
        {
            return $"Date:{CreationDate} Sum:{Sum} Comment:{Comment} Confirmed:{IsConfirmed} BudgetItemId:{BudgetItemId} " +
                   $"BudgetItemType:{BudgetItemType} CounterpartyId:{CounterpartyId} " +
                   $"PaymentAccountId:{PaymentAccountId} CompanyBin:{CompanyBin} OperationType:{OperationType}";
        }
    }
}