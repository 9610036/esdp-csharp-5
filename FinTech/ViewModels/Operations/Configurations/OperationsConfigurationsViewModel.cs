using FinTech.ViewModels.Operations.Configurations.Filtration;
using FinTech.ViewModels.Pagination;

namespace FinTech.ViewModels.Operations.Configurations
{
    public class OperationsConfigurationsViewModel
    {
        public PageViewModel PageViewModel { get; set; }
        public FilterOperationsViewModel ViewModel { get; set; }
    }
}