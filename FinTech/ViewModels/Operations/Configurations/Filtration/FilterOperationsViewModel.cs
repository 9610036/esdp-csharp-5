﻿using System;
using System.Collections.Generic;
using FinTech.Enums;
using Newtonsoft.Json;

namespace FinTech.ViewModels.Operations.Configurations.Filtration
{
    public class FilterOperationsViewModel
    {
        public bool IsEmpty { get; set; } = true;
        public BudgetItemTypes? Type { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateUntil { get; set; }
        public string SumFrom { get; set; }
        public string SumUntil { get; set; }
        
        public string SerializedPaymentAccounts { get; set; }
        public List<string> PaymentAccounts { get; set; }
        
        public string SerializedCounterparties { get; set; }
        public List<string> Counterparties { get; set; }

        public void DeserializeLists()
        {
            if (SerializedPaymentAccounts != null)
                PaymentAccounts =
                    JsonConvert.DeserializeObject<List<string>>(SerializedPaymentAccounts);
            if (SerializedCounterparties != null)
                Counterparties =
                    JsonConvert.DeserializeObject<List<string>>(SerializedCounterparties);
        }
    }
}