namespace FinTech.ViewModels.Companies
{
    public class CompanyViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Bin { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
    }
}