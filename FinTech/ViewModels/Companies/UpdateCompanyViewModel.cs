using System.ComponentModel.DataAnnotations;
using FinTech.Models;

namespace FinTech.ViewModels.Companies
{
    public class UpdateCompanyViewModel
    {
        public string Id { get; set; }
        
        [Required(ErrorMessage = "Введите название вашей компании")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Введите БИН вашей компании")]
        public string Bin { get; set; }
        [Required(ErrorMessage = "Введите номер телефона вашей компании")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Введите адрес электронной почты вашей компании")]
        public string Email { get; set; }
        
        public Company ConvertToCompany()
        {
            return new Company()
            {
                Id = Id,
                Name = Name,
                Bin = Bin,
                Phone = Phone,
                Email = Email
            };
        }

        public override string ToString()
        {
            return $"Bin:{Bin}, Email:{Email}, Id:{Id}," +
                   $"Name:{Name}, Phone:{Phone}";
        }
    }
}