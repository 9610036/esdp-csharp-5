﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FinTech.ViewModels.Operations;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.CashFlows
{
    public class CashFlowViewModel
    {
        [Required(ErrorMessage = "Введите год")]
        [Remote("YearValidation", "CashFlowValidation", AdditionalFields = "Year", ErrorMessage = "Год должен быть между 1900 и текущим годом")]
        public int Year { get; set; }
        public decimal StartSum { get; set; }
        public decimal EndSum { get; set; }
        public List<OperationViewModel> IncomeOperations { get; set; }
        public List<OperationViewModel> ExpenseOperations { get; set; }
    }
}