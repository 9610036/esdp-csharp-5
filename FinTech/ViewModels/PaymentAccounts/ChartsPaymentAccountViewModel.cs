namespace FinTech.ViewModels.PaymentAccounts
{
    public class ChartsPaymentAccountViewModel
    {
        public string Name { get; set; }
        public decimal[] SumPerMonths { get; set; }
    }
}