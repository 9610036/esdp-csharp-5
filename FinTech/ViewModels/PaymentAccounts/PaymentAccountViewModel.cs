﻿using System;
using FinTech.Enums;
using FinTech.ViewModels.Banks;
using FinTech.ViewModels.Companies;
using FinTech.ViewModels.Counterparties;

namespace FinTech.ViewModels.PaymentAccounts
{
    public class PaymentAccountViewModel
    {
        public string Id { get; set; }
        public string Iban { get; set; }
        public BankViewModel Bank { get; set; }
        public decimal Amount { get; set; }
        public bool IsActive { get; set; }
        public CurrencyTypes CurrencyType { get; set; }
        public DateTime CreationDate { get; set; }
        public CompanyViewModel Company { get; set; }
        public CounterpartyViewModel Counterparty { get; set; }
    }
}