﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FinTech.Enums;
using FinTech.ViewModels.Banks;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.PaymentAccounts
{
    public class CreatePaymentAccountViewModel
    {
        [Required(ErrorMessage = "Введите номер расчетного счета")]
        [MinLength(20, ErrorMessage = "Введите 20-тизначный буквенно-цифровой код")]
        [MaxLength(20, ErrorMessage = "Введите 20-тизначный буквенно-цифровой код")]
        [Remote("ValidateBankNameAndIban", "PaymentAccountValidation", 
            AdditionalFields = "Iban,BankName,CompanyId", ErrorMessage = "Такой счет уже есть")]
        public string Iban { get; set; }
        public string BankId { get; set; }
        [Required(ErrorMessage = "Введите сумму на рассчетном счете")]
        public decimal Amount { get; set; }
        public CurrencyTypes CurrencyType { get; set; }
        public DateTime CreationDate { get; set; }
        public string CompanyId { get; set; }
        public string CounterpartyId { get; set; }
        public List<BankViewModel> Banks { get; set; }

        public override string ToString()
        {
            return $"Amount:{Amount},Banks:{Banks},Iban:{Iban},BankId:{BankId} CompanyId:{CompanyId}," +
                   $"ConterpartyId:{CounterpartyId}, Date:{CreationDate}, Type:{CurrencyType}";
        }
    }
}