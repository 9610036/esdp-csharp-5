﻿using System;
using System.ComponentModel.DataAnnotations;
using FinTech.Enums;
using FinTech.Models;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.PaymentAccounts
{
    public class UpdatePaymentAccountViewModel
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Введите номер расчетного счета")]
        [MinLength(20, ErrorMessage = "Введите 20-тизначный буквенно-цифровой код")]
        [MaxLength(20, ErrorMessage = "Введите 20-тизначный буквенно-цифровой код")]
        [Remote("ValidateBankNameAndIban", "PaymentAccountValidation", 
            AdditionalFields = "Iban,BankName", ErrorMessage = "Такой счет уже есть")]
        public string Iban { get; set; }
        public string BankId { get; set; }
        public decimal Amount { get; set; }
        public CurrencyTypes CurrencyType { get; set; }
        public DateTime CreationDate { get; set; }
        public string CompanyId { get; set; }
        public string CounterpartyId { get; set; }
        
        
        public PaymentAccount ConvertToPaymentAccount()
        {
            return new PaymentAccount()
            {
                Id = Id,
                Amount = Amount,
                Iban = Iban,
                BankId = BankId,
                CompanyId = CompanyId,
                CounterpartyId = CounterpartyId,
                CurrencyType = CurrencyType
            };
        }

        public override string ToString()
        {
            return $"Amount:{Amount},Id:{Id},Iban:{Iban},BankId:{BankId}," +
            $"CompanyId:{CompanyId}, ConterpartyId:{CounterpartyId}, Date:{CreationDate}, Type:{CurrencyType}";
        }
    }
}