﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.Banks
{
    public class CreateBankViewModel
    {
        [Required(ErrorMessage = "Введите название банка")]
        [Remote("ValidateBankByName", "BankValidation",
            AdditionalFields = "Id", ErrorMessage = "Банк с таким названием уже есть")]
        public string Name { get; set; }

        [Remote("ValidateBankByBik", "BankValidation",
            AdditionalFields = "Id", ErrorMessage = "Банк с таким БИК уже есть")]
        [Required(ErrorMessage = "Введите БИК банка")]
        public string Bik { get; set; }

        public override string ToString()
        {
            return $"Bik: {Bik}, Name: {Name}";
        }
    }
}