﻿namespace FinTech.ViewModels.Banks
{
    public class BankViewModel
    {
        public string Id { get; set; } 
        public string Name { get; set; }
        public string Bik { get; set; }
        public bool IsActive { get; set; }
    }
}