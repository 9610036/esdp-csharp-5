﻿using System.ComponentModel.DataAnnotations;
using FinTech.Models;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.Banks
{
    public class UpdateBankViewModel
    {
        public  string Id { get; set; }
        [Required(ErrorMessage = "Введите название банка")]
        [Remote("ValidateBankByName", "BankValidation",
            AdditionalFields = "Id", ErrorMessage = "Банк с таким названием уже есть")]
        public string Name { get; set; }

        [Remote("ValidateBankByBik", "BankValidation",
            AdditionalFields = "Id", ErrorMessage = "Банк с таким БИК уже есть")]
        [Required(ErrorMessage = "Введите БИК банка")]
        public string Bik { get; set; }
        
        
        public Bank ConvertToBank()
        {
            return new Bank()
            {
                Id = Id,
                Name = Name,
                Bik = Bik
            };
        }

        public override string ToString()
        {
            return $"Bik:{Bik}, Name:{Bik}, id: {Id}";
        }
    }
}