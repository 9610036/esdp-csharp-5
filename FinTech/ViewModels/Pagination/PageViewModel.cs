﻿namespace FinTech.ViewModels.Pagination
{
    public class PageViewModel
    {
        public int UnitsOnPage { get; set; } = 10;
        public int CurrentPage { get; set; }
        public bool HasNextPage { get; set; }

        public PageViewModel() { }

        public PageViewModel(int currentPage)
        {
            CurrentPage = currentPage;
        }

        public void NextPageAvailability(int collectionCount)
        {
            HasNextPage = collectionCount > CurrentPage * UnitsOnPage + UnitsOnPage;
        }
    }
}