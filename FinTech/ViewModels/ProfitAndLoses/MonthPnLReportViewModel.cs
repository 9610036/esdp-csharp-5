﻿using FinTech.Enums;

namespace FinTech.ViewModels.ProfitAndLoses
{
    public class MonthPnLReportViewModel
    {
        public string BudgetItemName { get; set; }
        public BudgetItemTypes BudgetItemType { get; set; }
        public decimal Sum { get; set; }
        public string CompanyBin { get; set; }
    }
}