﻿using System;
using System.Collections.Generic;

namespace FinTech.ViewModels.ProfitAndLoses
{
    public class MonthsPnLReportViewModel
    {
        public List<MonthPnLReportViewModel> MonthsReport { get; set; }
        public DateTime DateOfReport { get; set; }
        public decimal OperatingProfit { get; set; }
        public decimal Profitability { get; set; }

    }
}