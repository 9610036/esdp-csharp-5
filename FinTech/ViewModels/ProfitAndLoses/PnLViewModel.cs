﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.ProfitAndLoses
{
    public class PnLViewModel
    {
        public List<MonthsPnLReportViewModel> YearPnLReport { get; set; }
        [Required(ErrorMessage = "Введите год")]
        [Remote("YearValidation", "CashFlowValidation", AdditionalFields = "Year", ErrorMessage = "Год должен быть между 1900 и текущим годом")]
        public int Year { get; set; }
        public string CompanyBin { get; set; }
    }
}