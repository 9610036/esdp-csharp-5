﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FinTech.ViewModels.Operations;

namespace FinTech.ViewModels.ProfitAndLoses
{
    public class ProfitAndLosesViewModel
    {
        public List<OperationViewModel> Operations { get; set; }
        public string CompanyId { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:MM}")]
        public DateTime FromDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:MM}")]
        public DateTime ToDate { get; set; }
        public decimal TotalExpenseSum { get; set; }
        public decimal TotalIncomeSum { get; set; }
        public decimal Profit { get; set; }
        public decimal ProfitabilityPercent { get; set; }
        public List<ProfitAndLosesReportViewModel> ReportViewModels { get; set; }

        public override string ToString()
        {
            return $"Operations:{Operations}, Profit:{Profit}," +
                $"CompanyId:{CompanyId}, FromDate:{FromDate}" +
                $"Percent:{ProfitabilityPercent}, ToDate:{ToDate}" +
                $"ListReports:{ReportViewModels}, " +
                $"ExpenseSum:{TotalExpenseSum},IncomeSum:{TotalIncomeSum}";
        }
    }
}