﻿using System.Collections.Generic;
using FinTech.ViewModels.Operations;

namespace FinTech.ViewModels.ProfitAndLoses
{
    public class ProfitAndLosesReportViewModel
    {
        public string ReportName { get; set; }
        public string OperationsName { get; set; }
        public List<OperationViewModel> Operations { get; set; }
        public decimal OperationsSum { get; set; }
        public bool IsIncome { get; set; } = false;
    }
    
}