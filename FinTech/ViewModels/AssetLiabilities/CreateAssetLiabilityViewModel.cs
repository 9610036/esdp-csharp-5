﻿using System;
using System.ComponentModel.DataAnnotations;
using FinTech.Enums;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.AssetLiabilities
{
    public class CreateAssetLiabilityViewModel
    {
        public string Id { get; set;}
        [Required(ErrorMessage = "Введите название статьи")]
        [Remote("ValidateAssetLiability", "AssetLiabilityValidationController", ErrorMessage = "Статья уже занята")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Выберите тип: актив или обязательство")]
        public AssetLiabilityTypes Type { get; set; }
        public DateTime CreationDate { get; set; }
        [Required(ErrorMessage = "Введите суммы статьи")]
        [Range(0, 999999999999.99,ErrorMessage = "Достигнут порог максимального значения")]
        
        public AssetTypes AssetType { get; set; }
        public decimal Sum { get; set; }
        public string CompanyId { get; set; }
        public bool IsActive { get; set; } = true;

        public override string ToString()
        {
            return $"Name:{Name} Sum:{Sum} Type:{Type} CompanyId:{CompanyId} Date: {CreationDate} IsActive:{IsActive} ";
        }
    }
}