﻿using System;
using System.ComponentModel.DataAnnotations;
using FinTech.Enums;
using FinTech.Models;
using Microsoft.AspNetCore.Mvc;

namespace FinTech.ViewModels.AssetLiabilities
{
    public class AssetLiabilityViewModel
    {
        public string Id { get; set;}
        [Required(ErrorMessage = "Введите название статьи")]
        [Remote("ValidateAssetLiability", "AssetLiabilityValidationController", ErrorMessage = "Статья уже занята")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Выберите тип: актив или обязательство")]
        public AssetLiabilityTypes Type { get; set; }
        public DateTime CreationDate { get; set; }
        [Required(ErrorMessage = "Введите суммы статьи")]
        [RegularExpression(@"^([1-9][0-9]*)+(.[0-9]{1,2})?$", ErrorMessage = "Не коректный ввод суммы!")]
        [Range(0, 999999999999.99,ErrorMessage = "Достигнут порог максимального значения")]
        public decimal Sum { get; set; }

        public AssetTypes AssetType { get; set; }
        public string CompanyId { get; set; }
        public bool IsActive { get; set; } = true;
        
        public AssetLiability ConvertToAssetLiability()
        {
            return new AssetLiability()
            {
                Id = Id,
                Name = Name,
                CompanyId = CompanyId,
                CreationDate = CreationDate,
                Sum = Sum,
                Type = Type
            };
        }

        public override string ToString()
        {
            return $"Id:{Id} Name:{Name} Sum:{Sum} Type:{Type} CompanyId:{CompanyId} " +
                   $"Date:{CreationDate} IsActive:{IsActive}";
        }
    }
}