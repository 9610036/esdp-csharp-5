using System;
using System.Collections.Generic;
using FinTech.ViewModels.AssetLiabilities;

namespace FinTech.ViewModels.Balances
{
    public class BalanceReportViewModel
    {
        public DateTime YearOfReport { get; set; }
        
        public decimal SumOnPaymentAccounts { get; set; }
        public decimal SumOnPaymentAccountsOnCreationDate { get; set; }

        public List<AssetLiabilityViewModel> InventoryAssets{ get; set; }
        public decimal InventorySum { get; set; }

        public List<AssetLiabilityViewModel> MainCashAssets { get; set; }
        public decimal MainCashSum { get; set; }

        public List<AssetLiabilityViewModel> ShortTermFinancialLiabilities { get; set; }
        public decimal ShortTermFinancialLiabilitiesSum { get; set; }

        public decimal AuthorizedCapital { get; set; }
        public decimal AccountsReceivable { get; set; }
        public decimal AccountsPayable { get; set; }
        public decimal RetainedEarnings { get; set; }
        public decimal TotalLiabilitiesSum { get; set; }
        public decimal TotalAssetsSum { get; set; }
        public decimal TotalCapital { get; set; }
        public decimal ShortTermAssets { get; set; }
        public decimal ShortTermLiabilities { get; set; }

        public override string ToString()
        {
            return $"Year:{YearOfReport} SumOnPaymentAccounts:{SumOnPaymentAccounts} " +
                   $"SumOnPaymentAccountsOnCreationDate:{SumOnPaymentAccountsOnCreationDate} " +
                   $"InventorySum:{InventorySum} MainCashSum:{MainCashSum} " +
                   $"ShortTermFinancialLiabilitiesSum:{ShortTermFinancialLiabilitiesSum} AuthorizedCapital:{AuthorizedCapital} " +
                   $"AccountsReceivable:{AccountsReceivable} " +
                   $"AccountsPayable:{AccountsPayable} RetainedEarnings:{RetainedEarnings} TotalLiabilitiesSum:{TotalLiabilitiesSum} " +
                   $"TotalAssetsSum:{TotalAssetsSum} TotalCapital:{TotalCapital} ShortTermAssets:{ShortTermAssets} " +
                   $"ShortTermLiabilities:{ShortTermLiabilities}";
        }
    }
}