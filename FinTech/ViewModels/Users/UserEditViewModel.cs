namespace FinTech.ViewModels.Users
{
    public class UserEditViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }

        public override string ToString()
        {
            return $"Id:{Id} Email:{Email} RoleName:{RoleName}";
        }
    }
}