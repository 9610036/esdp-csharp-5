using System.ComponentModel.DataAnnotations;

namespace FinTech.ViewModels.Users
{
    public class CreateUserViewModel
    {
        [Required(ErrorMessage = "Введите адрес электронной почты")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Введите пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
 
        [Required(ErrorMessage = "Повторите введенный пароль")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }

        public string CreatorUserName { get; set; }
    }
}