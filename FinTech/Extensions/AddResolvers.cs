using FinTech.MappingConfigurations.Resolvers;
using Microsoft.Extensions.DependencyInjection;

namespace FinTech.Extensions
{
    public static class AddResolvers
    {
        public static void Resolvers(this IServiceCollection services)
        {
            services.AddSingleton<GetBankResolver>();
            services.AddSingleton<GetBudgetItemResolver>();
            services.AddSingleton<GetCompanyResolver>();
            services.AddSingleton<GetCounterpartyResolver>();
            services.AddSingleton<GetPaymentAccountResolver>();
        }
    }
}