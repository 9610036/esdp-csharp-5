using FinTech.Services.Filtration.Operation;
using Microsoft.Extensions.DependencyInjection;

namespace FinTech.Extensions
{
    public static class AddFiltrationServices
    {
        public static void FiltrationServices(this IServiceCollection services)
        {
            services.AddTransient<IOperationFiltrationService, OperationFiltrationService>();
        }
    }
}