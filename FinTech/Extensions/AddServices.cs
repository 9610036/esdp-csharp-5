using FinTech.Services.AccountService;
using FinTech.Services.AssetLiabilityService;
using FinTech.Services.BalanceServices;
using FinTech.Services.BankService;
using FinTech.Services.BudgetItemService;
using FinTech.Services.CashFlowService;
using FinTech.Services.Charts;
using FinTech.Services.CompanyService;
using FinTech.Services.CounterpartyService;
using FinTech.Services.ExcellReportsService;
using FinTech.Services.FileService;
using FinTech.Services.OperationService;
using FinTech.Services.PaymentAccountService;
using FinTech.Services.ProfitAndLosesServices;
using FinTech.Services.UserService;
using Microsoft.Extensions.DependencyInjection;

namespace FinTech.Extensions
{
    public static class AddServices
    {
        public static void Services(this IServiceCollection services)
        {
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IPaymentAccountService, PaymentAccountService>();
            services.AddTransient<IProfitAndLosesService, ProfitAndLosesService>();
            services.AddTransient<ICompanyService, CompanyService>();
            services.AddTransient<ICounterpartyService, CounterpartyService>();
            services.AddTransient<IBudgetItemService, BudgetItemService>();
            services.AddTransient<IAssetLiabilityService, AssetLiabilityService>();
            services.AddTransient<IOperationService, OperationService>();
            services.AddTransient<IBankService, BankService>();
            services.AddTransient<IBalanceService, BalanceService>();
            services.AddTransient<ICashFlowService, CashFlowService>();
            services.AddTransient<IChartsService, ChartsService>();
            services.FiltrationServices();
            services.PaginationServices();
            services.AddTransient<ExcelReports>();
            services.AddTransient<RolesInitializer>();
            services.AddTransient<IUserService, UserService>();
        }
    }
}