using FinTech.Services.Pagination.Operation;
using Microsoft.Extensions.DependencyInjection;

namespace FinTech.Extensions
{
    public static class AddPaginationServices
    {
        public static void PaginationServices(this IServiceCollection services)
        {
            services.AddTransient<IOperationPaginationService, OperationPaginationService>();
        }
    }
}