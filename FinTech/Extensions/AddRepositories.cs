using FinTech.Repositories.AssetLiabilityRepositories;
using FinTech.Repositories.BankRepositories;
using FinTech.Repositories.BudgetItemRepositories;
using FinTech.Repositories.CompanyRepositories;
using FinTech.Repositories.CounterpartyRepositories;
using FinTech.Repositories.OperationRepositories;
using FinTech.Repositories.PaymentAccountRepositories;
using FinTech.Repositories.UserRepositories;
using Microsoft.Extensions.DependencyInjection;

namespace FinTech.Extensions
{
    public static class AddRepositories
    {
        public static void Repositories(this IServiceCollection services)
        {
            services.AddTransient<IAssetLiabilityRepository, AssetLiabilityRepository>();
            services.AddTransient<IBudgetItemRepository, BudgetItemRepository>();
            services.AddTransient<ICounterpartyRepository, CounterpartyRepository>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();
            services.AddTransient<IOperationRepository, OperationRepository>();
            services.AddTransient<IPaymentAccountRepository, PaymentAccountRepository>();
            services.AddTransient<IBankRepository, BankRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
        }
    }
}