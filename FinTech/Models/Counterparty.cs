﻿using System.Collections.Generic;

namespace FinTech.Models
{
    public class Counterparty
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Bin { get; set; }
        public string Comment { get; set; }
        public bool IsActive { get; set; } = true;
        
        public string CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public virtual List<PaymentAccount> PaymentAccounts { get; set; }
    }
}