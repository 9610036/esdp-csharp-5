﻿using FinTech.Models.MapConfigurations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FinTech.Models.Data
{
    public class EsdpContext : IdentityDbContext<User>
    {
        public DbSet<Company> Companies { get; set; }
        public DbSet<Counterparty> Counterparties { get; set; }
        public DbSet<PaymentAccount> PaymentAccounts { get; set; }
        public DbSet<AssetLiability> AssetLiabilities { get; set; }
        public DbSet<BudgetItem> BudgetItems { get; set; }

        public DbSet<Bank> Banks { get; set;}
        public DbSet<Operation> Operations { get; set; }
        public EsdpContext(DbContextOptions<EsdpContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new OperationDbMap());
            builder.ApplyConfiguration(new CompanyDbMap());
            builder.ApplyConfiguration(new BudgetItemDbMap());
            builder.ApplyConfiguration(new CounterpartyDbMap());
            builder.ApplyConfiguration(new PaymentAccountDbMap());
            builder.ApplyConfiguration(new AssetLiabilityDbMap());
            builder.ApplyConfiguration(new BankDbMap());
        }

    }
}