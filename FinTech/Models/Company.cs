﻿using System.Collections.Generic;

namespace FinTech.Models
{
    public class Company
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Bin { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        
        public virtual List<Counterparty> Counterparties { get; set; }
        public virtual List<PaymentAccount> PaymentAccounts { get; set; }
        public virtual List<AssetLiability> AssetLiabilities { get; set; }
        public virtual List<BudgetItem> BudgetItems { get; set; }
    }
}