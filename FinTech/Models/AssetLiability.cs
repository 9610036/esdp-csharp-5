﻿using System;
using FinTech.Enums;

namespace FinTech.Models
{
    public class AssetLiability
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string Name { get; set; }
        public decimal Sum { get; set; }
        public bool IsActive { get; set; } = true;
        public AssetLiabilityTypes Type { get; set; }
        public AssetTypes? AssetType { get; set; }
        public string CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }
}