﻿using System;
using FinTech.Enums;

namespace FinTech.Models
{
    public class PaymentAccount
    {
        public string Id { get; set; }
        public string Iban { get; set; }
        public decimal Amount { get; set; }
        public CurrencyTypes CurrencyType { get; set; }
        public DateTime CreationDate { get; set; }
        public string CompanyId { get; set; }
        public virtual Company? Company { get; set; }
        public string CounterpartyId { get; set; }
        public virtual Counterparty? Counterparty { get; set; }
        public string BankId { get; set; }
        public virtual Bank Bank { get; set; }
        public bool IsActive { get; set; } = true;
    }
}