﻿namespace FinTech.Models
{
    public class Bank
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Bik { get; set; }
        public bool IsActive { get; set; } = true;
    }
}