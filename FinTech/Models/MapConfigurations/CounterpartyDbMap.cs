﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinTech.Models.MapConfigurations
{
    public class CounterpartyDbMap :IEntityTypeConfiguration<Counterparty>
    {
        public void Configure(EntityTypeBuilder<Counterparty> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id).ValueGeneratedOnAdd().HasMaxLength(36);
            builder.Property(c => c.Bin).HasMaxLength(12);
            builder.Property(c => c.Comment).HasMaxLength(64);
            builder.Property(c => c.Name).HasMaxLength(64);
            builder.Property(c => c.ShortName).HasMaxLength(12);
        }
    }
}