﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinTech.Models.MapConfigurations
{
    public class CompanyDbMap:IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id).ValueGeneratedOnAdd().HasMaxLength(36);
            builder.Property(c=>c.Bin).HasMaxLength(12);
            builder.Property(c=>c.Email).HasMaxLength(32);
            builder.Property(c=>c.Name).HasMaxLength(64);
            builder.Property(c=>c.Phone).HasMaxLength(12);
        }
    }
}