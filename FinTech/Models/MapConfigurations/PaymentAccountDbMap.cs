﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinTech.Models.MapConfigurations
{
    public class PaymentAccountDbMap:IEntityTypeConfiguration<PaymentAccount>
    {
        public void Configure(EntityTypeBuilder<PaymentAccount> builder)
        {
            builder.HasKey(pa => pa.Id);
            builder.Property(pa => pa.Id).ValueGeneratedOnAdd().HasMaxLength(36);
            builder.Property(p => p.Amount).HasColumnType("numeric(18,2)");
            builder.Property(p=>p.Iban).HasMaxLength(20);
        }
    }
}