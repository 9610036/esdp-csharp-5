﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinTech.Models.MapConfigurations
{
    public class BankDbMap:IEntityTypeConfiguration<Bank>
    {
        public void Configure(EntityTypeBuilder<Bank> builder)
        {
            builder.HasKey(b => b.Id);
            builder.Property(b => b.Id).ValueGeneratedOnAdd().HasMaxLength(36);
            builder.Property(b => b.Name).HasMaxLength(64);
            builder.Property(b => b.Bik).HasMaxLength(12);
        }
    }
}