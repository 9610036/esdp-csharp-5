﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinTech.Models.MapConfigurations
{
    public class OperationDbMap:IEntityTypeConfiguration<Operation>
    {
        public void Configure(EntityTypeBuilder<Operation> builder)
        {
            builder.HasKey(o => o.Id);
            builder.Property(o => o.Id).ValueGeneratedOnAdd().HasMaxLength(36);
            builder.Property(o => o.Sum).HasColumnType("numeric(18,2)");
            builder.Property(o => o.Comment).HasMaxLength(512);
        }
    }
}