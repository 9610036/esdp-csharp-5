﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinTech.Models.MapConfigurations
{
    public class AssetLiabilityDbMap:IEntityTypeConfiguration<AssetLiability>
    {
        public void Configure(EntityTypeBuilder<AssetLiability> builder)
        {
            builder.HasKey(a => a.Id);
            builder.Property(a => a.Id).ValueGeneratedOnAdd().HasMaxLength(36);
            builder.Property(a => a.Name).HasMaxLength(64);
            builder.Property(a => a.Sum).HasColumnType("numeric(18,2)");
        }
    }
}