﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinTech.Models.MapConfigurations
{
    public class BudgetItemDbMap:IEntityTypeConfiguration<BudgetItem>
    {
        public void Configure(EntityTypeBuilder<BudgetItem> builder)
        {
            builder.HasKey(bi => bi.Id);
            builder.Property(bi => bi.Id).ValueGeneratedOnAdd().HasMaxLength(36);
            builder.Property(b => b.Name).HasMaxLength(64);
 
        }
    }
}