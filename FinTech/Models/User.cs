﻿using Microsoft.AspNetCore.Identity;

namespace FinTech.Models
{
    public class User : IdentityUser
    {
        public virtual Company Company { get; set; }
        public bool IsActive { get; set; }
    }
}