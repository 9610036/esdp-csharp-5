﻿using FinTech.Enums;

namespace FinTech.Models
{
    public class BudgetItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public BudgetItemTypes Type { get; set; }
        public virtual Company Company { get; set; }
        public virtual AssetLiability AssetLiability { get; set; }
        public bool IsActive { get; set; } = true;
    }
}