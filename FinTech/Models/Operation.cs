﻿using System;
using FinTech.Enums;

namespace FinTech.Models
{
    public class Operation
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public decimal Sum { get; set; }
        public string Comment { get; set; }
        public bool IsActive { get; set; } = true;
        public bool IsConfirmed { get; set; }
        public OperationTypes Type { get; set; }
        public string CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public string BudgetItemId { get; set; }
        public virtual BudgetItem BudgetItem { get; set; }
        public string CounterpartyId { get; set; }
        public virtual Counterparty Counterparty { get; set; }
        public string PaymentAccountId { get; set; }
        public virtual PaymentAccount PaymentAccount { get; set; }
    }
}