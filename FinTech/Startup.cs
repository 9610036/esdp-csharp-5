using FinTech.Extensions;
using FinTech.Models;
using FinTech.Models.Data;
using FinTech.Services.AccountService;
using FinTech.Services.EmailService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FinTech
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<EsdpContext>(o =>
                {
                    o.UseLazyLoadingProxies().UseNpgsql(connection);
                    o.EnableSensitiveDataLogging();
                })
                .AddIdentity<User, IdentityRole>(o => 
                    o.Password.RequireNonAlphanumeric = false)
                .AddEntityFrameworkStores<EsdpContext>()
                .AddDefaultTokenProviders()
                .AddErrorDescriber<CustomIdentityErrorDescriber>();
            services.ConfigureApplicationCookie(o => o.LoginPath = "/Account/Login");
            services.Services();
            services.Repositories();
            services.AddAutoMapper(typeof(Startup));
            services.Configure<InfoEmail>(Configuration.GetSection("AccountGmail"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, RolesInitializer rolesInitializer)
        {
            rolesInitializer.SeedAdminUser();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Charts}/{action=Index}/{id?}");
            });
        }
    }
}